echo "[INSTALLER] Installing monitoring server..."

# Configuring APT sources
# Influx repository
if [ ! -f "/etc/apt/sources.list.d/influxdata.list" ]; then
  echo "[INSTALLER] Adding Influx repository..."
  echo "deb https://repos.influxdata.com/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/influxdata.list
  sudo curl -sL https://repos.influxdata.com/influxdb.key | sudo apt-key add -
fi
# Grafana repository
if [ ! -f "/etc/apt/sources.list.d/grafana.list" ]; then
  echo "[INSTALLER] Adding Grafana repository..."
  echo "deb https://packages.grafana.com/oss/deb stable main" | sudo tee /etc/apt/sources.list.d/grafana.list
  wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -
fi
echo "[INSTALLER] Refreshing packets list..."
sudo apt update

# Installing software
echo "[INSTALLER] Installing InfluxDB..."
sudo apt install influxdb -y

echo "[INSTALLER] Installing Grafana..."
sudo apt install grafana -y

echo "[INSTALLER] Installing Telegraf..."
sudo apt install telegraf -y

echo "[INSTALLER] Configuring Telegraf..."
sudo cp telegraf.conf /etc/telegraf/telegraf.conf

# Enabling services
echo "[INSTALLER] Enabling InfluxDB..."
sudo systemctl enable influxdb
sudo systemctl start influxdb
echo "[INSTALLER] Enabling Telegraf..."
sudo systemctl enable telegraf
sudo systemctl start telegraf
echo "[INSTALLER] Enabling Grafana..."
sudo systemctl daemon-relad
sudo systemctl enable grafana-server
sudo systemctl start grafana-server

echo "[INSTALLER] Done."

