package sevsu.gradeo.services;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import sevsu.gradeo.GradeoTestUtil;
import sevsu.gradeo.dtos.users.CreateUserDto;
import sevsu.gradeo.dtos.users.UpdateUserDto;
import sevsu.gradeo.entities.Teacher;
import sevsu.gradeo.entities.User;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.*;
import sevsu.gradeo.repositories.TeachersRepository;
import sevsu.gradeo.repositories.UserRepository;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
public class UsersServiceTests {
    @InjectMocks
    private UsersService usersService;
    @Mock
    private UserRepository userRepository;
    @Mock
    private TeachersRepository teachersRepository;
    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Mock
    private SessionService sessionService;
    @Spy
    private ModelMapper mapper;

    public UsersServiceTests() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllUsers_ShouldReturn_AListOfUsersDto() {
        var users = new ArrayList<User>();
        users.add(new User(
                1, "Foo", "1234", "email1@ya.ru",
                true, new HashSet<User.Role>(Arrays.asList(User.Role.ADMIN, User.Role.TEACHER)),
                new Teacher()));
        users.add(new User(
                2, "Bar", "4321", "email2@rambler.ru",
                true, new HashSet<User.Role>(Collections.singletonList(User.Role.STUDENT)),
                new Teacher()));

        Mockito.when(userRepository.findAllByRemovedDateIsNullOrderByIdAsc()).thenReturn(users);

        var dtos = usersService.getAllUsers();

        assertEquals(dtos.get(0).getEmail(), "email1@ya.ru");
        assertEquals(dtos.get(0).getId(), 1);
        assertEquals(dtos.get(0).getPassword(), "1234");
        assertEquals(dtos.get(0).getRoles(), new HashSet<User.Role>(Arrays.asList(User.Role.ADMIN, User.Role.TEACHER)));
        assertEquals(dtos.get(0).getUsername(), "Foo");
        assertEquals(dtos.get(1).getEmail(), "email2@rambler.ru");
        assertEquals(dtos.get(1).getId(), 2);
        assertEquals(dtos.get(1).getPassword(), "4321");
        assertEquals(dtos.get(1).getRoles(), new HashSet<User.Role>(Collections.singletonList(User.Role.STUDENT)));
        assertEquals(dtos.get(1).getUsername(), "Bar");
    }

    @Test
    public void getUserById_withoutExceptions() {
        var userId = 321321;
        var user = GradeoTestUtil.createBasicActiveUserWithId(userId);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        UpdateUserDto returnedUser = null;
        try {
            returnedUser = usersService.getUserById(userId).get();
        } catch (EntityNotFoundException e) {
            fail();
        }
        assertEquals(returnedUser.getPassword(),"");
        assertEquals(returnedUser.getId(), userId);
        assertEquals(returnedUser.getUsername(), user.getUsername());
    }

    @Test
    public void getUserById_with_EntityNotFoundException(){
        var userId = 321321;
        var user = GradeoTestUtil.createBasicActiveUserWithId(userId);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class, () -> usersService.getUserById(userId));
    }

    @Test
    public void getUserByEmail_withoutExceptions() {
        var userId = 321321;
        var user = GradeoTestUtil.createBasicActiveUserWithId(userId);
        Mockito.when(userRepository.findByEmailAndRemovedDateIsNull(user.getEmail())).thenReturn(user);
        User returnedUser = null;
        try {
            returnedUser = usersService.getUserByEmail(user.getEmail());
        } catch (EntityNotFoundException e) {
            fail();
        }
        assertEquals(returnedUser,user);
    }

    @Test
    public void getUserByEmail_with_EntityNotFoundException() {
        var userId = 321321;
        var user = GradeoTestUtil.createBasicActiveUserWithId(userId);
        Mockito.when(userRepository.findByEmailAndRemovedDateIsNull(user.getEmail())).thenReturn(null);
        assertThrows(EntityNotFoundException.class, () -> usersService.getUserByEmail(user.getEmail()));
    }

    @Test
    public void createUser_without_Exception() {
        mapper.getConfiguration().setAmbiguityIgnored(true);

        var argument = ArgumentCaptor.forClass(User.class);
        var createDto = new CreateUserDto();

        var encodePassword = "4321";
        var password = "1234";
        var email = "email@ya.ru";
        var username = "username";

        Mockito.when(bCryptPasswordEncoder.encode(password)).thenReturn(encodePassword);
        Mockito.when(userRepository.findByUsernameAndRemovedDateIsNull(username)).thenReturn(null);
        Mockito.when(userRepository.findByEmailAndRemovedDateIsNull(email)).thenReturn(null);

        createDto.setPassword(password);
        createDto.setConfirmPassword(password);
        createDto.setEmail(email);
        createDto.setUsername(username);

        Mockito.when(userRepository.save(Mockito.any(User.class))).thenAnswer(AdditionalAnswers.returnsFirstArg());
        assertDoesNotThrow(() -> usersService.createUser(createDto));
        Mockito.verify(userRepository, Mockito.times(1)).save(argument.capture());

        var createdUser = argument.getValue();

        assertEquals(createdUser.getUsername(), username);
        assertEquals(createdUser.getPassword(), encodePassword);
        assertEquals(createdUser.getEmail(), email);
        assertTrue(createdUser.isActive());

    }

    @Test
    public void createUser_with_UserAlreadyExistsException(){
        var user = GradeoTestUtil.createBasicActiveUserWithId(42);
        var createUserDto = new CreateUserDto();
        createUserDto.setUsername("userName");
        user.setUsername(createUserDto.getUsername());
        Mockito.when(userRepository.findByUsernameAndRemovedDateIsNull(createUserDto.getUsername())).thenReturn(user);
        assertThrows(UserAlreadyExistsException.class,() -> usersService.createUser(createUserDto));
    }

    @Test
    public void createUser_with_EmailAlreadyExistsException() {
        var user = GradeoTestUtil.createBasicActiveUserWithId(42);
        var createUserDto = new CreateUserDto();
        createUserDto.setEmail("email@mail.ru");
        user.setEmail(createUserDto.getEmail());
        Mockito.when(userRepository.findByEmailAndRemovedDateIsNull(createUserDto.getEmail())).thenReturn(user);
        assertThrows(EmailAlreadyExistsException.class,() -> usersService.createUser(createUserDto));
    }

    @Test
    public void createUser_with_PasswordsNotEqualException(){
        var createUserDto = new CreateUserDto();
        createUserDto.setPassword("Foo");
        createUserDto.setConfirmPassword("Bar");
        createUserDto.setEmail("email@mail.ru");
        createUserDto.setUsername("userName");
        Mockito.when(userRepository.findByEmailAndRemovedDateIsNull(createUserDto.getEmail())).thenReturn(null);
        Mockito.when(userRepository.findByUsernameAndRemovedDateIsNull(createUserDto.getUsername())).thenReturn(null);
        assertThrows(PasswordsNotEqualException.class, () -> usersService.createUser(createUserDto));
    }

    @Test
    public void deleteUserTeacherById_withoutException() {
        var argument = ArgumentCaptor.forClass(User.class);

        var userId = 13;
        var teacherId = 17;
        var user = GradeoTestUtil.createBasicActiveUserWithId(userId);
        Teacher teacher = GradeoTestUtil.createBasicTeacherWithId(teacherId);
        user.setTeacher(teacher);
        user.getTeacher().setUser(user);
        var user2 = GradeoTestUtil.createBasicActiveUserWithId(3322);
        Mockito.when(userRepository.findAll()).thenReturn(new ArrayList<>(Arrays.asList(user, user2)));
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        Mockito.when(teachersRepository.findById(user.getTeacher().getId())).thenReturn(Optional.of(teacher));
        sessionService.expireUserSessions(user.getUsername());

        Mockito.when(userRepository.save(Mockito.any(User.class))).thenAnswer(AdditionalAnswers.returnsFirstArg());
        assertDoesNotThrow(() -> usersService.deleteUserById(userId));
        Mockito.verify(userRepository, Mockito.times(1)).save(argument.capture());

        var deletedUser = argument.getValue();
        assertEquals(deletedUser.getId(),userId);
        assertNotEquals(deletedUser.getRemovedDate(),null);
        assertNotEquals(deletedUser.getTeacher().getRemovedDate(),null);
    }

    @Test
    public void deleteUserNotTeacherById_withoutException() {
        var argument = ArgumentCaptor.forClass(User.class);

        var userId = 13;
        var user = GradeoTestUtil.createBasicActiveUserWithId(userId);
        var user2 = GradeoTestUtil.createBasicActiveUserWithId(3322);
        Mockito.when(userRepository.findAll()).thenReturn(new ArrayList<>(Arrays.asList(user, user2)));
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        sessionService.expireUserSessions(user.getUsername());

        Mockito.when(userRepository.save(Mockito.any(User.class))).thenAnswer(AdditionalAnswers.returnsFirstArg());
        assertDoesNotThrow(() -> usersService.deleteUserById(userId));
        Mockito.verify(userRepository, Mockito.times(1)).save(argument.capture());

        var deletedUser = argument.getValue();
        assertEquals(deletedUser.getId(),userId);
        assertNotEquals(deletedUser.getRemovedDate(),null);
    }

    @Test
    public void deleteUser_with_LastUserException() {
        var mainUserId = 1;
        var mainUser = GradeoTestUtil.createBasicActiveUserWithId(mainUserId);
        Mockito.when(userRepository.findAllByRemovedDateIsNullOrderByIdAsc()).thenReturn(Collections.singleton(mainUser));
        assertThrows(LastUserException.class, () -> usersService.deleteUserById(mainUserId));
    }

    @Test
    public void deleteUser_with_NoSuchUserException() {
        var mainUserId = 33222233;
        var offUser1Id = 555;
        var offUser1 = GradeoTestUtil.createBasicActiveUserWithId(offUser1Id);
        var offUser2Id = 321321;
        var offUser2 = GradeoTestUtil.createBasicActiveUserWithId(offUser2Id);
        Mockito.when(userRepository.findAllByRemovedDateIsNullOrderByIdAsc()).thenReturn(Arrays.asList(offUser1, offUser2));
        Mockito.when(userRepository.findById(mainUserId)).thenReturn(Optional.empty());
        assertThrows(NoSuchUserException.class, () -> usersService.deleteUserById(mainUserId));
    }

    @Test
    public void deleteUser_with_NoSuchTeacherException() {
        var teacherId = 777;
        var mainUserId = 33222233;
        var mainUser = GradeoTestUtil.createBasicActiveUserWithId(mainUserId);
        var offUser1Id = 555;
        var offUser1 = GradeoTestUtil.createBasicActiveUserWithId(offUser1Id);
        var offUser2Id = 321321;
        var offUser2 = GradeoTestUtil.createBasicActiveUserWithId(offUser2Id);
        Teacher teacher = GradeoTestUtil.createBasicTeacherWithId(teacherId);
        mainUser.setTeacher(teacher);
        mainUser.getTeacher().setUser(mainUser);

        Mockito.when(userRepository.findAllByRemovedDateIsNullOrderByIdAsc()).thenReturn(Arrays.asList(offUser1, offUser2));
        Mockito.when(userRepository.findById(mainUserId)).thenReturn(Optional.of(mainUser));
        Mockito.when(teachersRepository.findById(mainUser.getTeacher().getId())).thenReturn(Optional.empty());
        assertThrows(NoSuchTeacherException.class, () -> usersService.deleteUserById(mainUserId));
    }

    @Test
    public void updateUser_withoutExceptions() {
        var argument = ArgumentCaptor.forClass(User.class);
        var teacherId = 37;
        var teacher = GradeoTestUtil.createBasicTeacherWithId(37);
        var userId = 22;
        var updateUserDto = GradeoTestUtil.createDefaultUpdateUserDtoWithUSerIdAndTeacherId(userId, teacherId);
        var userFromDb = GradeoTestUtil.createBasicActiveUserWithId(userId);
        var updatedDateFromDb = userFromDb.getUpdatedDate();
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(userFromDb));

        Mockito.when(userRepository.save(Mockito.any(User.class))).thenAnswer(AdditionalAnswers.returnsFirstArg());
        assertDoesNotThrow(() -> usersService.updateUser(updateUserDto));
        Mockito.verify(userRepository, Mockito.times(1)).save(argument.capture());

        var updatedUser = argument.getValue();

        assertEquals(updatedUser.getId(), updateUserDto.getId());
        assertEquals(updatedUser.getUsername(),updateUserDto.getUsername());
        assertNotEquals(updatedUser.getUpdatedDate(),updatedDateFromDb);
    }

    @Test
    public void updateUser_with_NoSuchUserException() {
        var userId = 13;
        var teacherId = 75;
        var updateUserDto = GradeoTestUtil.createDefaultUpdateUserDtoWithUSerIdAndTeacherId(userId, teacherId);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.empty());
        assertThrows(NoSuchUserException.class, () -> usersService.updateUser(updateUserDto));
    }
}
