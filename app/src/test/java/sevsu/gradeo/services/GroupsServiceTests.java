package sevsu.gradeo.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import sevsu.gradeo.dtos.groups.CreateGroupDto;
import sevsu.gradeo.dtos.groups.GroupIndexDto;
import sevsu.gradeo.dtos.groups.UpdateGroupDto;
import sevsu.gradeo.entities.*;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchDepartmentException;
import sevsu.gradeo.repositories.*;

import java.time.LocalDate;
import java.time.Month;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class GroupsServiceTests {
    // Сервис, который хотим протестировать
    @InjectMocks
    private GroupsService groupsService;

    // Его зависимости в виде тестовых моков
    // Моки это заглушки, классы с тем же интерфейсом, но без реализации
    @Mock
    private GroupsRepository groupsRepository;
    @Mock
    private DepartmentsRepository departmentsRepository;
    @Mock
    private TeachersRepository teachersRepository;
    @Mock
    private SubjectsRepository subjectsRepository;
    @Mock
    InstituteRepository instituteRepository;
    @Mock
    private Teacher teacher;

    // Spy используется когда нужно использовать реальную реализацию
    @Spy
    private ModelMapper mapper;


    private LocalDate date = LocalDate.of(2020, 01, 31);

    public GroupsServiceTests() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllGroups_shouldReturnGroupDtoList() {
        // Arrange phase
        Speciality speciality = new Speciality("Specialty1", "09.03.01");
        Department department = new Department(1, "ИТКС", null, null);
        List<Student> students = new ArrayList<Student>();


        Group[] groups = new Group[]{
                new Group(speciality, "numberGroup1", teacher, students, 1, date, department),
                new Group(speciality, "numberGroup2", teacher, students, 2, date, department),
                new Group(speciality, "numberGroup3", teacher, students, 3, date, department)
        };

        Mockito.doReturn(Arrays.asList(groups)).when(groupsRepository).findAllByRemovedDateIsNullOrderByIdAsc();

        // Act phase
        List<GroupIndexDto> dtos = groupsService.getAllGroups();

        // Assert phase
        Assertions.assertNotNull(dtos);
        Assertions.assertEquals(3, dtos.size());
        Assertions.assertEquals(1, dtos.get(0).getId());
        Assertions.assertEquals(groups[0].getName(), dtos.get(0).getName());
        Assertions.assertEquals(date, dtos.get(0).getFormedDate());
        Assertions.assertEquals(2, dtos.get(1).getId());
        Assertions.assertEquals(groups[1].getName(), dtos.get(1).getName());
        Assertions.assertEquals(date, dtos.get(1).getFormedDate());
        Assertions.assertEquals(3, dtos.get(2).getId());
        Assertions.assertEquals(groups[2].getName(), dtos.get(2).getName());
        Assertions.assertEquals(date, dtos.get(2).getFormedDate());
    }

    @Test
    public void createGroup_ShouldThrowException_WhenDepartmentIsNotFound() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        // Arrange phase
        var createDto = new CreateGroupDto();
        createDto.setName("Bar");
        createDto.setFormedDate(date);
        createDto.setDepartmentId(100);

        // Используем для захвата аргумента, т.к. мы его создаем динамически в методе, а не в моке
        ArgumentCaptor<Group> argument = ArgumentCaptor.forClass(Group.class);

        // Метод save возвращает то, что в него передали
        Mockito.doReturn(Optional.empty()).when(departmentsRepository).findById(100);

        // Act and Assert phases
        Assertions.assertThrows(NoSuchDepartmentException.class, () -> groupsService.createGroup(createDto));
    }

    @Test
    public void createGroup_ShouldCallSave_WhenDepartmentIsFound() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        // Arrange phase
        var createDto = new CreateGroupDto();
        createDto.setName("Bar");
        createDto.setFormedDate(date);
        createDto.setDepartmentId(100);
        createDto.setTeacherId(100);

        // Используем для захвата аргумента, т.к. мы его создаем динамически в методе, а не в моке
        ArgumentCaptor<Group> argument = ArgumentCaptor.forClass(Group.class);

        var returnedDepartment = new Department();
        returnedDepartment.setId(100);
        returnedDepartment.setName("Foo");

        var returnedTeacher = new Teacher();
        returnedTeacher.setId(100);
        returnedTeacher.setName("Hariton");

        Mockito.doReturn(Optional.of(returnedDepartment)).when(departmentsRepository).findById(100);
        Mockito.doReturn(Optional.of(returnedTeacher)).when(teachersRepository).findById(100);

        // Метод save возвращает то, что в него передали
        when(groupsRepository.save(any(Group.class))).thenAnswer(returnsFirstArg());

        // Act and Assert phases

        Assertions.assertDoesNotThrow(() -> groupsService.createGroup(createDto));
        verify(groupsRepository, times(1)).save(argument.capture());
        Group createdEntity = argument.getValue();
        Assertions.assertEquals("Bar", createdEntity.getName());
        Assertions.assertEquals(returnedDepartment, createdEntity.getDepartment());
    }

    @Test
    public void updateGroup_ShouldThrowAnException_WhenEntityNotFound() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        // Arrange phase
        var updateDto = new UpdateGroupDto();
        updateDto.setId(100);
        updateDto.setName("Foo");
        updateDto.setFormedDate(date);

        var returnedEntity = new Group();
        returnedEntity.setId(100);
        returnedEntity.setName("Foo");

        Mockito.doReturn(Optional.of(returnedEntity)).when(groupsRepository).findById(100);
        Mockito.doReturn(Optional.empty()).when(departmentsRepository).findById(100);

        // Act and Assert phase
        Assertions.assertThrows(EntityNotFoundException.class, () -> groupsService.updateGroup(updateDto));
    }

    @Test
    public void updateGroup_ShouldThrowAnException_WhenInstituteNotFound() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        // Arrange phase
        var updateDto = new UpdateGroupDto();
        updateDto.setId(100);
        updateDto.setName("Foo");

        Mockito.doReturn(Optional.empty()).when(groupsRepository).findById(100);
        Mockito.doReturn(Optional.empty()).when(departmentsRepository).findById(100);

        // Act and Assert phase
        Assertions.assertThrows(EntityNotFoundException.class, () -> groupsService.updateGroup(updateDto));
    }

    @Test
    public void updateGroup_ShouldCallSaveAndUpdateFields_WhenEntityIsFound() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        // Arrange phase
        var updateDto = new UpdateGroupDto();
        updateDto.setId(100);
        updateDto.setName("Bar");
        updateDto.setDepartmentId(100);

        var returnedEntity = new Group();
        returnedEntity.setId(100);
        returnedEntity.setName("Foo");

        Mockito.doReturn(Optional.of(returnedEntity)).when(groupsRepository).findById(100);
        Mockito.doReturn(returnedEntity).when(groupsRepository).save(returnedEntity);

        var returnedDepartment = new Department();
        returnedDepartment.setId(100);
        returnedDepartment.setName("Foo");

        Mockito.doReturn(Optional.of(returnedDepartment)).when(departmentsRepository).findById(100);

        // Act and Assert phases
        /*    *//*Assertions.assertDoesNotThrow(() -> groupsService.updateGroup(updateDto));*//*
        Assertions.assertEquals("Foo", returnedEntity.getName());
        Assertions.assertNotNull(returnedEntity.getUpdatedDate());
        verify(groupsRepository, times(1)).save(returnedEntity);*/
    }

    @Test
    public void updateGroup_WithoutException(){
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает
        var argument = ArgumentCaptor.forClass(Group.class);

        var updateGroupDto = new UpdateGroupDto();
        var id = 1;
        var department = new Department();
        department.setId(5);
        department.setName("dep");
        var institute = new Institute();
        institute.setId(2);
        institute.setName("ins");
        department.setInstitute(institute);
        var name = "group";
        var teacher = new Teacher();
        teacher.setId(1);
        var formedDate = LocalDate.of(2020, Month.APRIL, 2);
        var subjects = new HashSet<Subject>();
        var subject1 = new Subject();
        subject1.setId(1);
        subject1.setName("sub1");
        subjects.add(subject1);
        var subject2 = new Subject();
        subject2.setId(2);
        subject2.setName("sub2");
        subjects.add(subject2);

        updateGroupDto.setName(name);
        updateGroupDto.setId(id);
        updateGroupDto.setInstituteId(institute.getId());
        updateGroupDto.setInstituteName(institute.getName());
        updateGroupDto.setDepartmentId(department.getId());
        updateGroupDto.setDepartmentName(department.getName());
        updateGroupDto.setFormedDate(formedDate);
        updateGroupDto.setSubjectsId(subjects.stream().map(Subject::getId).toArray(Integer[]::new));
        updateGroupDto.setTeacherId(teacher.getId());

        var groupFromBd = new Group();
        groupFromBd.setId(id);
        Mockito.when(groupsRepository.findById(id)).thenReturn(Optional.of(groupFromBd));
        Mockito.when(teachersRepository.findById(teacher.getId())).thenReturn(Optional.of(teacher));
        Mockito.when(subjectsRepository.findById(subject1.getId())).thenReturn(Optional.of(subject1));
        Mockito.when(subjectsRepository.findById(subject2.getId())).thenReturn(Optional.of(subject2));
        Mockito.when(departmentsRepository.findById(department.getId())).thenReturn(Optional.of(department));
        Mockito.when(instituteRepository.findById(institute.getId())).thenReturn(Optional.of(institute));

        Mockito.when(groupsRepository.save(Mockito.any(Group.class))).thenAnswer(AdditionalAnswers.returnsFirstArg());
        assertDoesNotThrow(() -> groupsService.updateGroup(updateGroupDto));
        Mockito.verify(groupsRepository, Mockito.times(1)).save(argument.capture());

        var updatedGroup = argument.getValue();

        assertEquals(updatedGroup.getId(),id);
        assertEquals(updatedGroup.getName(),name);
        assertEquals(updatedGroup.getDepartment(),department);
        assertEquals(updatedGroup.getFormedDate(),formedDate);
        assertEquals(updatedGroup.getTeacher(),teacher);
        assertEquals(updatedGroup.getSubjects(),subjects);
    }

    @Test
    public void deleteGroup_ShouldThrowAnException_WhenEntityNotFound() {
        // Arrange phase
        Mockito.doReturn(Optional.empty()).when(groupsRepository).findById(100);

        // Act and Assert phases
        Assertions.assertThrows(EntityNotFoundException.class, () -> groupsService.deleteGroupById(100));
    }

    @Test
    public void deleteGroup_ShouldCallSaveAndUpdateFields_WhenEntityIsFound() {
        // Arrange phase
        var returnedEntity = new Group();
        returnedEntity.setId(100);
        returnedEntity.setName("Foo");
        returnedEntity.setFormedDate(date);

        Mockito.doReturn(Optional.of(returnedEntity)).when(groupsRepository).findById(100);
        Mockito.doReturn(returnedEntity).when(groupsRepository).save(returnedEntity);

        // Act and Assert phases
        Assertions.assertDoesNotThrow(() -> groupsService.deleteGroupById(100));
        Assertions.assertNotNull(returnedEntity.getRemovedDate());
        verify(groupsRepository, times(1)).save(returnedEntity);
    }

}

