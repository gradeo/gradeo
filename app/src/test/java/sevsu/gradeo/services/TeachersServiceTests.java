package sevsu.gradeo.services;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import sevsu.gradeo.dtos.teachers.CreateTeacherDto;
import sevsu.gradeo.dtos.teachers.UpdateTeacherDto;
import sevsu.gradeo.dtos.users.CreateUserDto;
import sevsu.gradeo.entities.*;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.EmailAlreadyExistsException;
import sevsu.gradeo.exceptions.entities.UserAlreadyExistsException;
import sevsu.gradeo.repositories.*;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
public class TeachersServiceTests {
    @InjectMocks
    private TeachersService teachersService;

    @Mock
    UserRepository userRepository;
    @Mock
    TeachersRepository teachersRepository;
    @Mock
    DepartmentsRepository departmentsRepository;
    @Mock
    SubjectsRepository subjectsRepository;
    @Mock
    GroupsRepository groupsRepository;

    @Spy
    ModelMapper mapper;
    @Spy
    BCryptPasswordEncoder bCryptPasswordEncoder;

    public TeachersServiceTests() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllTeachers_ShouldReturn_AListOfTeacherDto() {
        var department1 = new Department();
        department1.setId(1);
        department1.setName("dep1");
        var institute1 = new Institute();
        institute1.setId(1);
        institute1.setName("inst1");
        department1.setInstitute(institute1);
        var department2 = new Department();
        department2.setId(2);
        department2.setName("dep2");
        var institute2 = new Institute();
        institute2.setId(2);
        institute2.setName("inst2");
        department2.setInstitute(institute2);

        var subjects1 = new HashSet<Subject>();
        var subject1 = new Subject();
        subject1.setId(1);
        subject1.setName("sub1");
        subjects1.add(subject1);
        var subject2 = new Subject();
        subject2.setId(2);
        subject2.setName("sub2");
        subjects1.add(subject2);

        var subjects2 = new HashSet<Subject>();
        var subject3 = new Subject();
        subject3.setId(3);
        subject3.setName("sub3");
        subjects2.add(subject3);
        var subject4 = new Subject();
        subject4.setId(4);
        subject4.setName("sub4");
        subjects2.add(subject4);

        var groups1 = new HashSet<Group>();
        var group1 = new Group();
        group1.setId(1);
        group1.setName("group1");
        var group2 = new Group();
        group2.setId(2);
        group2.setName("group2");

        var groups2 = new HashSet<Group>();
        var group3 = new Group();
        group3.setId(3);
        group3.setName("group3");
        var group4 = new Group();
        group4.setId(4);
        group4.setName("group4");

        var user1 = new User();
        user1.setId(1);

        var user2 = new User();
        user2.setId(2);

        var teachers = new ArrayList<Teacher>();
        teachers.add(new Teacher(1, "Иванов Иван Иванович", Position.PROFESSOR,department1,subjects1,group1,user1));
        teachers.add(new Teacher(2, "Петров Петр Петрович", Position.PROFESSOR,department2,subjects2,group2,user2));

        Mockito.when(teachersRepository.findAllByRemovedDateIsNullOrderByIdAsc()).thenReturn(teachers);
        var dtos = teachersService.getAllTeachers();
        var dto1 = dtos.get(0);
        var dto2 = dtos.get(1);

        assertEquals(dto1.getDepartmentId(), department1.getId());
        assertEquals(dto1.getDepartmentName(),department1.getName());
        assertEquals(dto1.getPosition(),"Профессор");
        assertEquals(dto1.getId(),1);
        assertEquals(dto1.getName(),"Иванов Иван Иванович");

        assertEquals(dto2.getDepartmentId(), department2.getId());
        assertEquals(dto2.getDepartmentName(),department2.getName());
        assertEquals(dto2.getPosition(),"Профессор");
        assertEquals(dto2.getId(),2);
        assertEquals(dto2.getName(),"Петров Петр Петрович");
    }

    @Test
    public void createTeacher_without_Exception() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает

        var teacherArgumentCaptor = ArgumentCaptor.forClass(Teacher.class);
        var userArgumentCaptor = ArgumentCaptor.forClass(User.class);

        var createTeacherDto = new CreateTeacherDto();

        var department1 = new Department();
        department1.setId(1);
        department1.setName("dep1");
        var institute1 = new Institute();
        institute1.setId(1);
        institute1.setName("inst1");
        department1.setInstitute(institute1);
        var email = "email@help.me";

        Mockito.when(departmentsRepository.findById(1)).thenReturn(Optional.of(department1));

        var groups1 = new HashSet<Group>();
        var group1 = new Group();
        group1.setId(1);
        group1.setName("group1");
        groups1.add(group1);
        var group2 = new Group();
        group2.setId(2);
        group2.setName("group2");
        groups1.add(group2);

        Mockito.when(groupsRepository.findById(1)).thenReturn(Optional.of(group1));

        var subjects1 = new HashSet<Subject>();
        var subject1 = new Subject();
        subject1.setId(1);
        subject1.setName("sub1");
        subjects1.add(subject1);
        var subject2 = new Subject();
        subject2.setId(2);
        subject2.setName("sub2");
        subjects1.add(subject2);

        var teacherName = "Петров Петр Петрович";
        var position = Position.PROFESSOR;
        var password = "1234";
        var username = "testName";

        Teacher createdTeacher = new Teacher();
        User createdUser = new User();

        createTeacherDto.setEmail(email);
        createTeacherDto.setDepartmentId(department1.getId());
        createTeacherDto.setName(teacherName);
        createTeacherDto.setPassword(password);
        createTeacherDto.setPosition(position);
        createTeacherDto.setUsername(username);

        Mockito.when(teachersRepository.save(Mockito.any(Teacher.class))).thenAnswer(AdditionalAnswers.returnsFirstArg());
        assertDoesNotThrow(() -> teachersService.createTeacher(createTeacherDto));
        Mockito.verify(teachersRepository, Mockito.times(2)).save(teacherArgumentCaptor.capture());

        var createUserDto = new CreateUserDto();
        createUserDto.setUsername(username);
        createUserDto.setEmail(email);
        createUserDto.setPassword(password);
        createUserDto.setConfirmPassword(password);

        Mockito.when(userRepository.save(Mockito.any(User.class))).thenAnswer(AdditionalAnswers.returnsFirstArg());
        Mockito.verify(userRepository, Mockito.times(1)).save(userArgumentCaptor.capture());

        createdUser = userArgumentCaptor.getValue();

        createdTeacher = teacherArgumentCaptor.getValue();
        createdTeacher.setUser(createdUser);
        createdTeacher.setGroup(group1);
        createdUser.setTeacher(createdTeacher);

        assertSame(createdTeacher.getDepartment(), department1);
        assertSame(createdTeacher.getGroup(), group1);
        assertEquals(createdTeacher.getName(), teacherName);
        assertSame(createdTeacher.getUser(),createdUser);
        assertEquals(createdTeacher.getPosition(),position);
        assertEquals(createdUser.getEmail(),email);
        assertNotEquals(createdUser.getPassword(),password);
        Set<User.Role> roles = new HashSet<User.Role>(Collections.singleton(User.Role.TEACHER));
        assertEquals(createdUser.getRoles(),roles);
        assertEquals(createdUser.getTeacher(),createdTeacher);
        assertEquals(createdUser.getUsername(),username);
    }

    @Test
    public void createTeacher_with_NoSuchDepartmentException() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает
        var createDto = new CreateTeacherDto();
        Mockito.when(departmentsRepository.findById(Mockito.any(Integer.class))).thenReturn(Optional.empty());
//TODO:         assertThrows(NoSuchDepartmentException.class, () -> teachersService.createTeacher(createDto));
    }

    @Test
    public void createTeacher_with_UserAlreadyExistsException(){
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает
        var createDto = new CreateTeacherDto();
        createDto.setUsername("username");
        var user = new User();
        Mockito.when(userRepository.findByUsernameAndRemovedDateIsNull(Mockito.anyString())).thenReturn(user);
        assertThrows(UserAlreadyExistsException.class, () -> teachersService.createTeacher(createDto));
    }

    @Test
    public void createTeacher_with_EmailAlreadyExistsException() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает
        var createDto = new CreateTeacherDto();
        createDto.setEmail("email@email.email");
        var user = new User();
        Mockito.when(userRepository.findByEmailAndRemovedDateIsNull(Mockito.anyString())).thenReturn(user);
        assertThrows(EmailAlreadyExistsException.class, () -> teachersService.createTeacher(createDto));
    }

    @Test
    public void updateTeacher_without_Exception() {
        var argument = ArgumentCaptor.forClass(Teacher.class);

        var updateTeacherDto = new UpdateTeacherDto();
        // Создаем кафедру
        var department1 = new Department();
        department1.setId(1);
        department1.setName("dep1");
        // Создаем институт
        var institute1 = new Institute();
        institute1.setId(1);
        institute1.setName("inst1");
        department1.setInstitute(institute1);
        // Создаем группы
        var group1 = new Group();
        group1.setId(1);
        group1.setName("group1");
        var group2 = new Group();
        group2.setId(2);
        group2.setName("group2");

        // Создаем список предметов
        var subjects1 = new HashSet<Subject>();
        var subject1 = new Subject();
        subject1.setId(1);
        subject1.setName("sub1");
        subjects1.add(subject1);
        var subject2 = new Subject();
        subject2.setId(2);
        subject2.setName("sub2");
        subjects1.add(subject2);

        var teacherName = "Петров Петр Петрович";

        var position = Position.PROFESSOR;

        updateTeacherDto.setDepartmentId(department1.getId());
        //todo тесты групп (заведена задача)
        updateTeacherDto.setGroupId(group1.getId());
        updateTeacherDto.setSubjectsId(subjects1.stream().map(Subject::getId).toArray(Integer[]::new));
        updateTeacherDto.setName(teacherName);
        updateTeacherDto.setPosition(position);
        updateTeacherDto.setInstituteId(department1.getInstitute().getId());
        updateTeacherDto.setInstituteName(department1.getInstitute().getName());
        updateTeacherDto.setDepartmentName(department1.getName());
        updateTeacherDto.setId(5);

        var teacherFromDb = new Teacher();
        teacherFromDb.setId(5);

        Mockito.when(teachersRepository.findById(5)).thenReturn(Optional.of(teacherFromDb));
        Mockito.when(departmentsRepository.findById(1)).thenReturn(Optional.of(department1));
        Mockito.when(subjectsRepository.findById(1)).thenReturn(Optional.of(subject1));
        Mockito.when(subjectsRepository.findById(2)).thenReturn(Optional.of(subject2));
        Mockito.when(groupsRepository.findById(1)).thenReturn(Optional.of(group1));

        Mockito.when(teachersRepository.save(Mockito.any(Teacher.class))).thenAnswer(AdditionalAnswers.returnsFirstArg());
        assertDoesNotThrow(() -> teachersService.updateTeacher(updateTeacherDto));
        Mockito.verify(teachersRepository, Mockito.times(1)).save(argument.capture());

        Teacher updatedTeacher = argument.getValue();

        assertSame(updatedTeacher.getDepartment(), department1);
        assertSame(updatedTeacher.getGroup(), group1);
        assertEquals(updatedTeacher.getSubjects(),subjects1);
        assertEquals(updatedTeacher.getPosition(),position);
        assertEquals(updatedTeacher.getName(),teacherName);
        assertEquals(updatedTeacher.getId(),5);
    }

    @Test
    public void updateTeacher_with_EntityNotFoundException_teacherRepo() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает
        var updateDto = new UpdateTeacherDto();
        var department = new Department();
        var subject = new Subject();
        Mockito.when(teachersRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        Mockito.when(departmentsRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(department));
        Mockito.when(subjectsRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(subject));
        assertThrows(EntityNotFoundException.class, () -> teachersService.updateTeacher(updateDto));
    }

    @Test
    public void updateTeacher_with_EntityNotFoundException_departmentRepo() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает
        var updateDto = new UpdateTeacherDto();
        var subject = new Subject();
        var teacher = new Teacher();
        Mockito.when(subjectsRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(subject));
        Mockito.when(departmentsRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        Mockito.when(teachersRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(teacher));
        assertThrows(EntityNotFoundException.class, () -> teachersService.updateTeacher(updateDto));
    }

    @Test
    public void updateTeacher_with_EntityNotFoundException_subjectsRepo() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает
        var updateDto = new UpdateTeacherDto();
        var teacher = new Teacher();
        var department = new Department();
        var group = new Group();
        Mockito.when(teachersRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(teacher));
        Mockito.when(departmentsRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(department));
        Mockito.when(groupsRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(group));
        Mockito.when(subjectsRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class, () -> teachersService.updateTeacher(updateDto));
    }
}
