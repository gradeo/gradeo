package sevsu.gradeo.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import sevsu.gradeo.dtos.departments.CreateDepartmentDto;
import sevsu.gradeo.dtos.departments.DepartmentListItemDto;
import sevsu.gradeo.dtos.departments.UpdateDepartmentDto;
import sevsu.gradeo.entities.Department;
import sevsu.gradeo.entities.Institute;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchInstituteException;
import sevsu.gradeo.repositories.DepartmentsRepository;
import sevsu.gradeo.repositories.InstituteRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DepartmentsServiceTests {
    // Сервис, который хотим протестировать
    @InjectMocks
    private DepartmentsService departmentsService;

    // Его зависимости в виде тестовых моков
    // Моки это заглушки, классы с тем же интерфейсом, но без реализации
    @Mock
    private DepartmentsRepository departmentsRepository;
    @Mock
    private InstituteRepository instituteRepository;

    // Spy используется когда нужно использовать реальную реализацию
    @Spy
    private ModelMapper mapper;

    public DepartmentsServiceTests() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllDepartments_ShouldReturn_AListOfDepartments() {
        // Arrange phase
        Department[] entities = new Department[]{
                new Department(1, "Foo", null, null),
                new Department(2, "Bar", null, null)
        };

        Mockito.doReturn(Arrays.asList(entities)).when(departmentsRepository).findAllByRemovedDateIsNullOrderByIdAsc();

        // Act phase

        List<DepartmentListItemDto> dtos = departmentsService.getAllDepartments();

        // Assert phase
        Assertions.assertNotNull(dtos);
        Assertions.assertEquals(2, dtos.size());
        Assertions.assertEquals(1, dtos.get(0).getId());
        Assertions.assertEquals("Foo", dtos.get(0).getName());
        Assertions.assertEquals(0, dtos.get(0).getSubjectsCount());
        Assertions.assertEquals(0, dtos.get(0).getGroupsCount());
        Assertions.assertEquals(2, dtos.get(1).getId());
        Assertions.assertEquals("Bar", dtos.get(1).getName());
    }

    @Test
    public void createDepartment_ShouldThrowException_WhenInstituteIsNotFound() {
        // Arrange phase
        var createDto = new CreateDepartmentDto();
        createDto.setName("Bar");
        createDto.setInstituteId(100);

        // Метод save возвращает то, что в него передали
        Mockito.doReturn(Optional.empty()).when(instituteRepository).findById(100);

        // Act and Assert phases
        Assertions.assertThrows(NoSuchInstituteException.class, () -> departmentsService.createDepartment(createDto));
    }

    @Test
    public void createDepartment_ShouldCallSave_WhenInstituteIsFound() {
        // Arrange phase
        var createDto = new CreateDepartmentDto();
        createDto.setName("Bar");
        createDto.setInstituteId(100);

        // Используем для захвата аргумента, т.к. мы его создаем динамически в методе, а не в моке
        ArgumentCaptor<Department> argument = ArgumentCaptor.forClass(Department.class);

        var returnedInstitute = new Institute();
        returnedInstitute.setId(100);
        returnedInstitute.setName("Foo");

        Mockito.doReturn(Optional.of(returnedInstitute)).when(instituteRepository).findById(100);

        // Метод save возвращает то, что в него передали
        when(departmentsRepository.save(any(Department.class))).thenAnswer(returnsFirstArg());

        // Act and Assert phases
        Assertions.assertDoesNotThrow(() -> departmentsService.createDepartment(createDto));
        verify(departmentsRepository, times(1)).save(argument.capture());
        Department createdEntity = argument.getValue();
        Assertions.assertEquals("Bar", createdEntity.getName());
        Assertions.assertEquals(returnedInstitute, createdEntity.getInstitute());
    }

    @Test
    public void updateDepartment_ShouldThrowAnException_WhenEntityNotFound() {
        // Arrange phase
        var updateDto = new UpdateDepartmentDto();
        updateDto.setId(100);
        updateDto.setName("Foo");
        updateDto.setInstituteId(100);

        var returnedEntity = new Department();
        returnedEntity.setId(100);
        returnedEntity.setName("Foo");

        Mockito.doReturn(Optional.of(returnedEntity)).when(departmentsRepository).findById(100);
        Mockito.doReturn(Optional.empty()).when(instituteRepository).findById(100);

        // Act and Assert phase
        Assertions.assertThrows(EntityNotFoundException.class, () -> departmentsService.updateDepartment(updateDto));
    }

    @Test
    public void updateInstitute_ShouldThrowAnException_WhenInstituteNotFound() {
        // Arrange phase
        var updateDto = new UpdateDepartmentDto();
        updateDto.setId(100);
        updateDto.setName("Foo");

        Mockito.doReturn(Optional.empty()).when(departmentsRepository).findById(100);
        Mockito.doReturn(Optional.empty()).when(instituteRepository).findById(100);

        // Act and Assert phase
        Assertions.assertThrows(EntityNotFoundException.class, () -> departmentsService.updateDepartment(updateDto));
    }

    @Test
    public void updateInstitute_ShouldCallSaveAndUpdateFields_WhenEntityIsFound() {
        // Arrange phase
        var updateDto = new UpdateDepartmentDto();
        updateDto.setId(100);
        updateDto.setName("Bar");
        updateDto.setInstituteId(100);

        var returnedEntity = new Department();
        returnedEntity.setId(100);
        returnedEntity.setName("Foo");

        Mockito.doReturn(Optional.of(returnedEntity)).when(departmentsRepository).findById(100);
        Mockito.doReturn(returnedEntity).when(departmentsRepository).save(returnedEntity);

        var returnedInstitute = new Institute();
        returnedInstitute.setId(100);
        returnedInstitute.setName("Foo");

        Mockito.doReturn(Optional.of(returnedInstitute)).when(instituteRepository).findById(100);

        // Act and Assert phases
        Assertions.assertDoesNotThrow(() -> departmentsService.updateDepartment(updateDto));
        Assertions.assertEquals("Bar", returnedEntity.getName());
        Assertions.assertNotNull(returnedEntity.getUpdatedDate());
        verify(departmentsRepository, times(1)).save(returnedEntity);
    }

    @Test
    public void deleteInstitute_ShouldThrowAnException_WhenEntityNotFound() {
        // Arrange phase
        Mockito.doReturn(Optional.empty()).when(departmentsRepository).findById(100);

        // Act and Assert phases
        Assertions.assertThrows(EntityNotFoundException.class, () -> departmentsService.deleteDepartmentById(100));
    }

    @Test
    public void deleteDepartment_ShouldCallSaveAndUpdateFields_WhenEntityIsFound() {
        // Arrange phase
        var returnedEntity = new Department();
        returnedEntity.setId(100);
        returnedEntity.setName("Foo");

        Mockito.doReturn(Optional.of(returnedEntity)).when(departmentsRepository).findById(100);
        Mockito.doReturn(returnedEntity).when(departmentsRepository).save(returnedEntity);

        // Act and Assert phases
        Assertions.assertDoesNotThrow(() -> departmentsService.deleteDepartmentById(100));
        Assertions.assertNotNull(returnedEntity.getRemovedDate());
        verify(departmentsRepository, times(1)).save(returnedEntity);
    }
}
