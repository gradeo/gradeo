package sevsu.gradeo.services;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import sevsu.gradeo.dtos.students.CreateStudentDto;
import sevsu.gradeo.dtos.students.UpdateStudentDto;
import sevsu.gradeo.entities.Group;
import sevsu.gradeo.entities.Student;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchGroupException;
import sevsu.gradeo.repositories.GroupsRepository;
import sevsu.gradeo.repositories.StudentsRepository;
import sevsu.gradeo.repositories.TeachersRepository;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
public class StudentsServiceTests {
    @InjectMocks
    private StudentsService studentsService;
    @Mock
    private StudentsRepository studentsRepository;
    @Mock
    private GroupsRepository groupsRepository;
    @Mock
    private TeachersRepository teachersRepository;

    @Spy
    ModelMapper mapper;

    public StudentsServiceTests() {
        MockitoAnnotations.initMocks(this);
    }

    //Тест возврата списка всех студентов (не удаленных)
    @Test
    public void getAllStudents_ShouldReturn_AListOfStudentDto() {
        var students = new ArrayList<Student>();
        students.add(new Student(
                1, "name1", 1, 111111, "email1@email.ru",
                null, null, "очная",
                LocalDate.of(2020, Month.APRIL, 2),
                LocalDate.of(2042, Month.APRIL, 4)));
        students.add(new Student(
                2, "name2", 2, 222222, "email2@email.ru",
                null, null, "очная",
                LocalDate.of(2022, Month.APRIL, 3),
                LocalDate.of(2033, Month.APRIL, 5)));

        Mockito.when(studentsRepository.findAllByRemovedDateIsNullOrderByIdAsc()).thenReturn(students);

        var dtos = studentsService.getAllStudents();

        assertEquals(dtos.size(), 2);
        assertEquals(dtos.get(0).getId(), 1);
        assertEquals(dtos.get(1).getId(), 2);
        assertEquals(dtos.get(0).getName(), "name1");
        assertEquals(dtos.get(1).getName(), "name2");
        assertEquals(dtos.get(0).getGradeBook(), 111111);
        assertEquals(dtos.get(1).getGradeBook(), 222222);
        assertEquals(dtos.get(0).getEducationalForm(), "очная");
        assertEquals(dtos.get(1).getEducationalForm(), "очная");
        assertEquals(dtos.get(0).getEnrollmentDate(), LocalDate.of(2020, Month.APRIL, 2));
        assertEquals(dtos.get(1).getEnrollmentDate(), LocalDate.of(2022, Month.APRIL, 3));
        assertEquals(dtos.get(0).getDeductionDate(), LocalDate.of(2042, Month.APRIL, 4));
        assertEquals(dtos.get(1).getDeductionDate(), LocalDate.of(2033, Month.APRIL, 5));
    }

    @Test
    public void createStudent_without_Exception() {
        var argument = ArgumentCaptor.forClass(Student.class);
        var createDto = new CreateStudentDto();

        var group = new Group();
        group.setId(42);
        group.setName("test_group_name");

        Mockito.when(groupsRepository.findById(42)).thenReturn(Optional.of(group));

        mapper.getConfiguration().setAmbiguityIgnored(true);

        createDto.setGroupId(42);
        createDto.setDepartmentId(0);
        createDto.setInstituteId(0);
        createDto.setEnrollmentDate(LocalDate.of(2020, Month.APRIL, 2));
        createDto.setEducationalForm("очная");
        createDto.setGradeBook(332222);
        createDto.setName("Вася Пупкин");

        Mockito.when(studentsRepository.save(Mockito.any(Student.class))).thenAnswer(AdditionalAnswers.returnsFirstArg());
        assertDoesNotThrow(() -> studentsService.createStudent(createDto));
        Mockito.verify(studentsRepository, Mockito.times(1)).save(argument.capture());

        Student createdStudent = argument.getValue();
        assertSame(createdStudent.getGroup(), group);
        assertEquals(createdStudent.getEnrollmentDate(), LocalDate.of(2020, Month.APRIL, 2));
        assertEquals(createdStudent.getEducationalForm(), "очная");
        assertEquals(createdStudent.getGradeBook(), 332222);
        //TODO проверка на эмайл когда он будет реализован
        assertEquals(createdStudent.getName(), "Вася Пупкин");
    }

    @Test
    public void createStudent_with_NoSuchGroupException() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает

        var createDto = new CreateStudentDto();

        Mockito.when(groupsRepository.findById(Mockito.any(Integer.class))).thenReturn(Optional.empty());

        createDto.setGroupId(42);
        createDto.setDepartmentId(0);
        createDto.setInstituteId(0);
        createDto.setEnrollmentDate(LocalDate.of(2020, Month.APRIL, 2));
        createDto.setEducationalForm("очная");
        createDto.setGradeBook(332222);
        createDto.setName("Вася Пупкин");

        assertThrows(NoSuchGroupException.class, () -> studentsService.createStudent(createDto));
    }

    @Test
    public void updateStudent_without_Exception() {
        var argument = ArgumentCaptor.forClass(Student.class);
        var updateDto = new UpdateStudentDto();
        var studentFromDb = new Student();
        studentFromDb.setId(3);

        var group = new Group();
        group.setId(42);
        group.setName("test_group_name");

        Mockito.when(groupsRepository.findById(42)).thenReturn(Optional.of(group));

        Mockito.when(studentsRepository.findById(3)).thenReturn(Optional.of(studentFromDb));

        updateDto.setId(3);
        updateDto.setGroupId(42);
        updateDto.setDepartmentId(0);
        updateDto.setInstituteId(0);
        updateDto.setEnrollmentDate(LocalDate.of(2020, Month.APRIL, 2));
        updateDto.setEducationalForm("очная");
        updateDto.setGradeBook(332222);
        updateDto.setName("Вася Пупкин");
        updateDto.setDeductionDate(LocalDate.of(2042, Month.APRIL, 4));

        Mockito.when(studentsRepository.save(Mockito.any(Student.class))).thenAnswer(AdditionalAnswers.returnsFirstArg());

        assertDoesNotThrow(() -> studentsService.updateStudent(updateDto));

        Mockito.verify(studentsRepository, Mockito.times(1)).save(argument.capture());

        Student updatedStudent = argument.getValue();

        assertEquals(updatedStudent.getName(), "Вася Пупкин");
        assertEquals(updatedStudent.getId(), 3);
        assertEquals(updatedStudent.getGradeBook(), 332222);
        assertEquals(updatedStudent.getEnrollmentDate(), LocalDate.of(2020, Month.APRIL, 2));
        assertEquals(updatedStudent.getDeductionDate(),LocalDate.of(2042, Month.APRIL, 4));
        assertEquals(updatedStudent.getEducationalForm(), "очная");
        assertSame(updatedStudent.getGroup(), group);
    }

    @Test
    public void updateStudent_with_EntityNotFoundException() {
        var updateDto = new UpdateStudentDto();

        Mockito.when(studentsRepository.findById(3)).thenReturn(Optional.empty());

        updateDto.setId(3);
        updateDto.setGroupId(42);
        updateDto.setDepartmentId(0);
        updateDto.setInstituteId(0);
        updateDto.setEnrollmentDate(LocalDate.of(2020, Month.APRIL, 2));
        updateDto.setEducationalForm("очная");
        updateDto.setGradeBook(332222);
        updateDto.setName("Вася Пупкин");
        updateDto.setDeductionDate(LocalDate.of(2042, Month.APRIL, 4));

        assertThrows(EntityNotFoundException.class, () -> studentsService.updateStudent(updateDto));
    }

    @Test
    public void deleteStudent_without_Exceptions() {
        var argument = ArgumentCaptor.forClass(Student.class);

        var studentFromDb = new Student();
        studentFromDb.setId(3);

        Mockito.when(studentsRepository.findById(3)).thenReturn(Optional.of(studentFromDb));
        Mockito.when(studentsRepository.save(Mockito.any(Student.class))).thenAnswer(AdditionalAnswers.returnsFirstArg());
        assertDoesNotThrow(() -> studentsService.deleteStudentById(3));
        Mockito.verify(studentsRepository, Mockito.times(1)).save(argument.capture());
        System.out.println("remove time = " + argument.getValue().getRemovedDate());
    }

    @Test
    public void deleteStudent_with_EntityNotFoundException() {
        Mockito.when(studentsRepository.findById(3)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> studentsService.deleteStudentById(3));
    }
}
