package sevsu.gradeo.services;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import sevsu.gradeo.dtos.subjects.CreateSubjectDto;
import sevsu.gradeo.dtos.subjects.SubjectDto;
import sevsu.gradeo.dtos.subjects.UpdateSubjectDto;
import sevsu.gradeo.entities.*;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchDepartmentException;
import sevsu.gradeo.repositories.DepartmentsRepository;
import sevsu.gradeo.repositories.SubjectsRepository;
import sevsu.gradeo.repositories.TeachersRepository;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
public class SubjectsServiceTests {
    // Сервис, который хотим протестировать
    @InjectMocks
    private SubjectsService subjectsService;

    // Его зависимости в виде тестовых моков
    // Моки это заглушки, классы с тем же интерфейсом, но без реализации
    @Mock
    private SubjectsRepository subjectsRepository;
    @Mock
    private DepartmentsRepository departmentsRepository;
    @Mock
    private TeachersRepository teachersRepository;

    // Spy используется когда нужно использовать реальную реализацию
    @Spy
    private ModelMapper mapper;
    @Spy
    private EmailService emailService;

    public SubjectsServiceTests() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllSubjects_shouldReturnSubjectDtoList() {
        // Arrange phase
        Set<Teacher> teachers = new HashSet<>();
        Set<Group> groups = new HashSet<>();
        var department1 = new Department();
        department1.setName("foo");
        department1.setId(1);
        var department2 = new Department();
        department2.setName("bar");
        department2.setId(2);
        Subject[] subjects = new Subject[]{
                new Subject(1, "Foo", teachers, groups, department1),
                new Subject(2, "Bar", teachers, groups, department2)
        };

        Mockito.doReturn(Arrays.asList(subjects)).when(subjectsRepository).findAllByRemovedDateIsNullOrderByIdAsc();

        // Act phase
        List<SubjectDto> dtos = subjectsService.getAllSubjects();

        // Assert phase
        assertNotNull(dtos);
        assertEquals(2, dtos.size());
        assertEquals(1, dtos.get(0).getId());
        assertEquals("Foo", dtos.get(0).getName());
        assertEquals(2, dtos.get(1).getId());
        assertEquals("Bar", dtos.get(1).getName());
        assertEquals(1,dtos.get(0).getDepartmentId());
        assertEquals(2,dtos.get(1).getDepartmentId());
    }

    @Test
    public void createSubject_without_Exception() {

        mapper.getConfiguration().setAmbiguityIgnored(true);

        var argument = ArgumentCaptor.forClass(Subject.class);
        var createDto = new CreateSubjectDto();

        var institute = new Institute();
        institute.setId(1);
        var department = new Department();
        department.setId(1);
        department.setInstitute(institute);
        var teachers = new HashSet<Teacher>();
        var teacher1 = new Teacher();
        var teacher2 = new Teacher();
        teacher1.setId(1);
        teacher2.setId(2);
        teachers.add(teacher1);
        teachers.add(teacher2);
        Integer[] teacherIds = teachers.stream().map(teacher -> teacher.getId()).toArray(Integer[]::new);
        var subjectName = "Foo";

        createDto.setDepartmentId(department.getId());
        createDto.setInstituteId(department.getInstitute().getId());
        createDto.setName(subjectName);
        createDto.setTeachersId(teacherIds);

        Mockito.when(teachersRepository.findById(1)).thenReturn(Optional.of(teacher1));
        Mockito.when(teachersRepository.findById(2)).thenReturn(Optional.of(teacher2));
        Mockito.when(departmentsRepository.findById(1)).thenReturn(Optional.of(department));


        Mockito.when(subjectsRepository.save(Mockito.any(Subject.class))).thenAnswer(AdditionalAnswers.returnsFirstArg());
        assertDoesNotThrow(() -> subjectsService.createSubject(createDto));
        Mockito.verify(subjectsRepository, Mockito.times(1)).save(argument.capture());

        var createdSubject = argument.getValue();

        assertEquals(createdSubject.getName(), subjectName);
        assertSame(createdSubject.getDepartment(),department);
//        TODO присваивание предметов при создании группы (реализация после курсов)
//        assertSame(createdSubject.getGroups(),???);
        assertEquals(createdSubject.getTeachers(),teachers);
    }

    @Test
    public void createSubject_with_NoSuchDepartmentException() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает
        var createDto = new CreateSubjectDto();

        var institute = new Institute();
        institute.setId(1);
        var department = new Department();
        department.setId(1);
        department.setInstitute(institute);
        var teachers = new HashSet<Teacher>();
        var teacher1 = new Teacher();
        var teacher2 = new Teacher();
        teacher1.setId(1);
        teacher2.setId(2);
        teachers.add(teacher1);
        teachers.add(teacher2);
        Integer[] teacherIds = teachers.stream().map(Teacher::getId).toArray(Integer[]::new);
        var subjectName = "Foo";

        createDto.setDepartmentId(department.getId());
        createDto.setInstituteId(department.getInstitute().getId());
        createDto.setName(subjectName);
        createDto.setTeachersId(teacherIds);

        Mockito.when(departmentsRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        assertThrows(NoSuchDepartmentException.class, () -> subjectsService.createSubject(createDto));
    }

    @Test
    public void createSubject_with_EntityNotFoundException() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает

        var createDto = new CreateSubjectDto();

        var institute = new Institute();
        institute.setId(1);
        var department = new Department();
        department.setId(1);
        department.setInstitute(institute);
        var teachers = new HashSet<Teacher>();
        var teacher1 = new Teacher();
        var teacher2 = new Teacher();
        teacher1.setId(1);
        teacher2.setId(2);
        teachers.add(teacher1);
        teachers.add(teacher2);
        Integer[] teacherIds = teachers.stream().map(Teacher::getId).toArray(Integer[]::new);
        var subjectName = "Foo";

        createDto.setDepartmentId(department.getId());
        createDto.setInstituteId(department.getInstitute().getId());
        createDto.setName(subjectName);
        createDto.setTeachersId(teacherIds);

        Mockito.when(departmentsRepository.findById(1)).thenReturn(Optional.of(department));
        Mockito.when(teachersRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class, () -> subjectsService.createSubject(createDto));
    }

    @Test
    public void updateSubject_without_Exceptions() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает
        var argument = ArgumentCaptor.forClass(Subject.class);

        var updateDto = new UpdateSubjectDto();

        var id = 1;
        var department = new Department();
        department.setId(5);
        department.setName("foo");
        var institute = new Institute();
        institute.setId(2);
        institute.setName("bar");
        department.setInstitute(institute);
        var name = "SPO";

        var teachers = new HashSet<Teacher>();
        var teacher1 = new Teacher();
        var teacher2 = new Teacher();
        teacher1.setId(1);
        teacher2.setId(2);
        teachers.add(teacher1);
        teachers.add(teacher2);
        Integer[] teacherIds = teachers.stream().map(Teacher::getId).toArray(Integer[]::new);


        updateDto.setId(id);
        updateDto.setDepartmentId(department.getId());
        updateDto.setDepartmentName(department.getName());
        updateDto.setInstituteId(department.getInstitute().getId());
        updateDto.setInstituteName(department.getInstitute().getName());
        updateDto.setName(name);
        updateDto.setTeachersId(teacherIds);

        var subjectFromDb = new Subject();
        subjectFromDb.setId(1);

        Mockito.when(subjectsRepository.findById(1)).thenReturn(Optional.of(subjectFromDb));
        Mockito.when(departmentsRepository.findById(5)).thenReturn(Optional.of(department));
        Mockito.when(teachersRepository.findById(1)).thenReturn(Optional.of(teacher1));
        Mockito.when(teachersRepository.findById(2)).thenReturn(Optional.of(teacher2));

        Mockito.when(subjectsRepository.save(Mockito.any(Subject.class))).thenAnswer(AdditionalAnswers.returnsFirstArg());
        assertDoesNotThrow(() -> subjectsService.updateSubject(updateDto));
        Mockito.verify(subjectsRepository, Mockito.times(1)).save(argument.capture());

        var createdSubject = argument.getValue();

//        TODO присваивание предметов при редактировании группы (реализация после курсов)
//        assertEquals(createdSubject.getGroups(),???);
        assertEquals(createdSubject.getName(),name);
        assertEquals(createdSubject.getId(),id);
        assertEquals(createdSubject.getTeachers(),teachers);
        assertSame(createdSubject.getDepartment(),department);
    }

    @Test
    public void updateSubject_with_NoSuchDepartmentException() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает
        var updateDto = new UpdateSubjectDto();

        var id = 1;
        var department = new Department();
        department.setId(5);
        department.setName("foo");
        var institute = new Institute();
        institute.setId(2);
        institute.setName("bar");
        department.setInstitute(institute);
        var name = "SPO";

        var teachers = new HashSet<Teacher>();
        var teacher1 = new Teacher();
        var teacher2 = new Teacher();
        teacher1.setId(1);
        teacher2.setId(2);
        teachers.add(teacher1);
        teachers.add(teacher2);
        Integer[] teacherIds = teachers.stream().map(Teacher::getId).toArray(Integer[]::new);


        updateDto.setId(id);
        updateDto.setDepartmentId(department.getId());
        updateDto.setDepartmentName(department.getName());
        updateDto.setInstituteId(department.getInstitute().getId());
        updateDto.setInstituteName(department.getInstitute().getName());
        updateDto.setName(name);
        updateDto.setTeachersId(teacherIds);

        var subjectFromDb = new Subject();
        subjectFromDb.setId(1);

        Mockito.when(subjectsRepository.findById(1)).thenReturn(Optional.of(subjectFromDb));
        Mockito.when(departmentsRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        Mockito.when(teachersRepository.findById(1)).thenReturn(Optional.of(teacher1));
        Mockito.when(teachersRepository.findById(2)).thenReturn(Optional.of(teacher2));

        assertThrows(NoSuchDepartmentException.class, () -> subjectsService.updateSubject(updateDto));
    }

    @Test
    public void updateSubject_with_EntityNotFoundException_in_teachers() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает
        var updateDto = new UpdateSubjectDto();

        var id = 1;
        var department = new Department();
        department.setId(5);
        department.setName("foo");
        var institute = new Institute();
        institute.setId(2);
        institute.setName("bar");
        department.setInstitute(institute);
        var name = "SPO";

        var teachers = new HashSet<Teacher>();
        var teacher1 = new Teacher();
        var teacher2 = new Teacher();
        teacher1.setId(1);
        teacher2.setId(2);
        teachers.add(teacher1);
        teachers.add(teacher2);
        Integer[] teacherIds = teachers.stream().map(Teacher::getId).toArray(Integer[]::new);


        updateDto.setId(id);
        updateDto.setDepartmentId(department.getId());
        updateDto.setDepartmentName(department.getName());
        updateDto.setInstituteId(department.getInstitute().getId());
        updateDto.setInstituteName(department.getInstitute().getName());
        updateDto.setName(name);
        updateDto.setTeachersId(teacherIds);

        var subjectFromDb = new Subject();
        subjectFromDb.setId(1);

        Mockito.when(subjectsRepository.findById(1)).thenReturn(Optional.of(subjectFromDb));
        Mockito.when(departmentsRepository.findById(5)).thenReturn(Optional.of(department));
        Mockito.when(teachersRepository.findById(1)).thenReturn(Optional.empty());
        Mockito.when(teachersRepository.findById(2)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> subjectsService.updateSubject(updateDto));
    }

    @Test
    public void updateSubject_with_EntityNotFoundException_in_subject() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает
        var updateDto = new UpdateSubjectDto();

        var id = 1;
        var department = new Department();
        department.setId(5);
        department.setName("foo");
        var institute = new Institute();
        institute.setId(2);
        institute.setName("bar");
        department.setInstitute(institute);
        var name = "SPO";

        var teachers = new HashSet<Teacher>();
        var teacher1 = new Teacher();
        var teacher2 = new Teacher();
        teacher1.setId(1);
        teacher2.setId(2);
        teachers.add(teacher1);
        teachers.add(teacher2);
        Integer[] teacherIds = teachers.stream().map(Teacher::getId).toArray(Integer[]::new);


        updateDto.setId(id);
        updateDto.setDepartmentId(department.getId());
        updateDto.setDepartmentName(department.getName());
        updateDto.setInstituteId(department.getInstitute().getId());
        updateDto.setInstituteName(department.getInstitute().getName());
        updateDto.setName(name);
        updateDto.setTeachersId(teacherIds);

        var subjectFromDb = new Subject();
        subjectFromDb.setId(1);

        Mockito.when(subjectsRepository.findById(1)).thenReturn(Optional.empty());
        Mockito.when(departmentsRepository.findById(5)).thenReturn(Optional.of(department));
        Mockito.when(teachersRepository.findById(1)).thenReturn(Optional.of(teacher1));
        Mockito.when(teachersRepository.findById(2)).thenReturn(Optional.of(teacher2));

        assertThrows(EntityNotFoundException.class, () -> subjectsService.updateSubject(updateDto));
    }

    @Test
    public void sendEmail_without_Exceptions() {
        var studentFromDb = new Student();
        studentFromDb.setId(3);
        studentFromDb.setEmail("student@help.me");
        Mockito.doNothing().when(emailService).send(Mockito.anyString(), Mockito.anyString(), Mockito.anyString());

        assertDoesNotThrow(() -> emailService.send(studentFromDb.getEmail(), "TEST", "TEST"));
    }
}
