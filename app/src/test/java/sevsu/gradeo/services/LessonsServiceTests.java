package sevsu.gradeo.services;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import sevsu.gradeo.dtos.lessons.CreateLessonDto;
import sevsu.gradeo.dtos.lessons.UpdateLessonDto;
import sevsu.gradeo.entities.Group;
import sevsu.gradeo.entities.Lesson;
import sevsu.gradeo.entities.Subject;
import sevsu.gradeo.entities.Teacher;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchGroupException;
import sevsu.gradeo.exceptions.entities.NoSuchSubjectException;
import sevsu.gradeo.repositories.GroupsRepository;
import sevsu.gradeo.repositories.LessonsRepository;
import sevsu.gradeo.repositories.SubjectsRepository;
import sevsu.gradeo.repositories.TeachersRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
public class LessonsServiceTests {
    @InjectMocks
    private LessonsService lessonsService;

    @Mock
    private LessonsRepository lessonsRepository;
    @Mock
    private GroupsRepository groupsRepository;
    @Mock
    private SubjectsRepository subjectsRepository;
    @Mock
    private TeachersRepository teachersRepository;

    @Spy
    private ModelMapper mapper;

    public LessonsServiceTests() {
        MockitoAnnotations.initMocks(this);
    }

    //тест возврата списка всех уроков (не удаленных)
    @Test
    public void getAllLessons_ShouldReturn_AListOfLessonDto(){

        var lessons = new ArrayList<Lesson>();
        lessons.add(new Lesson(1, null, null,
                LocalDateTime.of(2020, Month.APRIL, 2, 17, 51),
                null, null, "test_auditorium_1", "test_lesson_1"));

        lessons.add(new Lesson(42, null, null,
                LocalDateTime.of(2042, Month.APRIL, 4, 2, 42),
                null, null, "test_auditorium_2", "test_lesson_2"));

        //так как LessonsService под возвратом всех уроков подразумевает только не удаленные
        //то репозиторию описываем соответсвующее поведение
        Mockito.when(lessonsRepository.findAllByRemovedDateIsNullOrderByIdAsc()).thenReturn(lessons);

        var dtos = lessonsService.getAllLessons();

        assertEquals(2, dtos.size());
        assertEquals(dtos.get(0).getId(),1);
        assertEquals(dtos.get(1).getId(),42);
        assertEquals(dtos.get(0).getDate(), LocalDateTime.of(2020, Month.APRIL,2,17,51));
        assertEquals(dtos.get(1).getDate(), LocalDateTime.of(2042, Month.APRIL,4,2,42));
        assertEquals(dtos.get(0).getAuditorium(), "test_auditorium_1");
        assertEquals(dtos.get(1).getAuditorium(), "test_auditorium_2");
        assertEquals(dtos.get(0).getName(), "test_lesson_1");
        assertEquals(dtos.get(1).getName(), "test_lesson_2");
    }

    @Test
    public void createLesson_without_Exception(){

        var argument = ArgumentCaptor.forClass(Lesson.class);

        var createDto = new CreateLessonDto();

        var group = new Group();
        group.setId(42);
        group.setName("test_group_name");

        Mockito.when(groupsRepository.findById(42)).thenReturn(Optional.of(group));

        var subject = new Subject();
        subject.setId(24);
        subject.setName("test_subject_name");

        Mockito.when(subjectsRepository.findById(24)).thenReturn(Optional.of(subject));

        var teacher = new Teacher();
        teacher.setId(12);
        teacher.setName("test_teacher_name");

        Mockito.when(teachersRepository.findById(12)).thenReturn(Optional.of(teacher));

        mapper.getConfiguration().setAmbiguityIgnored(true);

        createDto.setGroupId(42);
        createDto.setSubjectId(24);
        createDto.setDate(LocalDate.of(2020, Month.APRIL, 2));
        createDto.setTime(LocalTime.of(17, 51));
        createDto.setAuditorium("B-214");
        createDto.setDepartmentId(0);
        createDto.setInstituteId(0);
        createDto.setTeacherId(12);

        Mockito.when(lessonsRepository.save(Mockito.any(Lesson.class))).thenAnswer(AdditionalAnswers.returnsFirstArg());

        assertDoesNotThrow(() -> lessonsService.createLesson(createDto));

        Mockito.verify(lessonsRepository, Mockito.times(1)).save(argument.capture());

        Lesson createdLesson = argument.getValue();
        assertSame(createdLesson.getGroup(),group);
        assertSame(createdLesson.getTeacher(),teacher);
        assertSame(createdLesson.getSubject(),subject);
        assertEquals(createdLesson.getDate(), LocalDateTime.of(2020, Month.APRIL,2,17,51));
        assertEquals(createdLesson.getName(),subject.getName());
        assertEquals(createdLesson.getAuditorium(),"B-214");

    }

    @Test
    public void createLesson_with_NoSuchSubjectException(){

        var createDto = new CreateLessonDto();

        var group = new Group();
        group.setId(42);
        group.setName("test_group_name");

        Mockito.when(groupsRepository.findById(42)).thenReturn(Optional.of(group));

        Mockito.when(subjectsRepository.findById(Mockito.any(Integer.class))).thenReturn(Optional.empty());

        mapper.getConfiguration().setAmbiguityIgnored(true);

        createDto.setGroupId(42);
        createDto.setSubjectId(24);
        createDto.setDate(LocalDate.of(2020, Month.APRIL, 2));
        createDto.setTime(LocalTime.of(17, 51));
        createDto.setAuditorium("B-214");
        createDto.setDepartmentId(0);
        createDto.setInstituteId(0);

        assertThrows(NoSuchSubjectException.class, () -> lessonsService.createLesson(createDto));
    }

    @Test
    public void createLesson_with_NoSuchGroupException(){

        var createDto = new CreateLessonDto();

        Mockito.when(groupsRepository.findById(Mockito.any(Integer.class))).thenReturn(Optional.empty());

        var subject = new Subject();
        subject.setId(24);
        subject.setName("test_subject_name");

        Mockito.when(subjectsRepository.findById(24)).thenReturn(Optional.of(subject));

        mapper.getConfiguration().setAmbiguityIgnored(true);

        createDto.setGroupId(42);
        createDto.setSubjectId(24);
        createDto.setDate(LocalDate.of(2020, Month.APRIL, 2));
        createDto.setTime(LocalTime.of(17, 51));
        createDto.setAuditorium("B-214");
        createDto.setDepartmentId(0);
        createDto.setInstituteId(0);

        assertThrows(NoSuchGroupException.class, () -> lessonsService.createLesson(createDto));
    }

    @Test
    public void updateLesson_without_Exception(){
        var argument = ArgumentCaptor.forClass(Lesson.class);

        var updateDto = new UpdateLessonDto();

        var lessonFromDb = new Lesson();
        lessonFromDb.setId(3);

        Mockito.when(lessonsRepository.findById(3)).thenReturn(Optional.of(lessonFromDb));

        updateDto.setId(3);
        updateDto.setGroupId(42);
        updateDto.setSubjectId(24);
        updateDto.setDate(LocalDate.of(2020, Month.APRIL, 2));
        updateDto.setTime(LocalTime.of(17, 51));
        updateDto.setAuditorium("B-214");
        updateDto.setDepartmentId(0);
        updateDto.setInstituteId(0);

        Mockito.when(lessonsRepository.save(Mockito.any(Lesson.class))).thenAnswer(AdditionalAnswers.returnsFirstArg());

        assertDoesNotThrow(() -> lessonsService.updateLesson(updateDto));

        Mockito.verify(lessonsRepository, Mockito.times(1)).save(argument.capture());

        Lesson updatedLesson = argument.getValue();
        assertEquals(updatedLesson.getAuditorium(),"B-214");
        assertEquals(updatedLesson.getDate(), LocalDateTime.of(2020, Month.APRIL,2,17,51));

    }

    @Test
    public void updateLesson_with_EntityNotFoundException(){
        var updateDto = new UpdateLessonDto();

        Mockito.when(lessonsRepository.findById(3)).thenReturn(Optional.empty());

        updateDto.setId(3);
        updateDto.setGroupId(42);
        updateDto.setSubjectId(24);
        updateDto.setDate(LocalDate.of(2020, Month.APRIL, 2));
        updateDto.setTime(LocalTime.of(17, 51));
        updateDto.setAuditorium("B-214");
        updateDto.setDepartmentId(0);
        updateDto.setInstituteId(0);

        assertThrows(EntityNotFoundException.class, () -> lessonsService.updateLesson(updateDto));
    }

    @Test
    public void deleteLesson_without_Exception(){
        var argument = ArgumentCaptor.forClass(Lesson.class);

        var lessonFromDb = new Lesson();
        lessonFromDb.setId(3);

        Mockito.when(lessonsRepository.findById(3)).thenReturn(Optional.of(lessonFromDb));

        Mockito.when(lessonsRepository.save(Mockito.any(Lesson.class))).thenAnswer(AdditionalAnswers.returnsFirstArg());

        assertDoesNotThrow(() -> lessonsService.deleteLessonById(3));

        Mockito.verify(lessonsRepository, Mockito.times(1)).save(argument.capture());

        System.out.println("remove time = " + argument.getValue().getRemovedDate());
    }

    @Test
    public void deleteLesson_with_EntityNotFoundException(){
        Mockito.when(lessonsRepository.findById(3)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonsService.deleteLessonById(3));
    }
}

