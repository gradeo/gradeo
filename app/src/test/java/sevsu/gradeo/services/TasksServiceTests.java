package sevsu.gradeo.services;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import sevsu.gradeo.dtos.tasks.CreateTaskDto;
import sevsu.gradeo.dtos.tasks.UpdateTaskDto;
import sevsu.gradeo.entities.Subject;
import sevsu.gradeo.entities.Task;
import sevsu.gradeo.repositories.SubjectsRepository;
import sevsu.gradeo.repositories.TaskRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class TasksServiceTests {
    @InjectMocks
    private TasksService tasksService;
    @Mock
    private SubjectsRepository subjectsRepository;
    @Mock
    private TaskRepository taskRepository;
    @Spy
    private ModelMapper modelMapper;

    public TasksServiceTests() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createTask_withOutException(){
        var argument = ArgumentCaptor.forClass(Task.class);

        var createTaskDto = new CreateTaskDto();
        var name = "task";
        var subject = new Subject();
        var subjectId = 1;
        subject.setId(subjectId);
        var taskType = Task.Type.PRACTICAL;

        createTaskDto.setType(taskType);
        createTaskDto.setSubjectId(subjectId);
        createTaskDto.setName(name);

        Mockito.when(subjectsRepository.findById(subjectId)).thenReturn(Optional.of(subject));

        Mockito.when(taskRepository.save(Mockito.any(Task.class))).thenAnswer(AdditionalAnswers.returnsFirstArg());
        assertDoesNotThrow(() -> tasksService.createTask(createTaskDto));
        Mockito.verify(taskRepository, Mockito.times(1)).save(argument.capture());

        var createdTask = argument.getValue();

        assertEquals(createdTask.getName(), name);
        assertEquals(createdTask.getSubject(), subject);
        assertEquals(createdTask.getType(),taskType);
    }

    @Test
    public void updateTask_WithOutException(){
        var argument = ArgumentCaptor.forClass(Task.class);

        var updateTaskDto = new UpdateTaskDto();
        var name = "task";
        var subject = new Subject();
        var subjectId = 1;
        subject.setId(subjectId);
        var taskType = Task.Type.PRACTICAL;

        var taskId = 1;

        var taskFromDb = new Task();
        taskFromDb.setId(taskId);

        Mockito.when(taskRepository.findById(taskId)).thenReturn(Optional.of(taskFromDb));

        updateTaskDto.setId(taskId);
        updateTaskDto.setType(taskType);
        updateTaskDto.setSubjectId(subjectId);
        updateTaskDto.setName(name);

        Mockito.when(subjectsRepository.findById(subjectId)).thenReturn(Optional.of(subject));

        Mockito.when(taskRepository.save(Mockito.any(Task.class))).thenAnswer(AdditionalAnswers.returnsFirstArg());
        assertDoesNotThrow(() -> tasksService.updateTask(updateTaskDto));
        Mockito.verify(taskRepository, Mockito.times(1)).save(argument.capture());

        var updatedTask = argument.getValue();

        assertEquals(updatedTask.getName(), name);
        assertEquals(updatedTask.getSubject(), subject);
        assertEquals(updatedTask.getType(),taskType);
    }
}
