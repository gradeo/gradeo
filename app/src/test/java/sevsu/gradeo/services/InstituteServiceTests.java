package sevsu.gradeo.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import sevsu.gradeo.dtos.institutes.CreateInstituteDto;
import sevsu.gradeo.dtos.institutes.InstituteListItemDto;
import sevsu.gradeo.dtos.institutes.UpdateInstituteDto;
import sevsu.gradeo.entities.Department;
import sevsu.gradeo.entities.Institute;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.repositories.InstituteRepository;

import java.util.*;

import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class InstituteServiceTests {
    // Сервис, который хотим протестировать
    @InjectMocks
    private InstituteService instituteService;

    // Его зависимости в виде тестовых моков
    // Моки это заглушки, классы с тем же интерфейсом, но без реализации
    @Mock
    private InstituteRepository instituteRepository;

    // Spy используется когда нужно использовать реальную реализацию
    @Spy
    private ModelMapper mapper;

    public InstituteServiceTests() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllInstitutes_ShouldReturn_AListOfInstitutes() {
        // Arrange phase
        Institute[] entities = new Institute[]{
                new Institute(1, "Foo", Set.of(
                        new Department(1, "Foo", null, null),
                        new Department(2, "Bar", null, null)
                )),
                new Institute(2, "Bar", new HashSet<>()),
        };

        Mockito.doReturn(Arrays.asList(entities)).when(instituteRepository).findAllByRemovedDateIsNullOrderByIdAsc();

        instituteService.configureMappings();

        // Act phase
        List<InstituteListItemDto> dtos = instituteService.getAllInstitutes();

        // Assert phase
        Assertions.assertNotNull(dtos);
        Assertions.assertEquals(2, dtos.size());
        Assertions.assertEquals(1, dtos.get(0).getId());
        Assertions.assertEquals("Foo", dtos.get(0).getName());
        Assertions.assertEquals(2, dtos.get(0).getDepartmentsCount());
        Assertions.assertEquals(2, dtos.get(1).getId());
        Assertions.assertEquals("Bar", dtos.get(1).getName());
        Assertions.assertEquals(0, dtos.get(1).getDepartmentsCount());
    }

    @Test
    public void createInstitute_ShouldCallSave() {
        // Arrange phase
        var createDto = new CreateInstituteDto();
        createDto.setName("Bar");

        // Используем для захвата аргумента, т.к. мы его создаем динамически в методе, а не в моке
        ArgumentCaptor<Institute> argument = ArgumentCaptor.forClass(Institute.class);

        // Метод save возвращает то, что в него передали
        when(instituteRepository.save(any(Institute.class))).thenAnswer(returnsFirstArg());

        // Act phase
        instituteService.createInstitute(createDto);

        // Assert phase
        verify(instituteRepository, times(1)).save(argument.capture());
        Institute createdEntity = argument.getValue();
        Assertions.assertEquals("Bar", createdEntity.getName());
    }

    @Test
    public void updateInstitute_ShouldThrowAnException_WhenEntityNotFound() {
        // Arrange phase
        var updateDto = new UpdateInstituteDto();
        updateDto.setId(100);
        updateDto.setName("Foo");

        Mockito.doReturn(Optional.empty()).when(instituteRepository).findById(100);

        // Act and Assert phase
        Assertions.assertThrows(EntityNotFoundException.class, () -> instituteService.updateInstitute(updateDto));
    }

    @Test
    public void updateInstitute_ShouldCallSaveAndUpdateFields_WhenEntityIsFound() {
        // Arrange phase
        var updateDto = new UpdateInstituteDto();
        updateDto.setId(100);
        updateDto.setName("Bar");

        var returnedEntity = new Institute();
        returnedEntity.setId(100);
        returnedEntity.setName("Foo");

        Mockito.doReturn(Optional.of(returnedEntity)).when(instituteRepository).findById(100);
        Mockito.doReturn(returnedEntity).when(instituteRepository).save(returnedEntity);

        // Act and Assert phases
        Assertions.assertDoesNotThrow(() -> instituteService.updateInstitute(updateDto));
        Assertions.assertEquals("Bar", returnedEntity.getName());
        Assertions.assertNotNull(returnedEntity.getUpdatedDate());
        verify(instituteRepository, times(1)).save(returnedEntity);
    }

    @Test
    public void deleteInstitute_ShouldThrowAnException_WhenEntityNotFound() {
        // Arrange phase
        Mockito.doReturn(Optional.empty()).when(instituteRepository).findById(100);

        // Act and Assert phases
        Assertions.assertThrows(EntityNotFoundException.class, () -> instituteService.deleteInstituteById(100));
    }

    @Test
    public void deleteInstitute_ShouldCallSaveAndUpdateFields_WhenEntityIsFound() {
        // Arrange phase
        var returnedEntity = new Institute();
        returnedEntity.setId(100);
        returnedEntity.setName("Foo");

        Mockito.doReturn(Optional.of(returnedEntity)).when(instituteRepository).findById(100);
        Mockito.doReturn(returnedEntity).when(instituteRepository).save(returnedEntity);

        // Act and Assert phases
        Assertions.assertDoesNotThrow(() -> instituteService.deleteInstituteById(100));
        Assertions.assertNotNull(returnedEntity.getRemovedDate());
        verify(instituteRepository, times(1)).save(returnedEntity);
    }
}
