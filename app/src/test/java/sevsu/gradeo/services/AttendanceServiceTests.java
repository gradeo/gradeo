package sevsu.gradeo.services;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import sevsu.gradeo.dtos.attendance.CreateUpdateAttendanceDto;
import sevsu.gradeo.entities.*;
import sevsu.gradeo.repositories.AttendanceRepository;
import sevsu.gradeo.repositories.GroupsRepository;
import sevsu.gradeo.repositories.LessonsRepository;
import sevsu.gradeo.repositories.StudentsRepository;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
public class AttendanceServiceTests {
    @InjectMocks
    private AttendanceService attendanceService;
    @Mock
    private LessonsRepository lessonsRepository;
    @Mock
    private GroupsRepository groupsRepository;
    @Mock
    private StudentsRepository studentsRepository;
    @Mock
    private AttendanceRepository attendanceRepository;
    @Spy
    private ModelMapper mapper;

    AttendanceServiceTests() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createAttendance_without_Exception() {
        mapper.getConfiguration().setAmbiguityIgnored(true);

        var argument = ArgumentCaptor.forClass(Attendance.class);
        var createDto = new CreateUpdateAttendanceDto();

        var group = new Group();
        group.setId(1);
        var lesson = new Lesson();
        lesson.setId(2);
        var students = new ArrayList<Student>();
        var student1 = new Student();
        student1.setId(3);
        student1.setName("Foo");
        var student2 = new Student();
        student2.setId(4);
        student2.setName("Bar");
        var student3 = new Student();
        student3.setId(34);
        student3.setName("Kar");
        var student4 = new Student();
        student4.setId(43);
        student4.setName("Par");
        students.add(student1);
        students.add(student2);
        var studentsInGroup = new ArrayList<Student>();
        studentsInGroup.add(student1);
        studentsInGroup.add(student2);
        studentsInGroup.add(student3);
        studentsInGroup.add(student4);
        var studentsId = students.stream().map(Student::getId).toArray(Integer[]::new);
        var studentsNames = students.stream().map(Student::getName).toArray(String[]::new);
        var subject = new Subject();
        subject.setId(5);

        createDto.setSubjectId(subject.getId());
        createDto.setStudentsName(studentsNames);
        createDto.setStudentsId(studentsId);
        createDto.setLessonId(lesson.getId());
        createDto.setGroupId(group.getId());

        Mockito.when(studentsRepository.findById(3)).thenReturn(Optional.of(student1));
        Mockito.when(studentsRepository.findById(4)).thenReturn(Optional.of(student2));
        Mockito.when(studentsRepository.findById(34)).thenReturn(Optional.of(student3));
        Mockito.when(studentsRepository.findById(43)).thenReturn(Optional.of(student4));
        Mockito.when(studentsRepository.findAllByGroupIdAndRemovedDateIsNull(group.getId()))
                .thenReturn(studentsInGroup);
        Mockito.when(lessonsRepository.findById(lesson.getId())).thenReturn(Optional.of(lesson));
        Mockito.when(attendanceRepository.findAllByLessonId(lesson.getId())).thenReturn(new ArrayList<Attendance>());

        assertDoesNotThrow(() -> attendanceService.handleAttendanceDto(createDto));
        Mockito.verify(attendanceRepository, Mockito.times(4)).save(argument.capture());

        var createdAttendances = argument.getAllValues();
        assertSame(createdAttendances.get(0).getStudent(), student1);
        assertSame(createdAttendances.get(0).getLesson(), lesson);
        assertEquals(createdAttendances.get(0).getAttendanceStatus(), AttendanceStatus.PRESENCE);
        assertSame(createdAttendances.get(1).getStudent(), student2);
        assertSame(createdAttendances.get(1).getLesson(), lesson);
        assertEquals(createdAttendances.get(1).getAttendanceStatus(), AttendanceStatus.PRESENCE);
        assertSame(createdAttendances.get(2).getStudent(), student3);
        assertSame(createdAttendances.get(2).getLesson(), lesson);
        assertEquals(createdAttendances.get(2).getAttendanceStatus(), AttendanceStatus.ABSENCE);
        assertSame(createdAttendances.get(3).getStudent(), student4);
        assertSame(createdAttendances.get(3).getLesson(), lesson);
        assertEquals(createdAttendances.get(3).getAttendanceStatus(), AttendanceStatus.ABSENCE);
    }
}
