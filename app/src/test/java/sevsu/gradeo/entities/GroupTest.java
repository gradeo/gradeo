package sevsu.gradeo.entities;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

public class GroupTest {
    LocalDate date = LocalDate.parse("2020-01-31", DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    private Group group1 = new Group(null, "42", null, null, 42, date, null);
    private Group group2 = new Group(null, "42", null, null, 42, date, null);
    private Group group3 = null;
    private Group group4 = null;
    private Group group5 = group1;
    private Group group6 = new Group(null, "4123", null, null, 12, date, null);


    @Test
    void testGroup() {
        // сравниваем на правильность выражения в скобках
        assertTrue(group1.equals(group2));

        // проверка на равенство объектов
        assertEquals(group1, group2);
        assertEquals(group3, group4);

        // проверка на неравенство объектов
        assertNotEquals(group1, group4);
        assertNotEquals(group2, group6);

        // проверка на то, что объект не null
        assertNotNull(group1);
        assertNotNull(group5);
        assertNotNull(group6);
        // проверка на то, что оба объекта ссылаются на один
        assertSame(group1, group5);

        // проверка на то, что оба объекта не ссылаются на один
        assertNotSame(group1, group3);

    }

}  //проверка билда