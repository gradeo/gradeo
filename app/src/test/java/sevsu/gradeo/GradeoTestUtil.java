package sevsu.gradeo;

import sevsu.gradeo.dtos.users.UpdateUserDto;
import sevsu.gradeo.entities.Position;
import sevsu.gradeo.entities.Teacher;
import sevsu.gradeo.entities.User;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;

public class GradeoTestUtil {

    private static final LocalDate date1 = LocalDate.of(2020, 1, 1);
    private static final LocalDateTime dateTime1 = LocalDateTime.of(2020, Month.JANUARY, 1,0,0,0);

    public static User createBasicActiveUserWithId(Integer id){
        var user = new User();
        user.setActive(true);
        user.setUsername("userName");
        user.setEmail("useremail@mail.ru");
        user.setId(id);
        user.setCreatedDate(dateTime1);
        return user;
    }

    public static Teacher createBasicTeacherWithId(Integer id){
        var teacher = new Teacher();
        teacher.setId(id);
        teacher.setName("teacherName");
        teacher.setPosition(Position.PROFESSOR);
        return teacher;
    }

    public static UpdateUserDto createDefaultUpdateUserDtoWithUSerIdAndTeacherId(Integer userId, Integer teacherId){
        var updateUserDto = new UpdateUserDto();
        updateUserDto.setId(userId);
        updateUserDto.setPassword("updateDtoPassword");
        updateUserDto.setUsername("updateDtoUsername");
        updateUserDto.setTeacherId(teacherId);
        return updateUserDto;
    }
}
