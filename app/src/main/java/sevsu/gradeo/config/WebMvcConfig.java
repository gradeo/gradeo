package sevsu.gradeo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import sevsu.gradeo.interceptors.TimeMeasurementInterceptor;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(
                "/img/**",
                "/css/**",
                "/js/**",
                "/fonts",
                "/templates/**")
                .addResourceLocations(
                        "classpath:/static/img/",
                        "classpath:/static/css/",
                        "classpath:/static/js/",
                        "classpath:/static/fonts",
                        "classpath:/templates/");
    }

    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/auth/sign-in").setViewName("Login");
    }
    //привязывает интерцепторы к приложению
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //интерцептор логирует время до обработки, после обработки или после завершения запросов
        registry.addInterceptor(new TimeMeasurementInterceptor());
    }
}