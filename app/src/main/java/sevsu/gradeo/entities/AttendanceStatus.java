package sevsu.gradeo.entities;

public enum AttendanceStatus { //фактор присутсвия
    PRESENCE,
    ABSENCE,
    LATE
}
