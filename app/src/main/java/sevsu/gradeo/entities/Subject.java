package sevsu.gradeo.entities;

import lombok.*;
import sevsu.gradeo.entities.interfaces.SelectListable;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "subjects")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
public class Subject extends BaseEntity implements SelectListable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id = 0;

    @NonNull
    private String name;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "teachers_subjects",
            joinColumns = @JoinColumn(name = "subject_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "teacher_id", referencedColumnName = "id"))
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Teacher> teachers; //список преподавателей, ведущих предмет

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "groups_subjects",
            joinColumns = @JoinColumn(name = "subject_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "group_id", referencedColumnName = "id"))
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Group> groups; //список групп, у которых есть этот предмет ???

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "department_id", nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Department department;

    public Integer[] getAllTeachersId() {
        return teachers.stream().map(Teacher::getId).toArray(Integer[]::new);
    }

    public String[] getAllTeachersNames() {
        return teachers.stream().map(Teacher::getName).toArray(String[]::new);
    }
}
