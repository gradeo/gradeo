package sevsu.gradeo.entities.interfaces;

public interface SelectListable {
    Integer getId();
    String getName();
}
