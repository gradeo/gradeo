package sevsu.gradeo.entities.interfaces;

public interface EmailSender {
    void send(String email, String subject, String body);
}
