package sevsu.gradeo.entities.interfaces;

public interface SelectListableDateTime extends SelectListable {
    String getDateAsString();
}
