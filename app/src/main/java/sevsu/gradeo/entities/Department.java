package sevsu.gradeo.entities;

import lombok.*;
import org.hibernate.annotations.Where;
import sevsu.gradeo.entities.interfaces.SelectListable;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Данный класс описывает сущность "Кафедра"
 */
@Entity
@Table(name = "departments")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
public class Department extends BaseEntity implements SelectListable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id = 0;

    @NonNull
    private String name;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "institute_id", nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Institute institute;

    @OneToMany(mappedBy = "department")
    @Where(clause = "removed_date is null")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Group> groups = new HashSet<>();

}
