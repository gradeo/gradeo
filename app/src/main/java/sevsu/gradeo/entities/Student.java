package sevsu.gradeo.entities;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;
import sevsu.gradeo.entities.interfaces.SelectListable;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "students")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
public class Student extends BaseEntity implements SelectListable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id = 0;

    @NonNull
    private String name;

    @Transient
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private int course; // номер курса

    @JoinColumn(columnDefinition = "grade_book")
    private Integer gradeBook; //номер зачётной книжки

    @Transient
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private String email;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "group_id", nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Group group;

    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Attendance> attendances; //список всех посещаемостей

    private String educationalForm;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private LocalDate enrollmentDate = null; // дата зачисления

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private LocalDate deductionDate = null; // дата отчисления

}
