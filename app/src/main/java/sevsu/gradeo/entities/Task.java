package sevsu.gradeo.entities;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "tasks")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
public class Task extends BaseEntity {

    public enum Type {
        LABORATORY,
        PRACTICAL,
        COURSE,
        TEST,
        GRADUATE,
        PAYMENT_AND_GRAPHIC,
        ESSAY,
        SPECIFIC
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Task.Type type;

    @NonNull
    private String name;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "subject_id", nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Subject subject;

}
