package sevsu.gradeo.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "speciality")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Speciality extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private String name; //название специальности
    private String specialityCode; //код группы, к примеру 09.03.04

    public Speciality() {
    }

    public Speciality(String name, String specialityCode) {
        this.name = name;
        this.specialityCode = specialityCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCodeOfGroup() {
        return specialityCode;
    }

    public void setCodeOfGroup(String specialityCode) {
        this.specialityCode = specialityCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Speciality speciality = (Speciality) o;
        return Objects.equals(name, speciality.name) &&
                Objects.equals(specialityCode, speciality.specialityCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, specialityCode);
    }

    @Override
    public String toString() {
        return "Speciality{" +
                "name='" + name + '\'' +
                ", specialityCode='" + specialityCode + '\'' +
                '}';
    }
}
