package sevsu.gradeo.entities;

public enum Position {
    ASSISTANT,
    PROFESSOR,
    SENIOR_LECTURER,
    ASSISTANT_PROFESSOR
}
