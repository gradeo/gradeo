package sevsu.gradeo.entities;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;
import sevsu.gradeo.entities.interfaces.SelectListable;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "groups")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
public class Group extends BaseEntity implements SelectListable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @Transient
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Speciality speciality;

    @NonNull
    private String name;

    private String numberGroup;

    @OneToOne(targetEntity = Teacher.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "teacher_id")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Teacher teacher; //куратор

    @Transient // почему?
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private List<Student> students; //студенты в группе

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private LocalDate formedDate;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "department_id", nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Department department;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "groups_subjects",
            joinColumns = @JoinColumn(name = "group_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "subject_id", referencedColumnName = "id"))
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Subject> subjects; //предметы, изучаемые группой

    public Group(Speciality speciality, String numberGroup, Teacher teacher, List<Student> students,
                 Integer id, LocalDate formedDate, Department department) {
        this.speciality = speciality;
        this.numberGroup = numberGroup;
        this.teacher = teacher;
        this.students = students;
        this.id = id;
        this.formedDate = formedDate;
        this.department = department;
    }

    public Integer[] getAllSubjectsId() {
        if (subjects == null||subjects.isEmpty()){
            return new Integer[0];
        } else {
            return subjects.stream().map(Subject::getId).toArray(Integer[]::new);
        }
    }

    public String[] getAllSubjectsNames() {
        return subjects.stream().map(Subject::getName).toArray(String[]::new);
    }
}
