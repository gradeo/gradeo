package sevsu.gradeo.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

@MappedSuperclass
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class BaseEntity {
    private LocalDateTime createdDate; // объекты класса LocalDateTime
    private LocalDateTime updatedDate; // которые мы будем вызывать при создании, редактировании
    private LocalDateTime removedDate; // и удалении своих объектов, чтобы зафиксировать время.
}
