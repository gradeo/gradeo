package sevsu.gradeo.entities;

import lombok.*;
import sevsu.gradeo.entities.interfaces.SelectListable;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "teacher")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
public class Teacher extends BaseEntity implements SelectListable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @NonNull
    private String name;

    @Enumerated(EnumType.STRING)
    private Position position;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "department_id", nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Department department;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "teachers_subjects",
            joinColumns = @JoinColumn(name = "teacher_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "subject_id", referencedColumnName = "id"))
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Subject> subjects; // профильные предметы

    @OneToOne(targetEntity = Group.class, fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "group_id")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Group group; // курируемые группы

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private User user;

    public Integer[] getAllSubjectsId() {
        return subjects.stream().map(Subject::getId).toArray(Integer[]::new);
    }
}
