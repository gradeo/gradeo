package sevsu.gradeo.entities;

import lombok.*;
import sevsu.gradeo.entities.interfaces.SelectListableDateTime;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;

@Entity
@Table(name = "lessons")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
public class Lesson extends BaseEntity implements SelectListableDateTime {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id = 0;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "subject_id", nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Subject subject;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "group_id", nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Group group;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private LocalDateTime date;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "teacher_id", nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Teacher teacher;

    @OneToMany(mappedBy = "lesson", cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Attendance> attendance;

    private String auditorium;

    @NonNull
    private String name;     //имя = названию предмета

    @Override
    public String getDateAsString() {
        return date.format(
                DateTimeFormatter.ofPattern("dd-MM-YYYY HH:mm")
        );
    }

    public String[] getAllPresenceStudents() {
        return attendance
                .stream()
                .filter(a -> a.getAttendanceStatus() == AttendanceStatus.PRESENCE)
                .map(a -> a.getStudent().getName())
                .toArray(String[]::new);
    }

    public String[] getAllAbsenceStudents() {
        return attendance
                .stream()
                .filter(a -> a.getAttendanceStatus() == AttendanceStatus.ABSENCE)
                .map(a -> a.getStudent().getName())
                .toArray(String[]::new);
    }

    public LocalDate ldtToLocalDate() {
        return this.date.toLocalDate();
    }

    public LocalTime ldtToLocalTime() {
        return this.date.toLocalTime();
    }
}
