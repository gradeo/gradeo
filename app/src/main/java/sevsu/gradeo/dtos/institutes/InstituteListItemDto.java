package sevsu.gradeo.dtos.institutes;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InstituteListItemDto {
    private int id;
    private String name;
    private int departmentsCount;
}
