package sevsu.gradeo.dtos.institutes;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

@Getter
@Setter
public class UpdateInstituteDto {
    private int id;
    @Size(min = 2, message = "Введите правильное название института")
    private String name;
}
