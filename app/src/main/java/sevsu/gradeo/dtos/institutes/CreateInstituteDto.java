package sevsu.gradeo.dtos.institutes;

import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class CreateInstituteDto {
    @Size(min = 2, message = "Введите правильное название института")
    private String name;
}
