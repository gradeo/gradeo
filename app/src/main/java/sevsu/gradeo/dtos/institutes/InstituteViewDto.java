package sevsu.gradeo.dtos.institutes;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Size;

@Getter
@Setter
@ToString
public class InstituteViewDto {
    private int id;
    private String name;
}