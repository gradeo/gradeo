package sevsu.gradeo.dtos.tasks;

import lombok.Getter;
import lombok.Setter;
import sevsu.gradeo.entities.Task;

@Getter
@Setter
public class TaskDto {
    private Integer id;
    private String name;
    private Task.Type type;
    private Integer subjectId;
}
