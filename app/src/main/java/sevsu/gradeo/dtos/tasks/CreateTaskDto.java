package sevsu.gradeo.dtos.tasks;

import lombok.Getter;
import lombok.Setter;
import sevsu.gradeo.entities.Task;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class CreateTaskDto {
    @Size(min = 2, message = "Введите задание")
    private String name;
    @NotNull(message = "Выберите тип задания")
    private Task.Type type;
    @NotNull
    private Integer subjectId;
}
