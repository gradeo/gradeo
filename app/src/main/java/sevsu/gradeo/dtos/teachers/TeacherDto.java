package sevsu.gradeo.dtos.teachers;

import lombok.Getter;
import lombok.Setter;
import sevsu.gradeo.entities.Position;

@Getter
@Setter
public class TeacherDto {
    private int id;
    private String name;
    private int departmentId;
    private String departmentName;
    private String position;
    private Integer groupId;
    private Integer[] subjectsId;

    public void setPosition(Position position){
        switch (position) {
            case ASSISTANT:
                this.position = "Ассистент";
                break;
            case SENIOR_LECTURER:
                this.position = "Старший преподаватель";
                break;
            case ASSISTANT_PROFESSOR:
                this.position = "Доцент";
                break;
            case PROFESSOR:
                this.position = "Профессор";
                break;
            default:
                this.position = "";
        }
    }
}
