package sevsu.gradeo.dtos.teachers;

import lombok.Getter;
import lombok.Setter;
import sevsu.gradeo.entities.Position;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CreateTeacherDto {
    @NotBlank(message = "Введите ФИО")
    private String name;
    @NotBlank(message = "Введите логин")
    private String username;
    @NotBlank(message = "Введите пароль")
    private String password;
    @NotNull(message = "Выберите институт")
    private Integer instituteId;
    @NotNull(message = "Выберите кафедру")
    private Integer departmentId;
    @NotNull(message = "Выберите должность")
    private Position position;
    @NotBlank(message = "Введите email")
    private String email;

}
