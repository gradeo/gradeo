package sevsu.gradeo.dtos.teachers;

import lombok.Getter;
import lombok.Setter;
import sevsu.gradeo.entities.Position;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class UpdateTeacherDto {
    private int id;
    @NotBlank(message = "Введите Ф.И.О.")
    private String name;
    @NotNull(message = "Выберите институт")
    private Integer instituteId;
    private String instituteName;
    @NotNull(message = "Выберите кафедру")
    private Integer departmentId;
    private String departmentName;
    @NotNull(message = "Выберите должность")
    private Position position;
    /* Мультиселекс передает value выбранных групп*/
    private Integer groupId;
    /* @NotNull(message = "Выберите профильные предметы")*/ /*тоже самое*/
    private Integer[] subjectsId;

}
