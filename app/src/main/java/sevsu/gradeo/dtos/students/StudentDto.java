package sevsu.gradeo.dtos.students;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class StudentDto {
    private long id;
    private String name;
    private Integer instituteId;
    private Integer departmentId;
    private Integer groupId;
    private String groupName;
    private Integer gradeBook;
    private LocalDate enrollmentDate;
    private LocalDate deductionDate;
    private String educationalForm;
}
