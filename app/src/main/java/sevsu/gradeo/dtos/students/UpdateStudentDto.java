package sevsu.gradeo.dtos.students;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Data
public class UpdateStudentDto {
    private int id;
    @Size(min = 5, message = "ФИО не должно быть пустым")
    private String name;
    @NotNull(message = "Введите правильный номер зачетной книжки")
    @Digits(integer = 6, fraction = 0, message = "Введите правильный номер зачетной книжки")
    private Integer gradeBook;
    @NotNull(message = "Выберите институт")
    private Integer instituteId;
    private String instituteName;
    @NotNull(message = "Выберите кафедру")
    private Integer departmentId;
    private String departmentName;
    @NotNull(message = "Выберите группу")
    private Integer groupId;
    private String groupName;
    @NotNull(message = "Выберите дату зачисления")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate enrollmentDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deductionDate;
    @NotBlank(message = "Выберите форму обучения")
    private String educationalForm;
}
