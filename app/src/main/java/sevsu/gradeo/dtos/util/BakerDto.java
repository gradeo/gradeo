package sevsu.gradeo.dtos.util;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BakerDto {
    private String name;
    private String role;
    private String email;
}
