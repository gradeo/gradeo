package sevsu.gradeo.dtos.util;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SelectListItemDto {
    private String text;
    private String value;
}
