package sevsu.gradeo.dtos.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Класс-dto, который содержит в себе список преобразованных и отсортированных Dto,
 * а также метаданные странц
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EntityPageDto<K> {
    List<K> entityListItemDto;
    int totalPages;
    int currentPage;
    int start;
    int end;
}
