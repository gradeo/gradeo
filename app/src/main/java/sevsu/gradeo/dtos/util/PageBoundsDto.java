package sevsu.gradeo.dtos.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Класс-граница для страницы вывода сущностей
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PageBoundsDto {
    int start;
    int end;
}
