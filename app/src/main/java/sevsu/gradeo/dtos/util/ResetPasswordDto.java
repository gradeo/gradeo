package sevsu.gradeo.dtos.util;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class ResetPasswordDto {
    @NotBlank(message = "Пароль не может быть пустым")
    private String password;
    @NotBlank(message = "Подтвердите пароль")
    private String confirmPassword;
    private String code;
}
