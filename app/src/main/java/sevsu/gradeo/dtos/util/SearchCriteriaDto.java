package sevsu.gradeo.dtos.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import sevsu.gradeo.searchFilters.SearchOperations;
import sevsu.gradeo.searchFilters.SearchSpecifications;

/**
 * Класс-dto для сравнения исходных и переданных значений
 *
 * @see SearchSpecifications#toPredicate
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SearchCriteriaDto {
    /**
     * Обозначает название поля в исходной сущности
     */
    private String key;
    /**
     * Обозначает переданное значение для сравнения
     */
    private Object value;
    /**
     * Обозначает переданную поисковую операцию
     */
    private SearchOperations operation;

    /**
     * Нужен для поиска сущностей по переданному значению isDeleted
     */
    public SearchCriteriaDto(String key, SearchOperations operation) {
        this.key = key;
        this.operation = operation;
    }
}
