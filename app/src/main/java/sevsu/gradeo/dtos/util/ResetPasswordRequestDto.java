package sevsu.gradeo.dtos.util;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class ResetPasswordRequestDto {
    @NotBlank(message = "Поле не может быть пустым")
    private String email;
}
