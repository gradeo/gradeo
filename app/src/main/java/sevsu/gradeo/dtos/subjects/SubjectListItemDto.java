package sevsu.gradeo.dtos.subjects;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubjectListItemDto {
    private int id;
    private String name;
}
