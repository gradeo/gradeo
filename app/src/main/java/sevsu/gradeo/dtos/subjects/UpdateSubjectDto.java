package sevsu.gradeo.dtos.subjects;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class UpdateSubjectDto {
    private int id;
    @Size(min = 2, message = "Введите название предмета")
    private String name;
    @NotNull(message = "Выберите институт")
    private Integer instituteId;
    private String instituteName;
    @NotNull(message = "Выберите кафедру")
    private Integer departmentId;
    private String departmentName;
    private Integer[] teachersId;
}
