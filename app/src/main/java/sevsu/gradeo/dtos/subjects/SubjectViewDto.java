package sevsu.gradeo.dtos.subjects;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SubjectViewDto {
    private Integer id;
    private String name;
    private Integer instituteId;
    private Integer departmentId;
    private Integer[] teachersId;
}
