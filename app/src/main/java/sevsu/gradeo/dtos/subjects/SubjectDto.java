package sevsu.gradeo.dtos.subjects;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubjectDto {
    private long id;
    private Integer departmentId;
    private String name;
}