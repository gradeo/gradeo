package sevsu.gradeo.dtos.lessons;

import java.time.LocalDateTime;

public class CalendarLessonDto {
    String title;
    LocalDateTime start;
    LocalDateTime end;
    String url;

    public String getTitle() {
        return title;
    }

    public void setTitle(String tittle) {
        this.title = tittle;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end.plusMinutes(90);
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(Integer id) {
        this.url = "lessons/view/" + id;
    }
}
