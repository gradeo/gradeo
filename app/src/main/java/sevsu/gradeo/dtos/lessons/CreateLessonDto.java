package sevsu.gradeo.dtos.lessons;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
public class CreateLessonDto {
    @NotNull(message = "Выберите институт")
    private Integer instituteId;

    @NotNull(message = "Выберите кафедру")
    private Integer departmentId;

    @NotNull(message = "Выберите предмет")
    private Integer subjectId;

    @NotNull(message = "Выберите группу")
    private Integer groupId;

    @NotNull(message = "Выберите дату проведения занятия")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    @NotNull(message = "Выберите время проведения занятия")
    @DateTimeFormat(pattern = "HH:mm")
    private LocalTime time;

    @NotNull(message = "Введите аудиторию")
    private String auditorium;

    @NotNull(message = "Выберите преподавателя")
    private Integer teacherId;
}
