package sevsu.gradeo.dtos.lessons;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class LessonReportDto implements Serializable {

    private String subjectName;
    private String teacherName;
    private String date;
    private String groupName;
    private String[] presenceStudents;
    private String[] absenceStudents;


}