package sevsu.gradeo.dtos.lessons;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.time.LocalDateTime;

@Getter
@Setter
public class LessonDto {
    private int id;
    private String name;
    private Integer instituteId;
    private String instituteName;
    private Integer departmentId;
    private String departmentName;
    private Integer subjectId;
    private String subjectName;
    private Integer groupId;
    private String groupName;
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime date;
    private String auditorium;

    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime createdDate;
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime updatedDate;
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime removedDate;

}