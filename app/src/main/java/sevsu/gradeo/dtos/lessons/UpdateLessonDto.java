package sevsu.gradeo.dtos.lessons;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
public class UpdateLessonDto {
    private int id;
    private Integer instituteId;
    private String instituteName;
    private Integer departmentId;
    private String departmentName;
    private Integer subjectId;
    private String subjectName;
    private Integer groupId;
    private String groupName;

    @NotNull(message = "Выберите дату проведения занятия")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    @NotNull(message = "Выберите время проведения занятия")
    @DateTimeFormat(pattern = "HH:mm")
    private LocalTime time;

    @NotNull(message = "Введите аудиторию")
    private String auditorium;
}
