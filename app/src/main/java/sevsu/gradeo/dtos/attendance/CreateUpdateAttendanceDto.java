package sevsu.gradeo.dtos.attendance;

import lombok.Data;

@Data
public class CreateUpdateAttendanceDto {
    private Integer subjectId;
    private Integer lessonId;
    private Integer groupId;
    private Integer[] studentsId;
    private String[] studentsName;
}
