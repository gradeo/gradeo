package sevsu.gradeo.dtos.attendance;

import lombok.Data;
import sevsu.gradeo.entities.AttendanceStatus;

@Data
public class AttendanceDto {
    private Integer subjectId;
    private String subjectName;
    private Integer lessonId;
    private String lessonName;
    private Integer groupId;
    private String groupName;
    private Integer studentId;
    private String studentName;
    private boolean attendanceStatus;

    public void setAttendanceStatus(AttendanceStatus attendanceStatus) {
        switch (attendanceStatus) {
            case PRESENCE:
                this.attendanceStatus = true;
                break;
            case ABSENCE:
                this.attendanceStatus = false;
                break;
        }
    }
}
