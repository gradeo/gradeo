package sevsu.gradeo.dtos.departments;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DepartmentsViewDto {
    private Integer id;
    private String name;
    private Integer instituteId;
}
