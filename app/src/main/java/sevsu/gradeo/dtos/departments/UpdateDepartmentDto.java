package sevsu.gradeo.dtos.departments;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class UpdateDepartmentDto {
    private int id;
    @NotBlank(message = "Название кафедры не должно быть пустым")
    private String name;
    @NotNull(message = "Необходимо выбрать институт")
    private Integer instituteId;
}
