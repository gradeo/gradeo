package sevsu.gradeo.dtos.departments;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DepartmentPageDto {
    List<DepartmentListItemDto> departmentListItemDtos;
    int totalPages;
    int currentPage;
    int start;
    int end;
}
