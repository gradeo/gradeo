package sevsu.gradeo.dtos.departments;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DepartmentListItemDto {
    private int id;
    private String name;
    private int subjectsCount;
    private int groupsCount;
}
