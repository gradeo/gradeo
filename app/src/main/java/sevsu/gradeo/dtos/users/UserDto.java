package sevsu.gradeo.dtos.users;

import lombok.Getter;
import lombok.Setter;
import sevsu.gradeo.entities.User;

import java.util.Set;

@Getter
@Setter
public class UserDto {
    private int id;
    private String username;
    private String password;
    private String email;
    private Set<User.Role> roles;
}
