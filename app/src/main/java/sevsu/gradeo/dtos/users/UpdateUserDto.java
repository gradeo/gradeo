package sevsu.gradeo.dtos.users;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UpdateUserDto {
    private int id;
    @NotBlank(message = "Введите логин")
    private String username;
    @NotBlank(message = "Введите пароль")
    private String password;
    private int teacherId;
}
