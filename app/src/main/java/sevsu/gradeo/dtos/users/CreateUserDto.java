package sevsu.gradeo.dtos.users;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CreateUserDto {
    @NotBlank(message = "Введите логин")
    private String username;
    @NotBlank(message = "Введите email")
    private String email;
    @NotBlank(message = "Введите пароль")
    private String password;
    @NotBlank(message = "Подтвердите пароль")
    private String confirmPassword;
}
