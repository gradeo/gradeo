package sevsu.gradeo.dtos.assessments;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AssessmentDto {
    private Integer id;
    private Integer studentId;
    private String studentName;
    private Integer score;
}
