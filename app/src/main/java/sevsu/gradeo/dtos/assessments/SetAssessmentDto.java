package sevsu.gradeo.dtos.assessments;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SetAssessmentDto {
    @NotNull
    private Integer subjectId;
    @NotNull
    private Integer taskId;
    @NotNull(message = "Выбирите студента")
    private Integer studentId;
    @NotNull(message = "Поставьте оценку")
    @Min(0)
    @Max(100)
    private Integer score;
}
