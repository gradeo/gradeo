package sevsu.gradeo.dtos.assessments;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SetAssessmentDtoByTaskAndStudent extends SetAssessmentDto {
}
