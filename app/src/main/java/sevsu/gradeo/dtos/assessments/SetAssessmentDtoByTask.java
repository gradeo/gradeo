package sevsu.gradeo.dtos.assessments;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SetAssessmentDtoByTask extends SetAssessmentDto {
    @NotNull(message = "Выбирите группу")
    private String groupId;
}
