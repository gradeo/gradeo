package sevsu.gradeo.dtos.groups;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Getter
@Setter
public class UpdateGroupDto {
    private int id;
    @Size(min = 2, message = "Введите правильное название группы")
    private String name;
    @NotNull(message = "Введите правильную дату")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate formedDate;
    @NotNull(message = "Необходимо выбрать институт")
    private Integer instituteId;
    private String instituteName;
    @NotNull(message = "Необходимо выбрать кафедру")
    private Integer departmentId;
    private String departmentName;
    @NotNull(message = "Необходимо выбрать куратора")
    private Integer teacherId;

    private Integer[] subjectsId;
}
