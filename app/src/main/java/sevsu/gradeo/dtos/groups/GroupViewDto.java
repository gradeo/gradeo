package sevsu.gradeo.dtos.groups;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class GroupViewDto {
    private int id;
    private String name;
    private String instituteName;
    private String departmentName;
    private String teacherName;
    private LocalDate formedDate;
    private String[] subjectNames;
}