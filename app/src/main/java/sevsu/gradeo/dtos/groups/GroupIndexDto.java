package sevsu.gradeo.dtos.groups;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class GroupIndexDto {
    private int id;
    private String name;
    private LocalDate formedDate;
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime removedDate; // заменить при добавлении функционала даты фактического расформирования
}
