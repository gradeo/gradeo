package sevsu.gradeo.dtos.groups;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Data
public class CreateGroupDto {
    @Size(min = 2, message = "Введите правильное название группы")
    private String name;
    @NotNull(message = "Введите правильную дату")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate formedDate;
    @NotNull(message = "Необходимо выбрать институт")
    private Integer instituteId;
    @NotNull(message = "Необходимо выбрать кафедру")
    private Integer departmentId;
    @NotNull(message = "Необходимо выбрать куратора")
    private Integer teacherId;
}
