package sevsu.gradeo.searchFilters;

public enum SearchOperations {
    EQUAL_ID,

    EQUAL_NAME,
    CONTAIN_IN_NAME,
    SEARCH_BY_REMOVED_DATE_NOT_NULL,
    SEARCH_BY_REMOVED_DATE_NULL,

    SEARCH_BY_INSTITUTE_ID
}