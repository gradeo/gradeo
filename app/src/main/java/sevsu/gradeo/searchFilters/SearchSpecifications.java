package sevsu.gradeo.searchFilters;

import org.springframework.data.jpa.domain.Specification;
import sevsu.gradeo.dtos.util.SearchCriteriaDto;
import sevsu.gradeo.entities.Department;
import sevsu.gradeo.entities.Institute;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс-builder критерий поиска сущностей в БД
 */
public class SearchSpecifications<T> implements Specification<T> {

    private List<SearchCriteriaDto> criterias;

    public SearchSpecifications() {
        this.criterias = new ArrayList<>();
    }

    public void add(SearchCriteriaDto criteria) {
        criterias.add(criteria);
    }

    /*
     * Раздел билдеров фильтров
     */

    public SearchSpecifications<T> name(String name) {
        if (!name.equals("")) this.add(new SearchCriteriaDto("name", name, SearchOperations.CONTAIN_IN_NAME));
        return this;
    }

    public SearchSpecifications<T> isDeleted(Boolean isDeleted) {
        if (!isDeleted) this.add(new SearchCriteriaDto("removedDate", SearchOperations.SEARCH_BY_REMOVED_DATE_NULL));
        if (isDeleted) this.add(new SearchCriteriaDto("removedDate", SearchOperations.SEARCH_BY_REMOVED_DATE_NOT_NULL));
        return this;
    }

    public SearchSpecifications<T> instituteId(Integer id) {
        if (id != 0) this.add(new SearchCriteriaDto("id", id, SearchOperations.SEARCH_BY_INSTITUTE_ID));
        return this;
    }
    /*
     * Конец раздела билдеров фильтров
     */

    /**
     * Функция, создающая предикатов для последующего обращения в БД
     *
     * @param root    - референс на исходную сущность (институт)
     * @param query   - запрос
     * @param builder - билдер самих запросов
     * @return список критериев сортировки
     * @see CriteriaQuery
     * @see CriteriaBuilder
     * <p>
     * Основная логика: узнает, что за операции необходимо выполнить по запросу переданному в сервис
     * Строит запрос через методы builder, сравнивает переданное поле root (key) и переданное значение String (value),
     */

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        List<Predicate> predicates = new ArrayList<>();

        for (SearchCriteriaDto criteria : criterias) {
            if (criteria.getOperation().equals(SearchOperations.EQUAL_ID)) {
                predicates.add(builder.equal(
                        root.get(criteria.getKey()), criteria.getValue().toString()));
            } else if (criteria.getOperation().equals(SearchOperations.EQUAL_NAME)) {
                predicates.add(builder.equal(
                        root.get(criteria.getKey()), criteria.getValue().toString()));
            } else if (criteria.getOperation().equals(SearchOperations.CONTAIN_IN_NAME)) {
                predicates.add(builder.like(
                        root.get(criteria.getKey()), "%" + criteria.getValue().toString() + "%"));
            } else if (criteria.getOperation().equals(SearchOperations.SEARCH_BY_REMOVED_DATE_NULL)) {
                predicates.add(builder.isNull(root.get(criteria.getKey())));
            } else if (criteria.getOperation().equals(SearchOperations.SEARCH_BY_REMOVED_DATE_NOT_NULL)) {
                predicates.add(builder.isNotNull(root.get(criteria.getKey())));
            } else if (criteria.getOperation().equals(SearchOperations.SEARCH_BY_INSTITUTE_ID)) {
                Join<Department, Institute> instituteJoin = root.join("institute");
                predicates.add(builder.equal(
                        instituteJoin.get("id"), criteria.getValue().toString()));
            }
        }
        return builder.and(predicates.toArray(new Predicate[0]));
    }
}

