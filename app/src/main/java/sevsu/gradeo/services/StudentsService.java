package sevsu.gradeo.services;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.dtos.students.CreateStudentDto;
import sevsu.gradeo.dtos.students.StudentDto;
import sevsu.gradeo.dtos.students.UpdateStudentDto;
import sevsu.gradeo.entities.Student;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchGroupException;
import sevsu.gradeo.repositories.GroupsRepository;
import sevsu.gradeo.repositories.StudentsRepository;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Сервис Студентов
 * Здесь расположена вся бизнес-логика функциональности, связанной с студентами
 */
@Service
public class StudentsService {
    @Autowired
    private GroupsRepository groupsRepository;
    @Autowired
    private StudentsRepository studentsRepository;
    @Autowired
    private ModelMapper mapper;
    Logger logger = LoggerFactory.getLogger(getClass());

    public List<StudentDto> getAllStudents() {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Departments");
        var entities = studentsRepository.findAllByRemovedDateIsNullOrderByIdAsc();
        return StreamSupport.stream(entities.spliterator(), false)
                .map(entity -> mapper.map(entity, StudentDto.class))
                .collect(Collectors.toList());
    }

    public List<UpdateStudentDto> getAllStudentsByNameAndGradeBook(String name, Integer gradeBook) {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Departments");
        var entities = studentsRepository.findAllByNameAndGradeBookAndRemovedDateIsNull(name, gradeBook);
        return StreamSupport.stream(entities.spliterator(), false)
                .map(entity -> mapper.map(entity, UpdateStudentDto.class))
                .collect(Collectors.toList());
    }

    public void createStudent(CreateStudentDto createDto) throws NoSuchGroupException {
        logger.debug("Mapping to Student.class");
        var entity = mapper.map(createDto, Student.class);
        logger.debug(LogStrings.FIND_BY_ID, "Group", createDto.getGroupId());
        var group = groupsRepository
                .findById(createDto.getGroupId())
                .orElseThrow(NoSuchGroupException::new);

        logger.debug("Setting gradeBook");
        entity.setGradeBook(createDto.getGradeBook());
        logger.debug("Setting group with id: {}", group.getId());
        entity.setGroup(group);
        logger.debug("Setting enrollmentDate");
        entity.setEnrollmentDate(createDto.getEnrollmentDate());
        logger.debug("Setting educationForm");
        entity.setEducationalForm(createDto.getEducationalForm());
        logger.debug("Setting createdDate");
        entity.setCreatedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        studentsRepository.save(entity);
    }


    public void updateStudent (UpdateStudentDto updateDto) throws EntityNotFoundException {
        logger.debug(LogStrings.FIND_BY_ID, "Student", updateDto.getId());
        var entity = studentsRepository
                .findById(updateDto.getId())
                .orElseThrow(EntityNotFoundException::new);
        logger.debug(LogStrings.FIND_BY_ID, "Group", updateDto.getGroupId());
        var group = groupsRepository
                .findById(updateDto.getGroupId())
                .orElseThrow(NoSuchGroupException::new);

        entity.setName(updateDto.getName());
        logger.debug("Setting gradeBook");
        entity.setGradeBook(updateDto.getGradeBook());
        logger.debug("Setting group with id: {}", group.getId());
        entity.setGroup(group);
        logger.debug("Setting enrollmentDate");
        entity.setEnrollmentDate(updateDto.getEnrollmentDate());
        logger.debug("Setting deductionDate");
        entity.setDeductionDate(updateDto.getDeductionDate());
        logger.debug("Setting educationForm");
        entity.setEducationalForm(updateDto.getEducationalForm());
        logger.debug("Setting updatedDate");
        entity.setUpdatedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        studentsRepository.save(entity);
    }

    public void deleteStudentById(Integer id) throws EntityNotFoundException {
        logger.debug(LogStrings.FIND_BY_ID, "Student", id);
        var entity = studentsRepository
                .findById(id)
                .orElseThrow(EntityNotFoundException::new);

        logger.debug("Setting removedDate");
        entity.setRemovedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        studentsRepository.save(entity);
    }

    public Optional<UpdateStudentDto> getStudentById(Integer id) {
        logger.debug(LogStrings.FIND_BY_ID, "Student", id);
        var student = studentsRepository.findById(id);
        logger.info("Mapping 'Student' to UpdateStudentDto.class");
        return student.map(value -> mapper.map(value, UpdateStudentDto.class));
    }

    public List<Student> getStudentsByGroupId(Integer id) {
        return studentsRepository.findAllByGroupIdAndRemovedDateIsNullOrderByNameAsc(id);
    }

    @PostConstruct
    private void configureMappings() {

        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает

        this.mapper.typeMap(Student.class, StudentDto.class)
                .addMappings(mapper -> mapper.map(Student::getGradeBook, StudentDto::setGradeBook))
                .addMappings(mapper -> mapper.map(d -> d.getGroup().getName(), StudentDto::setGroupName));

        this.mapper.typeMap(Student.class, UpdateStudentDto.class)
                .addMappings(mapper -> mapper.map(d -> d.getGroup().getId(), UpdateStudentDto::setGroupId))
                .addMappings(mapper -> mapper.map(d -> d.getGroup().getName(), UpdateStudentDto::setGroupName))
                .addMappings(mapper -> mapper.map(d -> d.getGroup().getDepartment().getId(), UpdateStudentDto::setDepartmentId))
                .addMappings(mapper -> mapper.map(d -> d.getGroup().getDepartment().getName(), UpdateStudentDto::setDepartmentName))
                .addMappings(mapper -> mapper.map(d -> d.getGroup().getDepartment().getInstitute().getId(), UpdateStudentDto::setInstituteId))
                .addMappings(mapper -> mapper.map(d -> d.getGroup().getDepartment().getInstitute().getName(), UpdateStudentDto::setInstituteName));

        this.mapper.typeMap(CreateStudentDto.class, Student.class)
                .addMappings(mapper -> mapper.skip(Student::setId));
    }
}
