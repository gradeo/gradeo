package sevsu.gradeo.services;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.dtos.groups.UpdateGroupDto;
import sevsu.gradeo.dtos.subjects.CreateSubjectDto;
import sevsu.gradeo.dtos.subjects.SubjectDto;
import sevsu.gradeo.dtos.subjects.SubjectListItemDto;
import sevsu.gradeo.dtos.subjects.UpdateSubjectDto;
import sevsu.gradeo.dtos.util.EntityPageDto;
import sevsu.gradeo.dtos.util.PageBoundsDto;
import sevsu.gradeo.entities.Subject;
import sevsu.gradeo.entities.Teacher;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchDepartmentException;
import sevsu.gradeo.repositories.DepartmentsRepository;
import sevsu.gradeo.repositories.SubjectsRepository;
import sevsu.gradeo.repositories.TeachersRepository;
import sevsu.gradeo.searchFilters.SearchSpecifications;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class SubjectsService {
    @Autowired
    private SubjectsRepository repository;
    @Autowired
    private ModelMapper mapper;
    @Autowired
    private DepartmentsRepository departmentsRepository;
    @Autowired
    private TeachersRepository teachersRepository;

    Logger logger = LoggerFactory.getLogger(getClass());

    public List<SubjectDto> getAllSubjects() {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Subjects");
        var subjects = repository.findAllByRemovedDateIsNullOrderByIdAsc();
        return StreamSupport.stream(subjects.spliterator(), false)
                .map(entity -> mapper.map(entity, SubjectDto.class))
                .collect(Collectors.toList());
    }

    public EntityPageDto<SubjectListItemDto> getAllSubjects(
            Pageable pageable,
            String subjectName,
            boolean isDeleted) {
        Page<Subject> page;
        List<SubjectListItemDto> dtoList;
        PageBoundsDto pageBoundsDto;
        if (subjectName.equals("")) {
            if (isDeleted == false) {
                page = repository.findAllByRemovedDateIsNull(pageable);
            } else {
                page = repository.findAllByRemovedDateIsNotNull(pageable);
            }
            dtoList = StreamSupport.stream(page.spliterator(), false)
                    .map(entity -> mapper.map(entity, SubjectListItemDto.class))
                    .collect(Collectors.toList());
        } else {
            var specifications = new SearchSpecifications<Subject>()
                    .name(subjectName)
                    .isDeleted(isDeleted);
            page = repository.findAll(specifications, pageable);
            dtoList = page.stream()
                    .map(entity -> mapper.map(entity, SubjectListItemDto.class))
                    .collect(Collectors.toList());
        }
        pageBoundsDto = PageService.findBounds(page);
        return new EntityPageDto<>(
                dtoList,
                page.getTotalPages(),
                page.getNumber(),
                pageBoundsDto.getStart(),
                pageBoundsDto.getEnd());
    }

    public void createSubject(CreateSubjectDto createDto) throws EntityNotFoundException {
        logger.debug("Mapping to Subject.class");
        var entity = mapper.map(createDto, Subject.class);
        logger.debug(LogStrings.FIND_BY_ID, "Department", createDto.getDepartmentId());
        var department = departmentsRepository
                .findById(createDto.getDepartmentId())
                .orElseThrow(NoSuchDepartmentException::new);

        logger.debug("Setting department with id: {}", department.getId());
        entity.setDepartment(department);
        logger.debug("Setting teachers");
        Set<Teacher> teachers = new HashSet<>();
        for (int ints : createDto.getTeachersId()) {
            var teacher = teachersRepository
                    .findById(ints)
                    .orElseThrow(EntityNotFoundException::new);
            teachers.add(teacher);
        }
        entity.setTeachers(teachers);
        logger.debug("Setting createDate");
        entity.setCreatedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
    }

    public void updateSubject(UpdateSubjectDto updateDto) throws EntityNotFoundException {
        logger.debug(LogStrings.FIND_BY_ID, "Subject", updateDto.getId());
        var entity = repository
                .findById(updateDto.getId())
                .orElseThrow(EntityNotFoundException::new);

        logger.debug(LogStrings.FIND_BY_ID, "Department", updateDto.getDepartmentId());
        var department = departmentsRepository
                .findById(updateDto.getDepartmentId())
                .orElseThrow(NoSuchDepartmentException::new);

        logger.debug("Setting department with id: {}", department.getId());
        entity.setDepartment(department);
        logger.debug("Setting name");
        entity.setName(updateDto.getName());
        logger.debug("Setting teachers");
        Set<Teacher> teachers = new HashSet<>();
        for (int ints : updateDto.getTeachersId()) {
            var teacher = teachersRepository
                    .findById(ints)
                    .orElseThrow(EntityNotFoundException::new);
            teachers.add(teacher);
        }
        entity.setTeachers(teachers);
        logger.debug("Setting updatedDate");
        entity.setUpdatedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
    }

    public Optional<UpdateSubjectDto> getSubjectById(Integer id) {
        logger.debug(LogStrings.FIND_BY_ID, "Subject", id);
        var entity = repository.findById(id);
        logger.info("Mapping 'Subject' to UpdateSubjectDto.class");
        return entity.map(value -> mapper.map(value, UpdateSubjectDto.class));
    }

    public void deleteSubjectById(Integer id) throws EntityNotFoundException {
        logger.debug(LogStrings.FIND_BY_ID, "Subject", id);
        var entity = repository
                .findById(id)
                .orElseThrow(EntityNotFoundException::new);

        logger.debug("Setting removedDate");
        entity.setRemovedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
    }

    public List<Subject> getSubjectsByGroupId(Integer id) {
        return repository.findAllByGroupsIdAndRemovedDateIsNull(id);
    }

    @PostConstruct
    private void configureMappings() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает

        this.mapper.typeMap(CreateSubjectDto.class, Subject.class)
                .addMappings(mapper -> mapper.map(CreateSubjectDto::getTeachersId, Subject::setTeachers))
                .addMappings(mapper -> mapper.skip(Subject::setId));


        this.mapper.typeMap(Subject.class, UpdateSubjectDto.class)
                .addMappings(mapper -> mapper.map(d -> d.getDepartment().getId(), UpdateSubjectDto::setDepartmentId))
                .addMappings(mapper -> mapper.map(d -> d.getDepartment().getName(), UpdateSubjectDto::setDepartmentName))
                .addMappings(mapper -> mapper.map(d -> d.getDepartment().getInstitute().getId(), UpdateSubjectDto::setInstituteId))
                .addMappings(mapper -> mapper.map(d -> d.getDepartment().getInstitute().getName(), UpdateSubjectDto::setInstituteName))
                .addMappings(mapper -> mapper.map(Subject::getAllTeachersId, UpdateSubjectDto::setTeachersId));
    }

    public List<UpdateSubjectDto> getAllSubjectsByName(String name) {
        var subjects = repository.findAllByNameAndRemovedDateIsNull(name);
        return StreamSupport.stream(subjects.spliterator(), false)
                .map(entity -> mapper.map(entity, UpdateSubjectDto.class))
                .collect(Collectors.toList());
    }
}
