package sevsu.gradeo.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.dtos.util.SelectListItemDto;
import sevsu.gradeo.entities.interfaces.SelectListable;
import sevsu.gradeo.entities.interfaces.SelectListableDateTime;
import sevsu.gradeo.repositories.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class SelectListService {
    @Autowired
    TeachersRepository teachersRepository;
    @Autowired
    InstituteRepository instituteRepository;
    @Autowired
    DepartmentsRepository departmentsRepository;
    @Autowired
    GroupsRepository groupsRepository;
    @Autowired
    SubjectsRepository subjectsRepository;
    @Autowired
    LessonsRepository lessonsRepository;
    @Autowired
    StudentsRepository studentsRepository;

    Logger logger = LoggerFactory.getLogger(getClass());

    public List<SelectListItemDto> getInstitutesList() {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Institutes");
        var entities = instituteRepository.findAllByRemovedDateIsNullOrderByIdAsc();
        return StreamSupport.stream(entities.spliterator(), false)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public List<SelectListItemDto> getDepartmentList() {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Departments");
        var entities = departmentsRepository.findAllByRemovedDateIsNullOrderByIdAsc();
        return StreamSupport.stream(entities.spliterator(), false)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public List<SelectListItemDto> getDepartmentListByInstituteId(Integer id) {
        logger.debug("Getting all 'Departments' by Institute id: {}", id);
        var entities = departmentsRepository.findAllByInstituteIdAndRemovedDateIsNull(id);
        return StreamSupport.stream(entities.spliterator(), false)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public List<SelectListItemDto> getGroupsList() {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Groups");
        var entities = groupsRepository.findAllByRemovedDateIsNullOrderByIdAsc();
        return StreamSupport.stream(entities.spliterator(), false)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public List<SelectListItemDto> getGroupsListByDepartmentId(Integer id) {
        logger.debug("Getting all 'Groups' by Department id: {}", id);
        var entities = groupsRepository.findAllByDepartmentIdAndRemovedDateIsNull(id);
        return StreamSupport.stream(entities.spliterator(), false)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public List<SelectListItemDto> getGroupsListBySubjectId(Integer id) {
        var entities = groupsRepository.findAllBySubjectsIdAndRemovedDateIsNull(id);
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public List<SelectListItemDto> getSubjectList() {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Subjects");
        var entities = subjectsRepository.findAllByRemovedDateIsNullOrderByIdAsc();
        return StreamSupport.stream(entities.spliterator(), false)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public List<SelectListItemDto> getSubjectListByDepartmentId(Integer id) {
        logger.debug("Getting all 'Subjects' by Department id: {}", id);
        var entities = subjectsRepository.findAllByDepartmentIdAndRemovedDateIsNull(id);
        return StreamSupport.stream(entities.spliterator(), false)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public List<SelectListItemDto> getSubjectListByGroupId(Integer id) {

        var entities = subjectsRepository.findAllByGroupsIdAndRemovedDateIsNull(id);
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public List<SelectListItemDto> getSubjectListByTeacherId(Integer id) {
        logger.debug("Getting all 'Subjects' by Teacher id: {}", id);
        var entities = subjectsRepository.findAllByTeachersIdAndRemovedDateIsNull(id);
        return StreamSupport.stream(entities.spliterator(), false)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public List<SelectListItemDto> getTeachersList() {
        var entities = teachersRepository.findAllByRemovedDateIsNullOrderByIdAsc();
        return StreamSupport.stream(entities.spliterator(), false)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public List<SelectListItemDto> getTeachersListByDepartmentId(Integer id) {
        var entities = teachersRepository.findAllByDepartmentIdAndRemovedDateIsNull(id);
        return StreamSupport.stream(entities.spliterator(), false)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public List<SelectListItemDto> getFreeTeachers(Integer departmentId) {
        logger.debug("Getting all teachers by group_id is null");
        var entities = teachersRepository.findAllByDepartmentIdAndGroupIdIsNullAndRemovedDateIsNull(departmentId);
        return StreamSupport.stream(entities.spliterator(), false)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public List<SelectListItemDto> getTeachersListBySubjectId(Integer id) {
        var entities = teachersRepository.findAllBySubjectsIdAndRemovedDateIsNull(id);
        return StreamSupport.stream(entities.spliterator(), false)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public List<SelectListItemDto> getLessonsList() {
        var entities = lessonsRepository.findAllByRemovedDateIsNullOrderByIdAsc();
        return StreamSupport.stream(entities.spliterator(), false)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public List<SelectListItemDto> getLessonsListBySubjectIdAndByGroupId(Integer subjectId, Integer groupId) {

        //TODO заглушки для дат убрать после реализации семестров
        var startDate = LocalDateTime.of(2019, 12, 30, 0, 0);
        var endDate = LocalDateTime.of(2020, 8, 30, 23, 59);

        var entities = lessonsRepository.findAllBySubjectIdAndGroupIdAndRemovedDateIsNullAndDateBetween(
                subjectId,
                groupId,
                startDate,
                endDate);
        return entities.stream()
                .map(this::lessonToDto)
                .collect(Collectors.toList());
    }

    public List<SelectListItemDto> getStudentsList() {
        var entities = studentsRepository.findAllByRemovedDateIsNullOrderByIdAsc();
        return StreamSupport.stream(entities.spliterator(), false)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public List<SelectListItemDto> getStudentsListByGroupId(Integer groupId) {
        var entities = studentsRepository.findAllByGroupIdAndRemovedDateIsNull(groupId);
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    private SelectListItemDto toDto(SelectListable item) {
        var dto = new SelectListItemDto();
        dto.setText(item.getName());
        dto.setValue(item.getId().toString());
        return dto;
    }

    private SelectListItemDto lessonToDto(SelectListableDateTime item) {
        var dto = new SelectListItemDto();
        dto.setText(item.getDateAsString());
        dto.setValue(item.getId().toString());
        return dto;
    }
}
