package sevsu.gradeo.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import sevsu.gradeo.repositories.UserRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepo;
    Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (userRepo.findByEmailAndRemovedDateIsNull(username) != null) {
            username = userRepo.findByEmailAndRemovedDateIsNull(username).getUsername();
            logger.info("User '{}' found and loaded", username);
            return userRepo.findByUsernameAndRemovedDateIsNull(username);
        }
        if (userRepo.findByUsernameAndRemovedDateIsNull(username) != null) {
            logger.info("User '{}' found and loaded", username);
            return userRepo.findByUsernameAndRemovedDateIsNull(username);
        } else {
            logger.error("User '{}' not found!", username);
            throw new UsernameNotFoundException("User not found!");
        }
    }
}
