package sevsu.gradeo.services;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.dtos.teachers.CreateTeacherDto;
import sevsu.gradeo.dtos.teachers.TeacherDto;
import sevsu.gradeo.dtos.teachers.UpdateTeacherDto;
import sevsu.gradeo.entities.Subject;
import sevsu.gradeo.entities.Teacher;
import sevsu.gradeo.entities.User;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.*;
import sevsu.gradeo.repositories.*;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class TeachersService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    TeachersRepository teachersRepository;
    @Autowired
    DepartmentsRepository departmentsRepository;
    @Autowired
    GroupsRepository groupsRepository;
    @Autowired
    private SubjectsRepository subjectsRepository;
    @Autowired
    ModelMapper mapper;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    Logger logger = LoggerFactory.getLogger(getClass());

    public List<TeacherDto> getAllTeachers() {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Teachers");
        var entities = teachersRepository.findAllByRemovedDateIsNullOrderByIdAsc();
        return StreamSupport.stream(entities.spliterator(), false)
                .map(entity -> mapper.map(entity, TeacherDto.class))
                .collect(Collectors.toList());
    }

    public List<UpdateTeacherDto> getAllTeachersByName(String name) {
        var entities = teachersRepository.findAllByNameAndRemovedDateIsNull(name);
        return StreamSupport.stream(entities.spliterator(), false)
                .map(entity -> mapper.map(entity, UpdateTeacherDto.class))
                .collect(Collectors.toList());
    }

    public Teacher getCurrentTeacher() throws EntityNotFoundException {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return teachersRepository
                .findByUserId(user.getId())
                .orElseThrow(EntityNotFoundException::new);
    }

    public TeacherDto thisTeacherToDto(Teacher teacher) {
        return mapper.map(teacher, TeacherDto.class);
    }

    /*
     * Основная логика - создаем преподавателя, затем пользователя,
     * связываем их, сохраняем
     */

    public void createTeacher(CreateTeacherDto createDto) throws EntityNotFoundException, UserAlreadyExistsException, EmailAlreadyExistsException {
        if (userRepository.findByUsernameAndRemovedDateIsNull((createDto.getUsername())) != null) {
            logger.error("User with nickname '{}' already exists!", createDto.getUsername());
            throw new UserAlreadyExistsException();
        } else if (userRepository.findByEmailAndRemovedDateIsNull(createDto.getEmail()) != null) {
            throw new EmailAlreadyExistsException();
        } else {
            logger.debug("Mapping to Teacher.class");
            var newTeacher = mapper.map(createDto, Teacher.class);

            logger.debug(LogStrings.FIND_BY_ID, "Department", createDto.getDepartmentId());
            var department = departmentsRepository
                    .findById(createDto.getDepartmentId())
                    .orElseThrow(NoSuchDepartmentException::new);

            logger.debug("Setting name: {}", createDto.getName());
            newTeacher.setName(createDto.getName());
            logger.debug("Setting department with id: {}", department.getId());
            newTeacher.setDepartment(department);
            logger.debug("Setting position");
            newTeacher.setPosition(createDto.getPosition());
/*            logger.debug("Setting email: {}", createDto.getEmail());
            newTeacher.setEmail(createDto.getEmail());*/
            logger.debug("Setting createDate");
            newTeacher.setCreatedDate(LocalDateTime.now());
            logger.debug(LogStrings.SAVE, newTeacher.getId(), newTeacher.getName());
            Teacher teacher = teachersRepository.save(newTeacher);

            logger.debug("Mapping to User.class");
            var newUser = mapper.map(createDto, User.class);
            logger.debug("Setting username");
            newUser.setUsername(createDto.getUsername());
            logger.debug("Setting password");
            newUser.setPassword(bCryptPasswordEncoder.encode(createDto.getPassword()));
            logger.debug("Marking as active");
            newUser.setActive(true);
            logger.debug("Setting role");
            newUser.setRoles(Collections.singleton(User.Role.TEACHER));
            logger.debug("Setting createdDate");
            newUser.setCreatedDate(LocalDateTime.now());
            logger.debug("Setting Teacher");
            newUser.setTeacher(newTeacher);
            logger.info(LogStrings.SAVE, newUser.getId(), newUser.getUsername());
            User user = userRepository.save(newUser);
            logger.debug(LogStrings.FIND_BY_ID, "Teacher", newTeacher.getId());
            teacher.setUser(user);
            teachersRepository.save(teacher);
        }
    }

    public void updateTeacher(UpdateTeacherDto updateDto) throws EntityNotFoundException {
        logger.debug(LogStrings.FIND_BY_ID, "Teacher", updateDto.getId());
        var teacher = teachersRepository
                .findById(updateDto.getId())
                .orElseThrow(EntityNotFoundException::new);
        logger.debug(LogStrings.FIND_BY_ID, "Department", updateDto.getDepartmentId());
        var department = departmentsRepository
                .findById(updateDto.getDepartmentId())
                .orElseThrow(EntityNotFoundException::new);
        logger.debug("Setting name");
        teacher.setName(updateDto.getName());
        logger.debug("Setting department");
        teacher.setDepartment(department);
        logger.debug("Setting position");
        teacher.setPosition(updateDto.getPosition());
        logger.debug("Setting subjects");
        Set<Subject> subjects = new HashSet<>();
        for (int ints : updateDto.getSubjectsId()) {
            var subject = subjectsRepository
                    .findById(ints)
                    .orElseThrow(EntityNotFoundException::new);
            subjects.add(subject);
        }
        teacher.setSubjects(subjects);
        if (updateDto.getGroupId() != null) {
                logger.debug(LogStrings.SEARCH, "Group", updateDto.getGroupId());
                var group = groupsRepository.findById(updateDto.getGroupId()).orElseThrow(NoSuchGroupException::new);
                if (group.getTeacher() != null) {
                    Teacher oldTeacher = group.getTeacher();
                    logger.debug("Deleting group from old curator id = {}", oldTeacher.getId());
                    oldTeacher.setGroup(null);
                    teachersRepository.save(oldTeacher);
                }
                logger.debug("Setting group");
                teacher.setGroup(group);
                logger.debug("Updating group id = {}, with new teacher id = {}", group.getId(), teacher.getId());
                group.setTeacher(teacher);
                groupsRepository.save(group);
        }
        logger.info(LogStrings.SAVE, teacher.getId(), teacher.getName());
        teachersRepository.save(teacher);
    }

    public boolean isTeacherHaveGroup(Integer id) throws NoSuchTeacherException {
        return teachersRepository.findById(id).orElseThrow(NoSuchTeacherException::new).getGroup() != null;
    }

    public Optional<UpdateTeacherDto> getTeacherById(Integer id) {
        logger.debug(LogStrings.FIND_BY_ID, "Teacher", id);
        var teacher = teachersRepository.findById(id);
        logger.info("Mapping 'Teacher' to UpdateTeacherDto.class");
        return teacher.map(value -> mapper.map(value, UpdateTeacherDto.class));
    }

    @PostConstruct
    private void configureMappings() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает

        this.mapper.typeMap(CreateTeacherDto.class, Teacher.class)
                .addMappings(mapper -> mapper.skip(Teacher::setId));

        this.mapper.typeMap(CreateTeacherDto.class, User.class)
                .addMappings(mapper -> mapper.skip(User::setId));

        this.mapper.typeMap(Teacher.class, UpdateTeacherDto.class)
                .addMappings(mapper -> mapper.map(d -> d.getDepartment().getId(), UpdateTeacherDto::setDepartmentId))
                .addMappings(mapper -> mapper.map(d -> d.getDepartment().getName(), UpdateTeacherDto::setDepartmentName))
                .addMappings(mapper -> mapper.map(d -> d.getDepartment().getInstitute().getId(), UpdateTeacherDto::setInstituteId))
                .addMappings(mapper -> mapper.map(d -> d.getDepartment().getInstitute().getName(), UpdateTeacherDto::setInstituteName))
                .addMappings(mapper -> mapper.map(d -> d.getGroup().getId(), UpdateTeacherDto::setGroupId))
                .addMappings(mapper -> mapper.map(Teacher::getAllSubjectsId, UpdateTeacherDto::setSubjectsId));

        this.mapper.typeMap(Teacher.class, TeacherDto.class)
                .addMappings(mapper -> mapper.map(d -> d.getDepartment().getName(), TeacherDto::setDepartmentName))
                .addMappings(mapper -> mapper.map(Teacher::getPosition, TeacherDto::setPosition))
                .addMappings(mapper -> mapper.map(Teacher::getAllSubjectsId, TeacherDto::setSubjectsId))
                .addMappings(mapper -> mapper.map(d -> d.getGroup().getId(), TeacherDto::setGroupId));
    }
}
