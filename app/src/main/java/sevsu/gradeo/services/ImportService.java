package sevsu.gradeo.services;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sevsu.gradeo.dtos.departments.CreateDepartmentDto;
import sevsu.gradeo.dtos.departments.UpdateDepartmentDto;
import sevsu.gradeo.dtos.groups.CreateGroupDto;
import sevsu.gradeo.dtos.groups.UpdateGroupDto;
import sevsu.gradeo.dtos.institutes.CreateInstituteDto;
import sevsu.gradeo.dtos.institutes.UpdateInstituteDto;
import sevsu.gradeo.dtos.lessons.CreateLessonDto;
import sevsu.gradeo.dtos.students.CreateStudentDto;
import sevsu.gradeo.dtos.subjects.CreateSubjectDto;
import sevsu.gradeo.dtos.subjects.UpdateSubjectDto;
import sevsu.gradeo.dtos.teachers.UpdateTeacherDto;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.SomeEntitiesInAnswersException;
import sevsu.gradeo.exceptions.WrongEducationTypeException;
import sevsu.gradeo.exceptions.entities.NoSuchGroupException;
import sevsu.gradeo.exceptions.entities.NoSuchInstituteException;
import sevsu.gradeo.exceptions.entities.NoSuchSubjectException;
import sevsu.gradeo.exceptions.entities.NoSuchTeacherException;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Service
public class ImportService {
    final private InstituteService instituteService;
    final private DepartmentsService departmentsService;
    final private GroupsService groupsService;
    final private TeachersService teachersService;
    final private StudentsService studentsService;
    final private SubjectsService subjectsService;
    final private LessonsService lessonsService;

    public ImportService(InstituteService instituteService, DepartmentsService departmentsService, GroupsService groupsService, TeachersService teachersService, StudentsService studentsService, SubjectsService subjectsService, LessonsService lessonsService) {
        this.instituteService = instituteService;
        this.departmentsService = departmentsService;
        this.groupsService = groupsService;
        this.teachersService = teachersService;
        this.studentsService = studentsService;
        this.subjectsService = subjectsService;
        this.lessonsService = lessonsService;
    }

    public void importGroupFromExcel(MultipartFile file) throws IOException, SomeEntitiesInAnswersException, EntityNotFoundException, WrongEducationTypeException {
        //cache
        var instituteCache = new HashMap<String, UpdateInstituteDto>();
        var departmentCache = new HashMap<String, UpdateDepartmentDto>();
        var groupCache = new HashMap<String, UpdateGroupDto>();
        //start of read file
        HSSFWorkbook workbook = new HSSFWorkbook(file.getInputStream());
        HSSFSheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.iterator();
        //skip head
        if (rowIterator.hasNext()) rowIterator.next();
        while (rowIterator.hasNext()) {
            StringBuilder keyGenerator = new StringBuilder();
            Iterator<Cell> cellIterator = rowIterator.next().cellIterator();
            String instituteKey = instituteImport(instituteCache, keyGenerator, cellIterator);
            String departmentKey = departmentImport(instituteCache, departmentCache, keyGenerator, cellIterator, instituteKey);
            String groupKey = groupImport(departmentCache, groupCache, keyGenerator, cellIterator, departmentKey);
            studentImport(groupCache, keyGenerator, cellIterator, groupKey);
        }
    }

    public void importLessonsFromExcel(MultipartFile file) throws IOException, SomeEntitiesInAnswersException, EntityNotFoundException {
        //cache
        var instituteCache = new HashMap<String, UpdateInstituteDto>();
        var departmentCache = new HashMap<String, UpdateDepartmentDto>();
        var groupCache = new HashMap<String, UpdateGroupDto>();
        var subjectCache = new HashMap<String, UpdateSubjectDto>();
        //start of read file
        HSSFWorkbook workbook = new HSSFWorkbook(file.getInputStream());
        HSSFSheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.iterator();
        //skip head
        if (rowIterator.hasNext()) rowIterator.next();
        while (rowIterator.hasNext()) {
            StringBuilder keyGenerator = new StringBuilder();
            Iterator<Cell> cellIterator = rowIterator.next().cellIterator();
            String instituteKey = instituteImport(instituteCache, keyGenerator, cellIterator);
            String departmentKey = departmentImport(instituteCache, departmentCache, keyGenerator, cellIterator, instituteKey);
            String groupKey = groupImport(departmentCache, groupCache, keyGenerator, cellIterator, departmentKey);
            var subjectImportResponseDTO = subjectImport(subjectCache,groupCache, keyGenerator, cellIterator, groupKey);
            String subjectKey = subjectImportResponseDTO.getSubjectKey();
            int teacherId = subjectImportResponseDTO.getTeacherId();
            lessonImport(subjectCache, groupCache, keyGenerator, cellIterator, subjectKey, groupKey, teacherId);
        }
    }

    private String lessonImport(
            Map<String, UpdateSubjectDto> subjectCache,
            Map<String, UpdateGroupDto> groupCache,
            StringBuilder keyGenerator,
            Iterator<Cell> cellIterator,
            String subjectKey,
            String groupKey,
            int teacherId) throws SomeEntitiesInAnswersException, NoSuchTeacherException, NoSuchSubjectException, NoSuchGroupException {
        var auditorium = cellIterator.next().getStringCellValue();
        var date = cellIterator.next().getLocalDateTimeCellValue();
        var time = cellIterator.next().getLocalDateTimeCellValue();
        var localDate = date.toLocalDate();
        var localTime = time.toLocalTime();
        var lessonKey = keyGenerator.append("-").append(localDate.toString()).append(localTime.toString()).toString();
        var subject = subjectCache.get(subjectKey);
        var group = groupCache.get(groupKey);
        var lessonsList = lessonsService.getLessonsByGroupIdAndDateAndTime(group.getId(), localDate, localTime);
        if (lessonsList.size() > 1) throw new SomeEntitiesInAnswersException();
        else if (lessonsList.size() < 1) {
            var createLessonDto = new CreateLessonDto();
            createLessonDto.setSubjectId(subject.getId());
            createLessonDto.setAuditorium(auditorium);
            createLessonDto.setDate(localDate);
            createLessonDto.setTime(localTime);
            createLessonDto.setDepartmentId(subject.getDepartmentId());
            createLessonDto.setInstituteId(subject.getInstituteId());
            createLessonDto.setGroupId(group.getId());
            createLessonDto.setTeacherId(teacherId);
            lessonsService.createLesson(createLessonDto);
        }
        return lessonKey;
    }

    private SubjectImportResponseDTO subjectImport(
            Map<String, UpdateSubjectDto> subjectCache,
            Map<String, UpdateGroupDto> groupCache,
            StringBuilder keyGenerator,
            Iterator<Cell> cellIterator,
            String groupKey) throws SomeEntitiesInAnswersException, EntityNotFoundException {
        var subjectName = cellIterator.next().getStringCellValue();
        var subjectKey = keyGenerator.append("-").append(subjectName).toString();
        var teacherName = cellIterator.next().getStringCellValue();
        // TODO сделать проверку наличия в базе и соответствия
        Integer teacherId = getTeacherByName(teacherName).getId();
        if (!subjectCache.containsKey(subjectKey)){
            var subjectsList = subjectsService.getAllSubjectsByName(subjectName);
            if (subjectsList.size() > 1) {
                throw new SomeEntitiesInAnswersException();
            } else if (subjectsList.size() == 1) {
                subjectCache.put(subjectKey, subjectsList.get(0));
            } else if (subjectsList.isEmpty()){
                var createSubjectDto = new CreateSubjectDto();
                createSubjectDto.setInstituteId(groupCache.get(groupKey).getInstituteId());
                createSubjectDto.setDepartmentId(groupCache.get(groupKey).getId());
                createSubjectDto.setName(subjectName);
                createSubjectDto.setTeachersId(new Integer[]{teacherId});
                subjectsService.createSubject(createSubjectDto);
                subjectCache.put(subjectKey, subjectsService.getAllSubjectsByName(subjectName).get(0));
            }
        }
        return new SubjectImportResponseDTO(teacherId,subjectKey);
    }

    private String studentImport(
            Map<String, UpdateGroupDto> groupCache,
            StringBuilder keyGenerator,
            Iterator<Cell> cellIterator,
            String groupKey) throws WrongEducationTypeException, SomeEntitiesInAnswersException, NoSuchGroupException {
        var studentName = cellIterator.next().getStringCellValue();
        var studentKey = keyGenerator.append("-").append(studentName).toString();
        Integer eduForm;
        String eduFormName = cellIterator.next().getStringCellValue();
        switch (eduFormName.toLowerCase()) {
            case "очная":
                eduForm = 1;
                break;
            case "очно-заочная":
                eduForm = 2;
                break;
            case "заочная":
                eduForm = 3;
                break;
            default:
                throw new WrongEducationTypeException();
        }
        Integer gradeBook = (int) cellIterator.next().getNumericCellValue();
        {
            var studentsList = studentsService.getAllStudentsByNameAndGradeBook(studentName, gradeBook);
            if (studentsList.size()>1){
                throw new SomeEntitiesInAnswersException();
            } else if (studentsList.isEmpty()) {
                var createStudentDto = new CreateStudentDto();
                createStudentDto.setGradeBook(gradeBook);
                createStudentDto.setEducationalForm(eduForm.toString());
                createStudentDto.setGroupId(groupCache.get(groupKey).getId());
                createStudentDto.setDepartmentId(groupCache.get(groupKey).getDepartmentId());
                createStudentDto.setInstituteId(groupCache.get(groupKey).getInstituteId());
                createStudentDto.setName(studentName);
                //todo заглушка
                createStudentDto.setEnrollmentDate(LocalDate.now());
                studentsService.createStudent(createStudentDto);
            }
        }
        return studentKey;
    }

    private String groupImport(
            Map<String, UpdateDepartmentDto> departmentCache,
            Map<String, UpdateGroupDto> groupCache,
            StringBuilder keyGenerator,
            Iterator<Cell> cellIterator,
            String departmentKey) throws SomeEntitiesInAnswersException, EntityNotFoundException {
        var groupName = cellIterator.next().getStringCellValue();
        var groupKey = keyGenerator.append("-").append(groupName).toString();
        //TODO do checking that from the teacher oversees the group
        var curatorName = cellIterator.next().getStringCellValue();
        if (!groupCache.containsKey(groupKey)) {
            var groupsList = groupsService.getAllGroupsByName(groupName);
            if (groupsList.size() > 1) {
                throw new SomeEntitiesInAnswersException();
            } else if (groupsList.size() == 1) {
                groupCache.put(groupKey, groupsList.get(0));
            } else if (groupsList.isEmpty()){
                var createGroupDto = new CreateGroupDto();
                createGroupDto.setInstituteId(departmentCache.get(departmentKey).getInstituteId());
                createGroupDto.setDepartmentId(departmentCache.get(departmentKey).getId());
                createGroupDto.setName(groupName);
                UpdateTeacherDto teacher = getTeacherByName(curatorName);
                createGroupDto.setTeacherId(teacher.getId());
                //TODO заглушка
                createGroupDto.setFormedDate(LocalDate.now());
                groupsService.createGroup(createGroupDto);
                groupCache.put(groupKey, groupsService.getAllGroupsByName(groupName).get(0));
            }
        }
        return groupKey;
    }

    private UpdateTeacherDto getTeacherByName(String teacherName) throws NoSuchTeacherException, SomeEntitiesInAnswersException {
        var teachersList = teachersService.getAllTeachersByName(teacherName);
        if (teachersList.isEmpty()){
            throw new NoSuchTeacherException();
        } else if (teachersList.size() > 1) {
            throw new SomeEntitiesInAnswersException();
        }
        return teachersList.get(0);
    }

    private String departmentImport(
            Map<String, UpdateInstituteDto> instituteCache,
            Map<String, UpdateDepartmentDto> departmentCache,
            StringBuilder keyGenerator,
            Iterator<Cell> cellIterator,
            String instituteKey) throws SomeEntitiesInAnswersException, NoSuchInstituteException {
        //start read department
        var departmentName = cellIterator.next().getStringCellValue();
        var departmentKey = keyGenerator.append("-").append(departmentName).toString();
        if (!departmentCache.containsKey(departmentKey)) {
            var departmentsList = departmentsService.getAllDepartmentsByName(departmentName);
            if (departmentsList.size() > 1) {
                throw new SomeEntitiesInAnswersException();
            } else if (departmentsList.size() == 1) {
                departmentCache.put(departmentKey, departmentsList.get(0));
            } else if (departmentsList.isEmpty()) {
                var createDepartmentDto = new CreateDepartmentDto();
                createDepartmentDto.setInstituteId(instituteCache.get(instituteKey).getId());
                createDepartmentDto.setName(departmentName);
                departmentsService.createDepartment(createDepartmentDto);
                departmentCache.put(departmentKey, departmentsService.getAllDepartmentsByName(departmentName).get(0));
            }
        }
        return departmentKey;
    }

    private String instituteImport(
            Map<String, UpdateInstituteDto> instituteCache,
            StringBuilder keyGenerator,
            Iterator<Cell> cellIterator) throws SomeEntitiesInAnswersException {
        //start of read institute
        var instituteName = cellIterator.next().getStringCellValue();
        //key from cache
        var instituteKey = keyGenerator.append(instituteName).toString();
        if (!instituteCache.containsKey(instituteKey)) {
            //if institute are not cached
            var institutesList = instituteService.getAllInstitutesByName(instituteName);
            if (institutesList.size() > 1) {
                //if we have more than one entities
                throw new SomeEntitiesInAnswersException();
            } else if (institutesList.size() == 1) {
                ///if an institute with that name exists
                instituteCache.put(instituteKey, institutesList.get(0));
            } else if (institutesList.isEmpty()) {
                //if an institute with that name does not exist
                var createInstituteDto = new CreateInstituteDto();
                createInstituteDto.setName(instituteName);
                instituteService.createInstitute(createInstituteDto);
                instituteCache.put(instituteKey,instituteService.getAllInstitutesByName(instituteName).get(0));
            }
        }
        return instituteKey;
    }

    @Setter
    @Getter
    @AllArgsConstructor
    private static class SubjectImportResponseDTO{
        private int teacherId;
        private String subjectKey;
    }
}
