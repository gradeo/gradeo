package sevsu.gradeo.services;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sevsu.gradeo.dtos.util.BakerDto;
import sevsu.gradeo.entities.Baker;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BakersService {

    @Autowired
    private ModelMapper mapper;

    public List<BakerDto> getAllBakers() {
        var bakers = Arrays.asList(
                new Baker("Денис Оболенский", "Руководитель проекта", "somemail@help.me"),
                new Baker("Андрей Юзвенко", "Frontend Dev", "dlya.yutuba.01@mail.ru"),
                new Baker("Владимир Чернов", "DevOps", "iblackthefall@protonmail.com"),
                new Baker("Максим Паньков", "Fullstack Dev", "msolntsevvv@gmail.com"),
                new Baker("Илья Вознесенский", "Android Dev", "dehominisfuturum@gmail.com"),
                new Baker("Андрей Сухих", "Android Dev", "sukhikh.andrew@gmail.com"),
                new Baker("Сергей Бондаренко", "Backend Dev", "serg1996bond@rambler.ru"),
                new Baker("Риза Измаилов", "Backend dev", "riza25@bk.ru"),
                new Baker("Татьяна Петрова", "UI/UX", "somemail1@help.me"),
                new Baker("Юрий Кобзарь", "Product Manager", "kobzar.yura.ru.1@gmail.com"),
                new Baker("Анна Конохова", "PM/QA", "a.konohova@rcrealty.ru"),
                new Baker("Елена Зубилова", "SM/QA", "elena.zubilina@mail.ru")
        );

        return bakers.stream()
                .map(entity -> mapper.map(entity, BakerDto.class))
                .collect(Collectors.toList());
    }
}
