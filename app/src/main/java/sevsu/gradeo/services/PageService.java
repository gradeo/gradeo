package sevsu.gradeo.services;

import org.springframework.data.domain.Page;
import sevsu.gradeo.dtos.util.PageBoundsDto;

/**
 * Содержит вспомогательные методы
 * необходимые при работе с Page
 */
public class PageService {
    /**
     * Определяет границы показываемых страниц
     * на панели пагинации в зависимости от
     * текущей страницы
     *
     * @param page страница
     * @return DTO хранящий значения границ панели пагинации
     */
    public static PageBoundsDto findBounds(Page page) {
        int start, end;
        if (page.getTotalPages() == 0) {
            start = 1;
            end = 1;
        } else if (page.getTotalPages() < 10) {
            start = 1;
            end = page.getTotalPages();
        } else if (page.getNumber() <= 5) {
            start = 1;
            end = 10;
        } else if (page.getNumber() >= page.getTotalPages() - 6) {
            end = page.getTotalPages();
            start = page.getTotalPages() - 9;
        } else {
            start = page.getNumber() - 3;
            end = page.getNumber() + 5;
        }
        return new PageBoundsDto(start, end);
    }

}
