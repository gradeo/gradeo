package sevsu.gradeo.services;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.dtos.tasks.CreateTaskDto;
import sevsu.gradeo.dtos.tasks.TaskDto;
import sevsu.gradeo.dtos.tasks.UpdateTaskDto;
import sevsu.gradeo.entities.Task;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchDepartmentException;
import sevsu.gradeo.exceptions.entities.NoSuchSubjectException;
import sevsu.gradeo.repositories.SubjectsRepository;
import sevsu.gradeo.repositories.TaskRepository;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TasksService {
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private SubjectsRepository subjectsRepository;
    @Autowired
    private ModelMapper mapper;

    Logger logger = LoggerFactory.getLogger(getClass());

    public void createTask(CreateTaskDto createDto) throws EntityNotFoundException {
        logger.debug("Mapping to Task.class");
        var entity = mapper.map(createDto, Task.class);
        logger.debug(LogStrings.FIND_BY_ID, "Subject", createDto.getSubjectId());
        var subject = subjectsRepository
                .findById(createDto.getSubjectId())
                .orElseThrow(NoSuchSubjectException::new);
        logger.debug("Setting subject with id: {}", subject.getId());
        entity.setSubject(subject);
        logger.debug("Setting type");
        entity.setType(createDto.getType());
        logger.debug("Setting createDate");
        entity.setCreatedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        taskRepository.save(entity);
    }

    public void updateTask(UpdateTaskDto updateDto) throws EntityNotFoundException {
        logger.debug(LogStrings.FIND_BY_ID, "Task", updateDto.getId());
        var entity = taskRepository
                .findById(updateDto.getId())
                .orElseThrow(EntityNotFoundException::new);
        logger.debug(LogStrings.FIND_BY_ID, "Subject", updateDto.getSubjectId());
        var subject = subjectsRepository
                .findById(updateDto.getSubjectId())
                .orElseThrow(NoSuchDepartmentException::new);
        logger.debug("Setting subject with id: {}", subject.getId());
        entity.setSubject(subject);
        logger.debug("Setting name");
        entity.setName(updateDto.getName());
        logger.debug("Setting type");
        entity.setType(updateDto.getType());
        logger.debug("Setting updatedDate");
        entity.setUpdatedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        taskRepository.save(entity);
    }

    public List<TaskDto> getAllTasksForSubject(Integer id) {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Tasks");
        var entities = taskRepository.findAllBySubjectIdAndRemovedDateIsNull(id);
        return entities.stream()
                .map(entity -> mapper.map(entity, TaskDto.class))
                .collect(Collectors.toList());
    }

    public Optional<UpdateTaskDto> getTaskById(Integer id) {
        logger.debug(LogStrings.FIND_BY_ID, "Task", id);
        var entity = taskRepository.findById(id);
        logger.info("Mapping 'Task' to UpdateTaskDto.class");
        return entity.map(value -> mapper.map(value, UpdateTaskDto.class));
    }

    public void deleteTaskById(Integer id) throws EntityNotFoundException {
        logger.debug(LogStrings.FIND_BY_ID, "Task", id);
        var entity = taskRepository
                .findById(id)
                .orElseThrow(EntityNotFoundException::new);

        logger.debug("Setting removedDate");
        entity.setRemovedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        taskRepository.save(entity);
    }

    @PostConstruct
    private void configureMappings() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает

        this.mapper.typeMap(CreateTaskDto.class, Task.class)
                .addMappings(mapper -> mapper.skip(Task::setId));

        this.mapper.typeMap(Task.class, TaskDto.class)
                .addMappings(mapper -> mapper.map(Task::getType, TaskDto::setType));

        this.mapper.typeMap(Task.class, UpdateTaskDto.class)
                .addMappings(mapper -> mapper.map(d -> d.getSubject().getId(), UpdateTaskDto::setSubjectId))
                .addMappings(mapper -> mapper.map(d -> d.getSubject().getName(), UpdateTaskDto::setSubjectName))
                .addMappings(mapper -> mapper.map(Task::getType, UpdateTaskDto::setType));

    }
}
