package sevsu.gradeo.services;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.dtos.groups.CreateGroupDto;
import sevsu.gradeo.dtos.groups.GroupIndexDto;
import sevsu.gradeo.dtos.groups.GroupViewDto;
import sevsu.gradeo.dtos.groups.UpdateGroupDto;
import sevsu.gradeo.entities.Group;
import sevsu.gradeo.entities.Subject;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchDepartmentException;
import sevsu.gradeo.exceptions.entities.NoSuchTeacherException;
import sevsu.gradeo.repositories.DepartmentsRepository;
import sevsu.gradeo.repositories.GroupsRepository;
import sevsu.gradeo.repositories.SubjectsRepository;
import sevsu.gradeo.repositories.TeachersRepository;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class GroupsService {
    @Autowired
    private GroupsRepository repository;
    @Autowired
    private DepartmentsRepository departmentsRepository;
    @Autowired
    private SubjectsRepository subjectsRepository;
    @Autowired
    private ModelMapper mapper;
    @Autowired
    private TeachersRepository teachersRepository;

    Logger logger = LoggerFactory.getLogger(getClass());

    public List<GroupIndexDto> getAllGroups() {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Groups");
        var groups = repository.findAllByRemovedDateIsNullOrderByIdAsc();
        return StreamSupport.stream(groups.spliterator(), false)
                .map(entity -> mapper.map(entity, GroupIndexDto.class))
                .collect(Collectors.toList());
    }

    public List<UpdateGroupDto> getAllGroupsByName(String name) {
        var groups = repository.findAllByNameAndRemovedDateIsNull(name);
        return StreamSupport.stream(groups.spliterator(), false)
                .map(entity -> mapper.map(entity, UpdateGroupDto.class))
                .collect(Collectors.toList());
    }

    public void createGroup(CreateGroupDto createDto) throws EntityNotFoundException {
        logger.debug("Mapping to Group.class");
        var entity = mapper.map(createDto, Group.class);
        logger.debug(LogStrings.FIND_BY_ID, "Department", createDto.getDepartmentId());
        var department = departmentsRepository
                .findById(createDto.getDepartmentId())
                .orElseThrow(NoSuchDepartmentException::new);

        logger.debug("Setting department with id: {}", department.getId());
        entity.setDepartment(department);
        logger.debug("Setting createdDate");
        entity.setCreatedDate(LocalDateTime.now());
        logger.debug("Setting groupFormedDate");
        entity.setFormedDate(createDto.getFormedDate());
        logger.debug("Setting curator");
        var teacher = teachersRepository.findById(createDto.getTeacherId()).orElseThrow(NoSuchTeacherException::new);
        entity.setTeacher(teacher);
        logger.debug("Update teacher id = {}, setting groupId = {}", createDto.getTeacherId(), entity.getId());
        teacher.setGroup(entity);
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        logger.info(LogStrings.SAVE, teacher.getId(), teacher.getName());
        teachersRepository.save(teacher);
    }

    public void updateGroup(UpdateGroupDto updateDto) throws EntityNotFoundException {
        logger.debug(LogStrings.FIND_BY_ID, "Group", updateDto.getId());
        var entity = repository
                .findById(updateDto.getId())
                .orElseThrow(EntityNotFoundException::new);

        logger.debug(LogStrings.FIND_BY_ID, "Department", updateDto.getDepartmentId());
        var department = departmentsRepository
                .findById(updateDto.getDepartmentId())
                .orElseThrow(NoSuchDepartmentException::new);

        logger.debug("Setting name");
        entity.setName(updateDto.getName());
        logger.debug("Setting department");
        entity.setDepartment(department);
        logger.debug("Setting subjects");
        Set<Subject> subjects = new HashSet<>();
        for (int ints : updateDto.getSubjectsId()) {
            var subject = subjectsRepository
                    .findById(ints)
                    .orElseThrow(EntityNotFoundException::new);
            subjects.add(subject);
        }
        entity.setSubjects(subjects);
        logger.debug("Setting updatedTime");
        entity.setUpdatedDate(LocalDateTime.now());
        logger.debug("Setting groupFormedDate");
        entity.setFormedDate(updateDto.getFormedDate());
        // Оповещаем предыдущего учителя что он уже не прикреплен к группе
        if (entity.getTeacher() != null) {
            var oldTeacher = entity.getTeacher();
            logger.debug("Removing group from old curator, id = {}", oldTeacher.getId());
            oldTeacher.setGroup(null);
            teachersRepository.save(oldTeacher);
        }
        logger.debug("Setting curator");
        var teacher = teachersRepository.findById(updateDto.getTeacherId()).orElseThrow(NoSuchTeacherException::new);
        entity.setTeacher(teacher);
        logger.debug("Update teacher id = {}, setting groupId = {}", updateDto.getTeacherId(), entity.getId());
        teacher.setGroup(entity);
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        logger.info(LogStrings.SAVE, teacher.getId(), teacher.getName());
        teachersRepository.save(teacher);
    }

    public void deleteGroupById(Integer id) throws EntityNotFoundException {
        logger.debug(LogStrings.FIND_BY_ID, "Group", id);
        var entity = repository
                .findById(id)
                .orElseThrow(EntityNotFoundException::new);

        logger.debug("Setting removedDate");
        entity.setRemovedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
    }

    public Optional<UpdateGroupDto> getGroupByIdForUpdate(Integer id) {
        logger.debug(LogStrings.FIND_BY_ID, "Group", id);
        var group = repository.findById(id);
        logger.info("Mapping 'Group' to UpdateGroupDto.class");
        return group.map(value -> mapper.map(value, UpdateGroupDto.class));
    }

    public Optional<GroupViewDto> getGroupById(Integer id) {
        logger.debug(LogStrings.FIND_BY_ID, "Group", id);
        var group = repository.findById(id);
        logger.info("Mapping 'Group' to GroupViewDto.class");
        return group.map(value -> mapper.map(value, GroupViewDto.class));
    }

    @PostConstruct
    private void configureMappings() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает

        this.mapper.typeMap(CreateGroupDto.class, Group.class)
                .addMappings(mapper -> mapper.skip(Group::setId));

        this.mapper.typeMap(Group.class, GroupViewDto.class)
                .addMappings(mapper -> mapper.map(d -> d.getDepartment().getName(), GroupViewDto::setDepartmentName))
                .addMappings(mapper -> mapper.map(d -> d.getDepartment().getInstitute().getName(), GroupViewDto::setInstituteName))
                .addMappings(mapper -> mapper.map(d -> d.getTeacher().getName(), GroupViewDto::setTeacherName))
                .addMappings(mapper -> mapper.map(Group::getAllSubjectsNames, GroupViewDto::setSubjectNames));

        this.mapper.typeMap(Group.class, UpdateGroupDto.class)
                .addMappings(mapper -> mapper.map(d -> d.getDepartment().getName(), UpdateGroupDto::setDepartmentName))
                .addMappings(mapper -> mapper.map(d -> d.getDepartment().getInstitute().getName(), UpdateGroupDto::setInstituteName))
                .addMappings(mapper -> mapper.map(d -> d.getDepartment().getId(), UpdateGroupDto::setDepartmentId))
                .addMappings(mapper -> mapper.map(d -> d.getDepartment().getInstitute().getId(), UpdateGroupDto::setInstituteId))
                .addMappings(mapper -> mapper.map(d -> d.getTeacher().getId(), UpdateGroupDto::setTeacherId))
                .addMappings(mapper -> mapper.map(Group::getAllSubjectsId, UpdateGroupDto::setSubjectsId));
    }
}
