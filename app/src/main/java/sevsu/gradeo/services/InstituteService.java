package sevsu.gradeo.services;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.controllers.InstitutesController;
import sevsu.gradeo.dtos.institutes.CreateInstituteDto;
import sevsu.gradeo.dtos.institutes.InstituteListItemDto;
import sevsu.gradeo.dtos.institutes.UpdateInstituteDto;
import sevsu.gradeo.dtos.util.EntityPageDto;
import sevsu.gradeo.dtos.util.PageBoundsDto;
import sevsu.gradeo.entities.Department;
import sevsu.gradeo.entities.Institute;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.repositories.InstituteRepository;
import sevsu.gradeo.searchFilters.SearchSpecifications;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Сервис Институтов
 * Здесь расположена вся бизнес-логика функциональности, связанной с институтами
 */
@Service
public class InstituteService {
    @Autowired
    private InstituteRepository repository;
    @Autowired
    private ModelMapper mapper;

    Logger logger = LoggerFactory.getLogger(getClass());

    public List<InstituteListItemDto> getAllInstitutes() { // для тестов
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Departments");
        var entities = repository.findAllByRemovedDateIsNullOrderByIdAsc();
        return StreamSupport.stream(entities.spliterator(), false)
                .map(entity -> mapper.map(entity, InstituteListItemDto.class))
                .collect(Collectors.toList());
    }

    public List<UpdateInstituteDto> getAllInstitutesByName(String name) { // для тестов
        var entities = repository.findAllByNameAndRemovedDateIsNull(name);
        return StreamSupport.stream(entities.spliterator(), false)
                .map(entity -> mapper.map(entity, UpdateInstituteDto.class))
                .collect(Collectors.toList());
    }

    /**
     * Получение постраничного списка всех <b>институтов</b> в системе
     *
     * @param name      - название по которому искать
     * @param isDeleted - удалена ли искомая сущность
     * @param pageable  - интерфейс, который нарезает страницы
     * @return список преобразованных в DTO отсортированных сущностей, метаданные страницы
     * @see InstitutesController#index
     */
    public EntityPageDto<InstituteListItemDto> getAllInstitutes(String name, Boolean isDeleted, Pageable pageable) {
        Page<Institute> page;
        List<InstituteListItemDto> dtoList;
        PageBoundsDto pageBoundsDto;

        if (name.equals("")) {
            if (isDeleted == false) {
                page = repository.findAllByRemovedDateIsNull(pageable);
            } else {
                page = repository.findAllByRemovedDateIsNotNull(pageable);
            }
            dtoList = StreamSupport.stream(page.spliterator(), false)
                    .map(entity -> mapper.map(entity, InstituteListItemDto.class))
                    .collect(Collectors.toList());
        } else {
            var specifications = new SearchSpecifications<Institute>()
                    .name(name)
                    .isDeleted(isDeleted);
            page = repository
                    .findAll(specifications, pageable);
            dtoList = page.stream()
                    .map(entity -> mapper.map(entity, InstituteListItemDto.class))
                    .collect(Collectors.toList());
        }
        pageBoundsDto = PageService.findBounds(page);
        return new EntityPageDto<>(
                dtoList,
                page.getTotalPages(),
                page.getNumber(),
                pageBoundsDto.getStart(),
                pageBoundsDto.getEnd());
    }

    /**
     * Создание нового института
     *
     * @param createSubjectDto форма нового инстиутта
     */
    //TODO: исправить логи везде
    public void createInstitute(CreateInstituteDto createSubjectDto) {
        logger.debug("Mapping to Institute.class");
        var entity = mapper.map(createSubjectDto, Institute.class);
        var localDateTime = LocalDateTime.now();
        logger.debug("Setting createdDate");
        entity.setCreatedDate(localDateTime);
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
    }

    /**
     * Редактирование института
     *
     * @param updateInstituteDto форма редактирования института
     */
    public void updateInstitute(UpdateInstituteDto updateInstituteDto) throws EntityNotFoundException {
        logger.debug(LogStrings.FIND_BY_ID, "Institute", updateInstituteDto.getId());
        var entity = repository
                .findById(updateInstituteDto.getId())
                .orElseThrow(EntityNotFoundException::new);

        logger.debug("Setting name");
        entity.setName(updateInstituteDto.getName());
        logger.debug("Setting updatedDate");
        entity.setUpdatedDate(LocalDateTime.now());

        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
    }

    /**
     * Удаление института по его Id
     *
     * @param id
     */

    public void deleteInstituteById(Integer id) throws EntityNotFoundException {
        logger.debug(LogStrings.FIND_BY_ID, "Institute", id);
        var entity = repository
                .findById(id)
                .orElseThrow(EntityNotFoundException::new);


        logger.debug("Setting removedDate");
        entity.setRemovedDate(LocalDateTime.now());

        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
    }

    /**
     * Получение института по его Id
     *
     * @param id Id института
     * @return модель института для формы редактирования
     */
    public Optional<UpdateInstituteDto> getInstituteById(Integer id) {
        logger.debug(LogStrings.FIND_BY_ID, "Institute", id);
        var entity = repository.findById(id);
        logger.info("Mapping 'Institute' to UpdateInstituteDto.class");
        return entity.map(value -> mapper.map(value, UpdateInstituteDto.class));
    }

    @PostConstruct
    void configureMappings() {
        this.mapper.typeMap(CreateInstituteDto.class, Institute.class)
                .addMappings(mapper -> mapper.skip(Institute::setId));

        Converter<Set<Department>, Integer> setSizeConverter = new AbstractConverter<>() {
            protected Integer convert(Set<Department> source) {
                return source == null ? 0 : source.size();
            }
        };

        this.mapper.typeMap(Institute.class, InstituteListItemDto.class)
                .addMappings(mapper -> mapper.using(setSizeConverter)
                        .map(Institute::getDepartments, InstituteListItemDto::setDepartmentsCount)
                );
    }
}
