package sevsu.gradeo.services;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.dtos.attendance.AttendanceDto;
import sevsu.gradeo.dtos.attendance.CreateUpdateAttendanceDto;
import sevsu.gradeo.entities.Attendance;
import sevsu.gradeo.entities.AttendanceStatus;
import sevsu.gradeo.entities.Student;
import sevsu.gradeo.exceptions.entities.NoSuchLessonException;
import sevsu.gradeo.exceptions.entities.NoSuchStudentException;
import sevsu.gradeo.repositories.AttendanceRepository;
import sevsu.gradeo.repositories.LessonsRepository;
import sevsu.gradeo.repositories.StudentsRepository;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Сервис посещаемости
 * Архитектура сущности посещаемости: Занятие - Студент - Статус
 * У одного занятия существует один список посещаемостей, равный количеству студентов в группе, у которой было занятие
 */
@Service
public class AttendanceService {
    @Autowired
    private LessonsRepository lessonsRepository;
    @Autowired
    private StudentsRepository studentsRepository;
    @Autowired
    private AttendanceRepository attendanceRepository;
    @Autowired
    private ModelMapper mapper;
    Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * Хэндлер запроса на создание/редактирование посещения одного занятия
     * Логика: Если найден список посещаемостей у занятия, то
     * 1) Редактируем его, используя новые данные из DTO
     * 2) Иначе создаем новый список посещаемостей.
     *
     * @param createDto - объект с данными
     * @throws NoSuchLessonException  - обработка в AttendanceController
     * @throws NoSuchStudentException - обработка в AttendanceController
     */

    public void handleAttendanceDto(CreateUpdateAttendanceDto createDto) throws NoSuchLessonException, NoSuchStudentException {
        var attendance = attendanceRepository
                .findAllByLessonId(createDto.getLessonId());
        if (attendance.isEmpty()) {
            createAttendance(createDto);
        } else {
            updateAttendance(createDto, attendance);
        }
    }

    /**
     * Создание посещения одного занятия.
     * Логика:
     * 1) Получаем список всех студентов в группе
     * 2) Присваиваем всем студентам, чьи id есть в studentsId в DTO, занятие и статус "Присутствовал"
     * 3) Удаляем их из списка
     * 4) Оставшимся студентам в списке присваиваем занятие и статус "Отсутствовал"
     *
     * @param createDto - объект с данными
     * @throws NoSuchLessonException  - обработка в AttendanceController
     * @throws NoSuchStudentException - обработка в AttendanceController
     */
    private void createAttendance(CreateUpdateAttendanceDto createDto) throws NoSuchLessonException, NoSuchStudentException {
        logger.debug("Mapping to Attendance.class");
        var lesson = lessonsRepository
                .findById(createDto.getLessonId())
                .orElseThrow(NoSuchLessonException::new);

        var studentsInGroup = studentsRepository
                .findAllByGroupIdAndRemovedDateIsNull(createDto.getGroupId());

        for (int presenceStudentId : createDto.getStudentsId()) {
            var student = studentsRepository
                    .findById(presenceStudentId)
                    .orElseThrow(NoSuchStudentException::new);

            var entity = mapper.map(createDto, Attendance.class);
            studentsInGroup.remove(student);
            entity.setStudent(student);
            entity.setLesson(lesson);
            entity.setAttendanceStatus(AttendanceStatus.PRESENCE);
            logger.debug("Setting createdDate");
            entity.setCreatedDate(LocalDateTime.now());
            logger.info(LogStrings.SAVE, entity.getId(), entity.getStudent());
            attendanceRepository.save(entity);
        }

        for (Student student : studentsInGroup) {
            var entity = mapper.map(createDto, Attendance.class);
            entity.setStudent(student);
            entity.setLesson(lesson);
            entity.setAttendanceStatus(AttendanceStatus.ABSENCE);
            logger.debug("Setting createdDate");
            entity.setCreatedDate(LocalDateTime.now());
            logger.info(LogStrings.SAVE, entity.getId(), entity.getStudent());
            attendanceRepository.save(entity);
        }
    }

    /**
     * Редактирование посещаемости.
     * Логика: Для каждой существующей сущности посещаемости
     * 1) Если studentsId в DTO = 0 (никто не пришел на занятие), прикрепленному студенту ставим статус "Отсутствовал"
     * 2) Иначе: Для каждого id студента в studentsId в DTO:
     * 2.1) Если id = id в сущности посещаемости, присваем этому студенту статус "Присутствовал"
     * 2.2) Иначе присваеваем этому студенту статус "Отсутствовал"
     *
     * @param createDto  - объект с данными
     * @param attendance - список посещаемостей для одного занятия
     */
    private void updateAttendance(CreateUpdateAttendanceDto createDto, List<Attendance> attendance) {
        for (Attendance editAttendance : attendance) {
            if (createDto.getStudentsId().length == 0) {
                editAttendance.setAttendanceStatus(AttendanceStatus.ABSENCE);
                editAttendance.setUpdatedDate(LocalDateTime.now());
                attendanceRepository.save(editAttendance);
            } else {
                for (int presenceStudentId : createDto.getStudentsId()) {
                    if (editAttendance.getStudent().getId() == presenceStudentId) {
                        editAttendance.setAttendanceStatus(AttendanceStatus.PRESENCE);
                        editAttendance.setUpdatedDate(LocalDateTime.now());
                        attendanceRepository.save(editAttendance);
                        break;
                    } else {
                        editAttendance.setAttendanceStatus(AttendanceStatus.ABSENCE);
                        editAttendance.setUpdatedDate(LocalDateTime.now());
                        attendanceRepository.save(editAttendance);
                    }
                }
            }
        }
    }

    public List<AttendanceDto> getAllAttendancesForLesson(Integer id) {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Attendances");
        var entities = attendanceRepository.findAllByLessonId(id);
        return entities.stream()
                .map(entity -> mapper.map(entity, AttendanceDto.class))
                .collect(Collectors.toList());
    }


    @PostConstruct
    private void configureMappings() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает
        this.mapper.typeMap(Attendance.class, AttendanceDto.class)
                .addMappings(mapper -> mapper.map(d -> d.getLesson().getSubject().getName(), AttendanceDto::setSubjectName))
                .addMappings(mapper -> mapper.map(d -> d.getLesson().getSubject().getId(), AttendanceDto::setSubjectId))
                .addMappings(mapper -> mapper.map(d -> d.getLesson().getGroup().getId(), AttendanceDto::setGroupId))
                .addMappings(mapper -> mapper.map(d -> d.getLesson().getGroup().getName(), AttendanceDto::setGroupName))
                .addMappings(mapper -> mapper.map(d -> d.getStudent().getId(), AttendanceDto::setStudentId))
                .addMappings(mapper -> mapper.map(d -> d.getStudent().getName(), AttendanceDto::setStudentName))
                .addMappings(mapper -> mapper.map(Attendance::getAttendanceStatus, AttendanceDto::setAttendanceStatus));
    }
}
