package sevsu.gradeo.services;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.dtos.groups.UpdateGroupDto;
import sevsu.gradeo.dtos.lessons.*;
import sevsu.gradeo.entities.Lesson;
import sevsu.gradeo.entities.User;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchGroupException;
import sevsu.gradeo.exceptions.entities.NoSuchSubjectException;
import sevsu.gradeo.exceptions.entities.NoSuchTeacherException;
import sevsu.gradeo.repositories.GroupsRepository;
import sevsu.gradeo.repositories.LessonsRepository;
import sevsu.gradeo.repositories.SubjectsRepository;
import sevsu.gradeo.repositories.TeachersRepository;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Сервис Занятий
 * Здесь расположена вся бизнес-логика функциональности, связанной с занятиями
 */
@Service
public class LessonsService {
    @Autowired
    private LessonsRepository lessonsRepository;
    @Autowired
    private GroupsRepository groupsRepository;
    @Autowired
    private SubjectsRepository subjectsRepository;
    @Autowired
    private TeachersRepository teachersRepository;
    @Autowired
    private ModelMapper mapper;

    Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * Получение списка всех занятий в системе
     *
     * @return Список объектов, отображаемых на странице "Занятия"
     */
    public List<LessonDto> getAllLessons() {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Lessons");
        var entities = lessonsRepository.findAllByRemovedDateIsNullOrderByIdAsc();
        return StreamSupport.stream(entities.spliterator(), false)
                .map(entity -> mapper.map(entity, LessonDto.class))
                .collect(Collectors.toList());
    }

    /**
     * Создание нового занятия
     *
     * @param createDto форма нового занятия
     */
    public void createLesson(CreateLessonDto createDto) throws NoSuchGroupException, NoSuchSubjectException, NoSuchTeacherException {
        logger.debug("Mapping to Lesson.class");
        var entity = mapper.map(createDto, Lesson.class);
        logger.debug(LogStrings.FIND_BY_ID, "Group", createDto.getGroupId());
        var group = groupsRepository
                .findById(createDto.getGroupId())
                .orElseThrow(NoSuchGroupException::new);
        logger.debug(LogStrings.FIND_BY_ID, "Subject", createDto.getSubjectId());
        var subject = subjectsRepository
                .findById(createDto.getSubjectId())
                .orElseThrow(NoSuchSubjectException::new);
        var teacher = teachersRepository
                .findById(createDto.getTeacherId())
                .orElseThrow(NoSuchTeacherException::new);

        logger.debug("Setting name: {}", subject.getName());
        entity.setName(subject.getName());
        logger.debug("Setting group with id: {}", group.getId());
        entity.setGroup(group);
        logger.debug("Setting subject with id: {}", subject.getId());
        entity.setSubject(subject);
        logger.debug("Setting date");
        entity.setDate(LocalDateTime.of(createDto.getDate(), createDto.getTime()));
        logger.debug("Setting auditorium");
        entity.setAuditorium(createDto.getAuditorium());
        logger.debug("Setting createdDate");
        entity.setCreatedDate(LocalDateTime.now());
        logger.debug("Setting teacher");

        entity.setTeacher(teacher);
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        lessonsRepository.save(entity);
    }

    /**
     * Редактирование занятия
     *
     * @param updateDto форма редактирования занятия
     */
    public void updateLesson(UpdateLessonDto updateDto) throws EntityNotFoundException {
        logger.debug(LogStrings.FIND_BY_ID, "Lesson", updateDto.getId());
        var entity = lessonsRepository
                .findById(updateDto.getId())
                .orElseThrow(EntityNotFoundException::new);
        logger.debug("Setting date");
        entity.setDate(LocalDateTime.of(updateDto.getDate(), updateDto.getTime()));
        logger.debug("Setting auditorium");
        entity.setAuditorium(updateDto.getAuditorium());
        logger.debug("Setting updatedDate");
        entity.setUpdatedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        lessonsRepository.save(entity);
    }

    /**
     * Удаление занятия по Id
     *
     * @param id
     */
    public void deleteLessonById(Integer id) throws EntityNotFoundException {
        logger.debug(LogStrings.FIND_BY_ID, "Lesson", id);
        var entity = lessonsRepository
                .findById(id)
                .orElseThrow(EntityNotFoundException::new);

        logger.debug("Setting removedDate");
        entity.setRemovedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        lessonsRepository.save(entity);
    }

    public Optional<LessonDto> getLessonById(Integer id) {
        logger.debug(LogStrings.FIND_BY_ID, "Lesson", id);
        var entity = lessonsRepository.findById(id);
        logger.info("Mapping 'Lesson' to UpdateLessonDto.class");
        return entity.map(value -> mapper.map(value, LessonDto.class));
    }

    public Optional<UpdateLessonDto> getUpdateLessonById(Integer id) {
        logger.debug(LogStrings.FIND_BY_ID, "Lesson", id);
        var entity = lessonsRepository.findById(id);
        logger.info("Mapping 'Lesson' to UpdateLessonDto.class");
        return entity.map(value -> mapper.map(value, UpdateLessonDto.class));
    }

    public Optional<LessonReportDto> getLessonByIdForReport(Integer id) {
        logger.debug(LogStrings.FIND_BY_ID, "Lesson", id);
        var entity = lessonsRepository.findById(id);
        logger.info("Mapping 'Lesson' to LessonDto.class");
        return entity.map(value -> mapper.map(value, LessonReportDto.class));
    }

    public List<CalendarLessonDto> getLessonsByCurrentTeacherIdFromCalendar(LocalDateTime start, LocalDateTime end) {
        logger.debug("Find id from current teacher");
        User user = (User) SecurityContextHolder
                .getContext().getAuthentication().getPrincipal();
        Integer id = user.getTeacher().getId();
        logger.debug("Find lessons by teacher id");
        var entities = lessonsRepository.findAllByTeacherIdAndRemovedDateIsNullAndDateBetween(id, start, end);
        logger.info("Mapping list of Lesson to list of LessonDto");
        return entities.stream()
                .map(entity -> mapper.map(entity, CalendarLessonDto.class))
                .collect(Collectors.toList());
    }

    @PostConstruct
    private void configureMappings() {

        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает

        this.mapper.typeMap(Lesson.class, CalendarLessonDto.class)
                .addMappings(mapper -> mapper.map(Lesson::getName, CalendarLessonDto::setTitle))
                .addMappings(mapper -> mapper.map(Lesson::getDate, CalendarLessonDto::setEnd))
                .addMappings(mapper -> mapper.map(Lesson::getDate, CalendarLessonDto::setStart))
                .addMappings(mapper -> mapper.map(Lesson::getId, CalendarLessonDto::setUrl));

        this.mapper.typeMap(Lesson.class, LessonDto.class)
                .addMappings(mapper -> mapper.map(d -> d.getSubject().getId(), LessonDto::setSubjectId))
                .addMappings(mapper -> mapper.map(d -> d.getSubject().getName(), LessonDto::setSubjectName))
                .addMappings(mapper -> mapper.map(d -> d.getGroup().getId(), LessonDto::setGroupId))
                .addMappings(mapper -> mapper.map(d -> d.getGroup().getName(), LessonDto::setGroupName))
                .addMappings(mapper -> mapper.map(d -> d.getGroup().getDepartment().getName(), LessonDto::setDepartmentName))
                .addMappings(mapper -> mapper.map(d -> d.getGroup().getDepartment().getInstitute().getName(), LessonDto::setInstituteName))
                .addMappings(mapper -> mapper.map(Lesson::getAuditorium, LessonDto::setAuditorium))
                .addMappings(mapper -> mapper.map(Lesson::getDate, LessonDto::setDate));

        this.mapper.typeMap(Lesson.class, LessonReportDto.class)
                .addMappings(mapper -> mapper.map(d -> d.getSubject().getName(), LessonReportDto::setSubjectName))
                .addMappings(mapper -> mapper.map(d -> d.getTeacher().getName(), LessonReportDto::setTeacherName))
                .addMappings(mapper -> mapper.map(Lesson::getDateAsString, LessonReportDto::setDate))
                .addMappings(mapper -> mapper.map(d -> d.getGroup().getName(), LessonReportDto::setGroupName))
                .addMappings(mapper -> mapper.map(Lesson::getAllPresenceStudents, LessonReportDto::setPresenceStudents))
                .addMappings(mapper -> mapper.map(Lesson::getAllAbsenceStudents, LessonReportDto::setAbsenceStudents));

        this.mapper.typeMap(Lesson.class, UpdateLessonDto.class)
                .addMappings(mapper -> mapper.map(d -> d.getSubject().getId(), UpdateLessonDto::setSubjectId))
                .addMappings(mapper -> mapper.map(d -> d.getGroup().getId(), UpdateLessonDto::setGroupId))
                .addMappings(mapper -> mapper.map(d -> d.getGroup().getDepartment().getName(), UpdateLessonDto::setDepartmentName))
                .addMappings(mapper -> mapper.map(d -> d.getGroup().getDepartment().getInstitute().getName(), UpdateLessonDto::setInstituteName))
                .addMappings(mapper -> mapper.map(d -> d.getGroup().getName(), UpdateLessonDto::setGroupName))
                .addMappings(mapper -> mapper.map(d -> d.getSubject().getName(), UpdateLessonDto::setSubjectName))

                .addMappings(mapper -> mapper.map(Lesson::ldtToLocalDate, UpdateLessonDto::setDate))
                .addMappings(mapper -> mapper.map(Lesson::ldtToLocalTime, UpdateLessonDto::setTime));

        this.mapper.typeMap(CreateLessonDto.class, Lesson.class)
                .addMappings(mapper -> mapper.skip(Lesson::setId));
    }

    public List<UpdateLessonDto> getLessonsByGroupIdAndDateAndTime(int groupId, LocalDate date, LocalTime time) {
        var dateTime = LocalDateTime.of(date, time);
        var lessons = lessonsRepository.findAllByGroup_IdAndDateAndRemovedDateIsNull(groupId,dateTime);
        return StreamSupport.stream(lessons.spliterator(), false)
                .map(entity -> mapper.map(entity, UpdateLessonDto.class))
                .collect(Collectors.toList());
    }
}
