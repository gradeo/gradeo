package sevsu.gradeo.services;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.dtos.assessments.AssessmentDto;
import sevsu.gradeo.dtos.assessments.SetAssessmentDto;
import sevsu.gradeo.dtos.tasks.CreateTaskDto;
import sevsu.gradeo.entities.Assessment;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchStudentException;
import sevsu.gradeo.exceptions.entities.NoSuchTaskException;
import sevsu.gradeo.repositories.AssessmentsRepository;
import sevsu.gradeo.repositories.StudentsRepository;
import sevsu.gradeo.repositories.TaskRepository;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AssessmentsService {
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private StudentsRepository studentsRepository;
    @Autowired
    private AssessmentsRepository assessmentsRepository;
    @Autowired
    private ModelMapper mapper;

    Logger logger = LoggerFactory.getLogger(getClass());

    public void setAssessment(SetAssessmentDto dto) throws EntityNotFoundException {
        logger.debug("Mapping to Assessment.class");
        var entity = mapper.map(dto, Assessment.class);
        var optAssessment = assessmentsRepository
                .findByTaskIdAndStudentIdAndRemovedDateIsNull(dto.getTaskId(), dto.getStudentId());
        if (optAssessment.isPresent()) {
            logger.debug("Setting updateDate");
            entity.setUpdatedDate(LocalDateTime.now());
            entity.setId(optAssessment.get().getId());
            entity.setCreatedDate(optAssessment.get().getCreatedDate());
        } else {
            logger.debug("Setting createDate");
            entity.setCreatedDate(LocalDateTime.now());
        }
        logger.debug(LogStrings.FIND_BY_ID, "Task", dto.getTaskId());
        var task = taskRepository
                .findById(dto.getTaskId())
                .orElseThrow(NoSuchTaskException::new);
        logger.debug("Setting task with id: {}", task.getId());
        entity.setTask(task);
        logger.debug(LogStrings.FIND_BY_ID, "Student", dto.getStudentId());
        var student = studentsRepository
                .findById(dto.getStudentId())
                .orElseThrow(NoSuchStudentException::new);
        logger.debug("Setting student with id: {}", student.getId());
        entity.setStudent(student);
        assessmentsRepository.save(entity);
    }

    public List<AssessmentDto> getAllAssessmentForTaskId(Integer id) {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Assessments");
        var entities = assessmentsRepository.findAllByTaskIdAndRemovedDateIsNull(id);
        return entities.stream()
                .map(entity -> mapper.map(entity, AssessmentDto.class))
                .collect(Collectors.toList());
    }

    @PostConstruct
    private void configureMappings() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);  //без этого не работает

        this.mapper.typeMap(CreateTaskDto.class, Assessment.class)
                .addMappings(mapper -> mapper.skip(Assessment::setId));

        this.mapper.typeMap(Assessment.class, AssessmentDto.class)
                .addMappings(mapper -> mapper.map(d -> d.getStudent().getId(), AssessmentDto::setStudentId))
                .addMappings(mapper -> mapper.map(d -> d.getStudent().getName(), AssessmentDto::setStudentName))
                .addMappings(mapper -> mapper.map(Assessment::getScore, AssessmentDto::setScore))
                .addMappings(mapper -> mapper.map(Assessment::getId, AssessmentDto::setId));
    }

}
