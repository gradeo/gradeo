package sevsu.gradeo.services;

import lombok.SneakyThrows;
import lombok.val;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sevsu.gradeo.dtos.users.UpdateUserDto;
import sevsu.gradeo.dtos.util.ResetPasswordDto;
import sevsu.gradeo.entities.ResetToken;
import sevsu.gradeo.entities.User;
import sevsu.gradeo.exceptions.entities.TokenAlreadyUsedException;
import sevsu.gradeo.exceptions.entities.TokenOverdueException;
import sevsu.gradeo.repositories.ResetTokenRepository;

import sevsu.gradeo.exceptions.EntityNotFoundException;

import java.sql.Date;
import java.time.LocalDate;
import java.util.UUID;

@Service
public class ResetPasswordService {
    @Autowired
    ResetTokenRepository resetTokenRepository;

    @Autowired
    UsersService usersService;

    @Autowired
    private ModelMapper mapper;

    @Autowired
    EmailService emailService;

    Logger logger = LoggerFactory.getLogger(getClass());

    public void createToken(User user, UUID token) {
        logger.info("Creating new reset token for {}", user.getEmail());
        ResetToken resetToken = new ResetToken();
        resetToken.setUser(user);
        resetToken.setToken(token);
        resetTokenRepository.save(resetToken);
    }

    public ResetToken getToken(UUID token) throws EntityNotFoundException, TokenOverdueException, TokenAlreadyUsedException {
        val entity = resetTokenRepository.findByToken(token);
        if (entity == null) {
            throw new EntityNotFoundException();
        } else if (entity.getExpiryDate().getTime() <= Date.valueOf(LocalDate.now()).getTime()) {
            throw new TokenOverdueException();
        } else if (entity.getIsUsed()) {
            throw new TokenAlreadyUsedException();
        }
        return entity;
    }

    @SneakyThrows
    public void resetPassword(ResetPasswordDto resetPasswordDto) {
        val token = getToken(UUID.fromString(resetPasswordDto.getCode()));
        val user = token.getUser();
        user.setPassword(resetPasswordDto.getPassword());
        usersService.updateUser(mapper.map(user, UpdateUserDto.class));
        token.setIsUsed(true);
        resetTokenRepository.save(token);
    }

    public void sendMail(String email, UUID token) {
        String RECOVERY_URL = String.format("http://gradeo.ru/auth/reset?code=%s", token);
        String MESSAGE = String.format("Для восстановления пароля перейдите по ссылке:\n%s", RECOVERY_URL);
        String SUBJECT = "Gradeo - восстановление пароля";
        emailService.send(email, SUBJECT, MESSAGE);
    }
}
