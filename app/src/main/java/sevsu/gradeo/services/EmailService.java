package sevsu.gradeo.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import sevsu.gradeo.entities.interfaces.EmailSender;

@Service
public class EmailService implements EmailSender {
    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    JavaMailSender mailSender;

    @Value("${spring.mail.from}")
    private String MAIL_FROM;

    @Override
    public void send(String email, String subject, String body) {
        logger.debug("Sending mail to {}, subject = {}", email, subject);
        var mail = new SimpleMailMessage();
        mail.setFrom(MAIL_FROM);
        mail.setTo(email);
        mail.setSubject(subject);
        mail.setText(body);

        mailSender.send(mail);
    }
}
