package sevsu.gradeo.services;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.dtos.users.CreateUserDto;
import sevsu.gradeo.dtos.users.UpdateUserDto;
import sevsu.gradeo.dtos.users.UserDto;
import sevsu.gradeo.entities.User;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.*;
import sevsu.gradeo.repositories.TeachersRepository;
import sevsu.gradeo.repositories.UserRepository;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Сервис пользователей (будущих студентов)
 */
@Service
public class UsersService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    TeachersRepository teachersRepository;
    @Autowired
    private ModelMapper mapper;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private SessionService sessionService;

    Logger logger = LoggerFactory.getLogger(getClass());

    public List<UserDto> getAllUsers() {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Users");
        var users = userRepository.findAllByRemovedDateIsNullOrderByIdAsc();
        return StreamSupport.stream(users.spliterator(), false)
                .map(entity -> mapper.map(entity, UserDto.class))
                .collect(Collectors.toList());
    }

    /* public List<Role> getAllRoles() {
        List<Role> roles = Arrays.asList(Role.values());
        return roles;
    } */

    public void createUser(CreateUserDto createDto) throws UserAlreadyExistsException, EmailAlreadyExistsException, PasswordsNotEqualException {
        if (userRepository.findByUsernameAndRemovedDateIsNull((createDto.getUsername())) != null) {
            throw new UserAlreadyExistsException();
        }
        if (userRepository.findByEmailAndRemovedDateIsNull(createDto.getEmail()) != null) {
            throw new EmailAlreadyExistsException();
        }
        if (!createDto.getConfirmPassword().equals(createDto.getPassword())) {
            throw new PasswordsNotEqualException();
        }
            logger.debug("Mapping to User.class");
            var entity = mapper.map(createDto, User.class);
            logger.debug("Setting password");
            entity.setPassword(bCryptPasswordEncoder.encode(createDto.getPassword()));
            logger.debug("Setting email");
            entity.setEmail(createDto.getEmail());
            logger.debug("Marking as active");
            entity.setActive(true);
            logger.debug("Setting role");
        entity.setRoles(Collections.singleton(User.Role.ADMIN));
            logger.debug("Setting createdDate");
            entity.setCreatedDate(LocalDateTime.now());
            logger.info(LogStrings.SAVE, entity.getId(), entity.getUsername());
            userRepository.save(entity);
    }

    public void updateUser(UpdateUserDto updateDto) throws NoSuchUserException {
        logger.debug(LogStrings.FIND_BY_ID, "User", updateDto.getId());
        var entity = userRepository
                .findById(updateDto.getId())
                .orElseThrow(NoSuchUserException::new);

        logger.debug("Setting username");
        entity.setUsername(updateDto.getUsername());
        logger.debug("Setting password");
        entity.setPassword(bCryptPasswordEncoder.encode(updateDto.getPassword()));
        logger.debug("Setting updatedDate");
        entity.setUpdatedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getUsername());
        userRepository.save(entity);

    }


    public void deleteUserById(Integer id) throws EntityNotFoundException, LastUserException {
        if (getAllUsers().size() == 1) {
            logger.warn("Couldn't delete last user in system!");
            throw new LastUserException();
        } else {
            logger.debug(LogStrings.FIND_BY_ID, "User", id);
            var entity = userRepository
                    .findById(id)
                    .orElseThrow(NoSuchUserException::new);
            if (entity.getTeacher() != null) {
                var teacher = teachersRepository
                        .findById(entity
                                .getTeacher()
                                .getId())
                        .orElseThrow(NoSuchTeacherException::new);
                teacher.setRemovedDate(LocalDateTime.now());
                teachersRepository.save(teacher);
            } else logger.warn("This is not a teacher!");

            logger.debug("Setting removedDate");
            entity.setRemovedDate(LocalDateTime.now());
            logger.debug("Expiring session");
            sessionService.expireUserSessions(entity.getUsername());
            logger.info(LogStrings.SAVE, entity.getId(), entity.getUsername());
            userRepository.save(entity);
        }
    }

    public Optional<UpdateUserDto> getUserById(Integer id) throws EntityNotFoundException {
        logger.debug(LogStrings.FIND_BY_ID, "User", id);
        var entity = userRepository.findById(id);
        logger.info("Flush password data");

        if (entity.isPresent()) entity.get().setPassword("");
        else throw new EntityNotFoundException();

        logger.info("Mapping 'User' to UpdateUserDto.class");
        return entity.map(value -> mapper.map(value, UpdateUserDto.class));
    }

    public User getUserByEmail(String email) throws EntityNotFoundException {
        var entity = userRepository.findByEmailAndRemovedDateIsNull(email);
        if (entity == null) throw new EntityNotFoundException();
        return entity;
    }

    @PostConstruct
    private void configureMappings() {
        this.mapper.typeMap(CreateUserDto.class, User.class)
                .addMappings(mapper -> mapper.skip(User::setId));
        this.mapper.typeMap(User.class, UpdateUserDto.class)
                .addMappings(mapper -> mapper.map(d -> d.getTeacher().getId(), UpdateUserDto::setTeacherId));
    }

}
