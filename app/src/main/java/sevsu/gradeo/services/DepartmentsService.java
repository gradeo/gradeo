package sevsu.gradeo.services;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.dtos.departments.CreateDepartmentDto;
import sevsu.gradeo.dtos.departments.DepartmentListItemDto;
import sevsu.gradeo.dtos.departments.UpdateDepartmentDto;
import sevsu.gradeo.dtos.util.EntityPageDto;
import sevsu.gradeo.dtos.util.PageBoundsDto;
import sevsu.gradeo.entities.Department;
import sevsu.gradeo.entities.Group;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchInstituteException;
import sevsu.gradeo.repositories.DepartmentsRepository;
import sevsu.gradeo.repositories.InstituteRepository;
import sevsu.gradeo.searchFilters.SearchSpecifications;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Сервис Кафедр
 * Здесь расположена вся бизнес-логика функциональности, связанной с кафедрами
 */
@Service
public class DepartmentsService {
    @Autowired
    private DepartmentsRepository repository;
    @Autowired
    private InstituteRepository instituteRepository;
    @Autowired
    private ModelMapper mapper;

    Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * Получение списка всех институтов в системе
     * @return Список объектов, отображаемых на странице "Институты"
     */
    public List<DepartmentListItemDto> getAllDepartments() {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Departments");
        var entities = repository.findAllByRemovedDateIsNullOrderByIdAsc();
        return StreamSupport.stream(entities.spliterator(), false)
                .map(entity -> mapper.map(entity, DepartmentListItemDto.class))
                .collect(Collectors.toList());
    }
    public List<UpdateDepartmentDto> getAllDepartmentsByName(String name) {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Departments");
        var entities = repository.findAllByNameAndRemovedDateIsNull(name);
        return StreamSupport.stream(entities.spliterator(), false)
                .map(entity -> mapper.map(entity, UpdateDepartmentDto.class))
                .collect(Collectors.toList());
    }

    public EntityPageDto<DepartmentListItemDto> getAllDepartments(
            String departName,
            int instId,
            boolean isDeleted,
            Pageable pageable) {
        Page<Department> page;
        List<DepartmentListItemDto> dtoList;
        PageBoundsDto pageBoundsDto;
        if (departName.equals("") && instId == 0) {
            if (isDeleted == false) {
                page = repository.findAllByRemovedDateIsNull(pageable);
            } else {
                page = repository.findAllByRemovedDateIsNotNull(pageable);
            }
            dtoList = StreamSupport.stream(page.spliterator(), false)
                    .map(entity -> mapper.map(entity, DepartmentListItemDto.class))
                    .collect(Collectors.toList());
        } else {
            var specifications = new SearchSpecifications<Department>()
                    .name(departName)
                    .isDeleted(isDeleted)
                    .instituteId(instId);
            page = repository.findAll(specifications, pageable);
            dtoList = page.stream()
                    .map(entity -> mapper.map(entity, DepartmentListItemDto.class))
                    .collect(Collectors.toList());
        }
        pageBoundsDto = PageService.findBounds(page);
        return new EntityPageDto<>(
                dtoList,
                page.getTotalPages(),
                page.getNumber(),
                pageBoundsDto.getStart(),
                pageBoundsDto.getEnd());
    }

    /**
     * Создание новой кафедры
     *
     * @param createDto форма новой кафедры
     */
    public void createDepartment(CreateDepartmentDto createDto) throws NoSuchInstituteException {
        logger.debug("Mapping to Department.class");
        var entity = mapper.map(createDto, Department.class);
        logger.debug(LogStrings.FIND_BY_ID, "Institute", createDto.getInstituteId());
        var institute = instituteRepository
                .findById(createDto.getInstituteId())
                .orElseThrow(NoSuchInstituteException::new);

        logger.debug("Setting institute with id: {}", institute.getId());
        entity.setInstitute(institute);
        logger.debug("Setting createdDate");
        entity.setCreatedDate(LocalDateTime.now());

        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
    }

    /**
     * Редактирование кафедры
     * @param updateDto форма редактирования кафедры
     */
    public void updateDepartment(UpdateDepartmentDto updateDto) throws EntityNotFoundException {
        logger.debug(LogStrings.FIND_BY_ID, "Department", updateDto.getId());
        var entity = repository
                .findById(updateDto.getId())
                .orElseThrow(EntityNotFoundException::new);

        logger.debug(LogStrings.FIND_BY_ID, "Institute", updateDto.getInstituteId());
        var institute = instituteRepository
                .findById(updateDto.getInstituteId())
                .orElseThrow(NoSuchInstituteException::new);

        logger.debug("Setting name");
        entity.setName(updateDto.getName());
        logger.debug("Setting institute");
        entity.setInstitute(institute);
        logger.debug("Setting updatedDate");
        entity.setUpdatedDate(LocalDateTime.now());

        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
    }


    /**
     * Удаление кафедры по Id
     *
     * @param id
     */
    public void deleteDepartmentById(Integer id) throws EntityNotFoundException {
        logger.debug(LogStrings.FIND_BY_ID, "Department", id);
        var entity = repository
                .findById(id)
                .orElseThrow(EntityNotFoundException::new);

        logger.debug("Setting removedDate");
        entity.setRemovedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
    }

    /**
     * Получение кафедры по  Id
     * @param id Id кафедры
     * @return модель кафедры для формы редактирования
     */
    public Optional<UpdateDepartmentDto> getDepartmentById(Integer id) {
        logger.debug(LogStrings.FIND_BY_ID, "Department", id);
        var entity = repository.findById(id);
        logger.info("Mapping 'Department' to UpdateDepartmentDto.class");
        return entity.map(value -> mapper.map(value, UpdateDepartmentDto.class));
    }

    @PostConstruct
    private void configureMappings() {
        this.mapper.typeMap(CreateDepartmentDto.class, Department.class)
                .addMappings(mapper -> mapper.skip(Department::setId));

        this.mapper.typeMap(Department.class, UpdateDepartmentDto.class)
                .addMappings(mapper -> mapper.map(d->d.getInstitute().getId(), UpdateDepartmentDto::setInstituteId));

        Converter<Set<Group>, Integer> setSizeConverter = new AbstractConverter<>() {
            protected Integer convert(Set<Group> source) {
                return source == null ? 0 : source.size();
            }
        };

        this.mapper.typeMap(Department.class, DepartmentListItemDto.class)
                .addMappings(mapper -> mapper.using(setSizeConverter)
                        .map(Department::getGroups, DepartmentListItemDto::setGroupsCount));
    }
}
