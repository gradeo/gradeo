package sevsu.gradeo.cqrs.queryhandler;

import an.awesome.pipelinr.Command;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.cqrs.query.GetDepartmentByIdQuery;
import sevsu.gradeo.dtos.departments.DepartmentsViewDto;
import sevsu.gradeo.exceptions.entities.NoSuchDepartmentException;
import sevsu.gradeo.repositories.DepartmentsRepository;

@Component
//возвращает кафедру по id
public class GetDepartmentByIdHandler implements Command.Handler<GetDepartmentByIdQuery, DepartmentsViewDto> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final DepartmentsRepository repository;
    private final ModelMapper mapper;

    public GetDepartmentByIdHandler(DepartmentsRepository repository, ModelMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public DepartmentsViewDto handle(GetDepartmentByIdQuery command) {
        int id = command.getId();
        logger.debug(LogStrings.FIND_BY_ID, "Department", id);
        var entity = repository
                .findByIdAndRemovedDateIsNull(id)
                .orElseThrow(NoSuchDepartmentException::new);
        logger.info("Mapping 'Department' to UpdateDepartmentDto.class");
        return mapper.map(entity, DepartmentsViewDto.class);
    }
}
