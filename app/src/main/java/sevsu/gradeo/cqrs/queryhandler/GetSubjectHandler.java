package sevsu.gradeo.cqrs.queryhandler;

import an.awesome.pipelinr.Command;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.cqrs.query.GetSubjectQuery;
import sevsu.gradeo.dtos.subjects.SubjectViewDto;
import sevsu.gradeo.dtos.subjects.UpdateSubjectDto;
import sevsu.gradeo.exceptions.entities.NoSuchSubjectException;
import sevsu.gradeo.repositories.SubjectsRepository;

@Component
@AllArgsConstructor
public class GetSubjectHandler implements Command.Handler<GetSubjectQuery, SubjectViewDto> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final SubjectsRepository repository;
    private final ModelMapper mapper;

    @Override
    public SubjectViewDto handle(GetSubjectQuery command) {
        logger.debug(LogStrings.FIND_BY_ID, "Subject", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchSubjectException::new);
        logger.info("Mapping 'Subject' to UpdateSubjectDto.class");
        return mapper.map(entity, SubjectViewDto.class);
    }
}
