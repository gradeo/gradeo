package sevsu.gradeo.cqrs.queryhandler;

import an.awesome.pipelinr.Command;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import sevsu.gradeo.cqrs.query.GetInstituteQuery;
import sevsu.gradeo.dtos.institutes.InstituteViewDto;
import sevsu.gradeo.exceptions.entities.NoSuchInstituteException;
import sevsu.gradeo.repositories.InstituteRepository;

@Component
public class GetInstituteHandler implements Command.Handler<GetInstituteQuery, InstituteViewDto> {

    private final InstituteRepository repository;
    private final ModelMapper mapper;

    public GetInstituteHandler(InstituteRepository repository, ModelMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public InstituteViewDto handle(GetInstituteQuery command) {
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchInstituteException::new);
        return mapper.map(entity, InstituteViewDto.class);
    }
}
