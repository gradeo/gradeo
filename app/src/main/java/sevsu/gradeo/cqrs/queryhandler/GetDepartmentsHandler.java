package sevsu.gradeo.cqrs.queryhandler;

import an.awesome.pipelinr.Command;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.cqrs.query.GetDepartmentsQuery;
import sevsu.gradeo.dtos.departments.DepartmentsViewDto;
import sevsu.gradeo.entities.Department;
import sevsu.gradeo.repositories.DepartmentsRepository;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
//для возврата всех кафедр
public class GetDepartmentsHandler implements Command.Handler<GetDepartmentsQuery, List<DepartmentsViewDto>> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final ModelMapper mapper;
    private final DepartmentsRepository repository;

    public GetDepartmentsHandler(ModelMapper mapper, DepartmentsRepository repository) {
        this.mapper = mapper;
        this.repository = repository;
    }


    @Override
    public List<DepartmentsViewDto> handle(GetDepartmentsQuery command) {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Departments");
        //список кафедр
        var entities = repository.findAllByRemovedDateIsNullOrderByIdAsc();
        //список ДТО кафедр
        return StreamSupport.stream(entities.spliterator(), false)
                .map(entity -> mapper.map(entity, DepartmentsViewDto.class))
                .collect(Collectors.toList());
    }

    @PostConstruct
    void configureMappings() {
        this.mapper.typeMap(Department.class, DepartmentsViewDto.class)
                .addMappings(mapper -> mapper.map(d->d.getInstitute().getId(), DepartmentsViewDto::setInstituteId));
    }
}
