package sevsu.gradeo.cqrs.query;

import an.awesome.pipelinr.Command;
import sevsu.gradeo.dtos.departments.DepartmentsViewDto;

import java.util.List;

//для возврата всех кафедр
public class GetDepartmentsQuery implements Command<List<DepartmentsViewDto>> {}
