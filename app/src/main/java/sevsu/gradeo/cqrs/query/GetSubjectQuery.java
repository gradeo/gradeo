package sevsu.gradeo.cqrs.query;

import an.awesome.pipelinr.Command;
import lombok.AllArgsConstructor;
import lombok.Getter;
import sevsu.gradeo.dtos.subjects.SubjectViewDto;

import javax.validation.constraints.Min;

@Getter
@AllArgsConstructor
public class GetSubjectQuery implements Command<SubjectViewDto> {
    @Min(1)
    private final Integer id;
}
