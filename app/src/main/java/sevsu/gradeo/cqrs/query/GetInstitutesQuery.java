package sevsu.gradeo.cqrs.query;

import an.awesome.pipelinr.Command;
import sevsu.gradeo.dtos.institutes.InstituteViewDto;

import java.util.List;

//выдача списка всех институтов
public class GetInstitutesQuery implements Command<List<InstituteViewDto>> {
}
