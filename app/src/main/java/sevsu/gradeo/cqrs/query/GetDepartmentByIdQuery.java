package sevsu.gradeo.cqrs.query;

import an.awesome.pipelinr.Command;
import lombok.Getter;
import sevsu.gradeo.dtos.departments.DepartmentsViewDto;

import javax.validation.constraints.Min;

@Getter
//для возврата кафедры по id
public class GetDepartmentByIdQuery implements Command<DepartmentsViewDto> {

    @Min(1)
    private final int id;

    public GetDepartmentByIdQuery(int id) {
        this.id = id;
    }
}
