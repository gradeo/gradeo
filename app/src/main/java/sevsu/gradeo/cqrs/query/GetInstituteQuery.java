package sevsu.gradeo.cqrs.query;

import an.awesome.pipelinr.Command;
import lombok.Getter;
import sevsu.gradeo.dtos.institutes.InstituteViewDto;

import javax.validation.constraints.Min;

@Getter
//выдача института
public class GetInstituteQuery implements Command<InstituteViewDto> {

    @Min(1)
    private final Integer id;

    public GetInstituteQuery(Integer id) {
        this.id = id;
    }
}
