package sevsu.gradeo.cqrs.middleware;

import an.awesome.pipelinr.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
//логер работает только с запросами
class Loggable implements Command.Middleware {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public <R, C extends Command<R>> R invoke(C command, Next<R> next) {
        //лог запроса
        logger.debug(command.toString());
        R response = next.invoke();
        //лог результата
        if (response != null) logger.debug(response.toString());
        else
            logger.debug("response == null");
        return response;
    }


}