package sevsu.gradeo.cqrs.middleware;

import an.awesome.pipelinr.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
@Order(2)
//производит валидацию команд
public class ValidatorMiddleware implements Command.Middleware{

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @Override
    public <R, C extends Command<R>> R invoke(C command, Next<R> next) {
        //невалидные поля команды
        Set<ConstraintViolation<C>> constraintViolations = validator.validate(command);
        //если хоть одно поле невалидно
        if(!constraintViolations.isEmpty()) {
            //хранит список ошибок для передачи в ответ
            List<String> errorsCollector = new ArrayList<>();
            //логирование ошибок и запись их в обвертку
            for (ConstraintViolation<C> violation : constraintViolations) {
                logger.error(violation.getMessage());
                errorsCollector.add(violation.getMessage());
            }
            throw new ConstraintViolationException("Error from " + this.getClass().getSimpleName(), constraintViolations);
        }
        //вызов следующего мидлвара и получение от него ответа хендлера команды
        R response = next.invoke();
        //передача ответа вышестоящему мидлвару
        return response;
    }
}