package sevsu.gradeo.cqrs.command;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
//создание института в БД
public class CreateInstituteCommand implements Command<Voidy> {

    @NotBlank(message = "Название института не должно быть пустым")
    //поле не final так как почему-то именно для этой комманды требует пустой конструктор и сеттер
    private String name;

    public CreateInstituteCommand(String name) {
        this.name = name;
    }

    public CreateInstituteCommand() {
    }
}