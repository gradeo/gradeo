package sevsu.gradeo.cqrs.command;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.Getter;

import javax.validation.constraints.Min;

@Getter

//Для удаления группы из БД
public class DeleteGroupCommand implements Command<Voidy>{

    @Min(1)
    private final int id;

    public DeleteGroupCommand(int id) {
        this.id = id;
    }
}
