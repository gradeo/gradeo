package sevsu.gradeo.cqrs.command;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.Getter;

import javax.validation.constraints.Min;

@Getter
//для удаления кафедры из бд
public class DeleteDepartmentCommand implements Command<Voidy> {

    @Min(1)
    private final int id;

    public DeleteDepartmentCommand(int id) {
        this.id = id;
    }
}
