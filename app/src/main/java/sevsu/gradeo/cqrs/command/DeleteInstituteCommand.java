package sevsu.gradeo.cqrs.command;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
//для удаления института
public class DeleteInstituteCommand implements Command<Voidy> {
    private final Integer id;
}
