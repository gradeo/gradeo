package sevsu.gradeo.cqrs.command;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@Getter
@AllArgsConstructor
public class UpdateSubjectCommand implements Command<Voidy> {

    @Min(1)
    private final Integer id;
    @NotBlank
    private final String name;
    @Min(1)
    private final Integer departmentId;
    private final Integer[] teachersId;
}
