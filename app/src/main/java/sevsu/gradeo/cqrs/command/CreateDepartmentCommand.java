package sevsu.gradeo.cqrs.command;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.Getter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
public class CreateDepartmentCommand implements Command<Voidy> {
    @NotBlank(message = "Название кафедры не должно быть пустым")
    private final String name;
    @NotNull(message = "Необходимо выбрать институт")
    @Min(1)
    private final Integer instituteId;

    public CreateDepartmentCommand(String name, Integer instituteId) {
        this.name = name;
        this.instituteId = instituteId;
    }
}
