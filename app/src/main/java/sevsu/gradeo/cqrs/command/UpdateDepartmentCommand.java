package sevsu.gradeo.cqrs.command;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.Getter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
//для редактирования кафедры
public class UpdateDepartmentCommand implements Command<Voidy> {

    @Min(1)
    private final Integer id;
    @NotBlank(message = "Название кафедры не должно быть пустым")
    private final String name;
    @NotNull(message = "Необходимо выбрать институт")
    private final Integer instituteId;

    public UpdateDepartmentCommand(Integer id, @NotBlank String name, @NotNull Integer instituteId) {
        this.id = id;
        this.name = name;
        this.instituteId = instituteId;
    }
}
