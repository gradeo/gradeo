package sevsu.gradeo.cqrs.commandHandler;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.cqrs.command.UpdateDepartmentCommand;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchDepartmentException;
import sevsu.gradeo.exceptions.entities.NoSuchInstituteException;
import sevsu.gradeo.repositories.DepartmentsRepository;
import sevsu.gradeo.repositories.InstituteRepository;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Component
//для редактирования кафедры
public class UpdateDepartmentHandler implements Command.Handler<UpdateDepartmentCommand, Voidy> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final InstituteRepository instituteRepository;
    private final DepartmentsRepository repository;

    public UpdateDepartmentHandler(InstituteRepository instituteRepository, DepartmentsRepository repository) {
        this.instituteRepository = instituteRepository;
        this.repository = repository;
    }

    @Override
    public Voidy handle(UpdateDepartmentCommand command) {
        logger.debug(LogStrings.FIND_BY_ID, "Department", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchDepartmentException::new);
        logger.debug(LogStrings.FIND_BY_ID, "Institute", command.getInstituteId());
        var institute = instituteRepository
                .findByIdAndRemovedDateIsNull(command.getInstituteId())
                .orElseThrow(NoSuchInstituteException::new);
        logger.debug("Setting name");
        entity.setName(command.getName());
        logger.debug("Setting institute");
        entity.setInstitute(institute);
        logger.debug("Setting updatedDate");
        entity.setUpdatedDate(LocalDateTime.now());

        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }
}
