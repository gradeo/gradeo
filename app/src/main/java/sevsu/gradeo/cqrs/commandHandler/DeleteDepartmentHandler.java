package sevsu.gradeo.cqrs.commandHandler;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.cqrs.command.DeleteDepartmentCommand;
import sevsu.gradeo.exceptions.entities.NoSuchDepartmentException;
import sevsu.gradeo.repositories.DepartmentsRepository;

import java.time.LocalDateTime;

@Component
//для удаления кафедры из БД
public class DeleteDepartmentHandler implements Command.Handler<DeleteDepartmentCommand, Voidy> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final DepartmentsRepository repository;

    public DeleteDepartmentHandler(DepartmentsRepository repository) {
        this.repository = repository;
    }

    @Override
    public Voidy handle(DeleteDepartmentCommand command) {
        logger.debug(LogStrings.FIND_BY_ID, "Department", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchDepartmentException::new);
        logger.debug("Setting removedDate");
        entity.setRemovedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }
}
