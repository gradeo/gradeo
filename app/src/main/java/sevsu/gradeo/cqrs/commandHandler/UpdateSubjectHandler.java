package sevsu.gradeo.cqrs.commandHandler;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.cqrs.command.UpdateSubjectCommand;
import sevsu.gradeo.entities.Teacher;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchDepartmentException;
import sevsu.gradeo.exceptions.entities.NoSuchSubjectException;
import sevsu.gradeo.exceptions.entities.NoSuchTeacherException;
import sevsu.gradeo.repositories.DepartmentsRepository;
import sevsu.gradeo.repositories.SubjectsRepository;
import sevsu.gradeo.repositories.TeachersRepository;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
@AllArgsConstructor
public class UpdateSubjectHandler implements Command.Handler<UpdateSubjectCommand, Voidy> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final SubjectsRepository repository;
    private final DepartmentsRepository departmentsRepository;
    private final TeachersRepository teachersRepository;

    @Override
    public Voidy handle(UpdateSubjectCommand command) {
        logger.debug(LogStrings.FIND_BY_ID, "Subject", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchSubjectException::new);

        logger.debug(LogStrings.FIND_BY_ID, "Department", command.getDepartmentId());
        var department = departmentsRepository
                .findByIdAndRemovedDateIsNull(command.getDepartmentId())
                .orElseThrow(NoSuchDepartmentException::new);

        logger.debug("Setting department with id: {}", department.getId());
        entity.setDepartment(department);
        logger.debug("Setting name");
        entity.setName(command.getName());
        logger.debug("Setting teachers");
        Set<Teacher> teachers = new HashSet<>();
        for (int ints : command.getTeachersId()) {
            var teacher = teachersRepository
                    .findByIdAndRemovedDateIsNull(ints)
                    .orElseThrow(NoSuchTeacherException::new);
            teachers.add(teacher);
        }
        entity.setTeachers(teachers);
        logger.debug("Setting updatedDate");
        entity.setUpdatedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }
}
