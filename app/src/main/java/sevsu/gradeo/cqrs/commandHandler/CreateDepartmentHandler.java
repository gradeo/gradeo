package sevsu.gradeo.cqrs.commandHandler;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.cqrs.command.CreateDepartmentCommand;
import sevsu.gradeo.entities.Department;
import sevsu.gradeo.exceptions.entities.NoSuchInstituteException;
import sevsu.gradeo.repositories.DepartmentsRepository;
import sevsu.gradeo.repositories.InstituteRepository;

import java.time.LocalDateTime;

@Component
public class CreateDepartmentHandler implements Command.Handler<CreateDepartmentCommand, Voidy> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final ModelMapper mapper;
    private final InstituteRepository instituteRepository;
    private final DepartmentsRepository repository;

    public CreateDepartmentHandler(ModelMapper mapper, InstituteRepository instituteRepository, DepartmentsRepository repository) {
        this.mapper = mapper;
        this.instituteRepository = instituteRepository;
        this.repository = repository;
    }

    @Override
    public Voidy handle(CreateDepartmentCommand command) {
        logger.debug("Mapping to Department.class");
        var entity = mapper.map(command, Department.class);
        logger.debug(LogStrings.FIND_BY_ID, "Institute", command.getInstituteId());
        var institute = instituteRepository
                .findByIdAndRemovedDateIsNull(command.getInstituteId())
                .orElseThrow(NoSuchInstituteException::new);
        logger.debug("Setting institute with id: {}", institute.getId());
        entity.setInstitute(institute);
        logger.debug("Setting createdDate");
        entity.setCreatedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }
}
