package sevsu.gradeo.cqrs.commandHandler;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.cqrs.command.DeleteInstituteCommand;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchInstituteException;
import sevsu.gradeo.repositories.InstituteRepository;

import java.time.LocalDateTime;

@Component
@AllArgsConstructor
//для удаления института
public class DeleteInstituteHandler implements Command.Handler<DeleteInstituteCommand, Voidy> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final InstituteRepository repository;

    @Override
    public Voidy handle(DeleteInstituteCommand command) {
        logger.debug(LogStrings.FIND_BY_ID, "Institute", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchInstituteException::new);
        logger.debug("Setting removedDate");
        entity.setRemovedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }
}
