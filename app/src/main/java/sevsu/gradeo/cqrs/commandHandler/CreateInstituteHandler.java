package sevsu.gradeo.cqrs.commandHandler;


import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.cqrs.command.CreateInstituteCommand;
import sevsu.gradeo.entities.Institute;
import sevsu.gradeo.repositories.InstituteRepository;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;

@Component
//создание института в БД
public class CreateInstituteHandler implements Command.Handler<CreateInstituteCommand, Voidy> {

    Logger logger = LoggerFactory.getLogger(getClass());
    private final ModelMapper mapper;
    private final InstituteRepository repository;

    public CreateInstituteHandler(ModelMapper mapper, InstituteRepository repository) {
        this.mapper = mapper;
        this.repository = repository;
    }

    @Override
    //преобразует команду в сущность и заносит в БД
    public Voidy handle(CreateInstituteCommand command) {
        logger.debug("Mapping to Institute.class");
        var entity = mapper.map(command, Institute.class);
        var localDateTime = LocalDateTime.now();
        logger.debug("Setting createdDate");
        entity.setCreatedDate(localDateTime);
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }

    @PostConstruct
    void configureMappings() {
        this.mapper.typeMap(CreateInstituteCommand.class, Institute.class)
                .addMappings(mapper -> mapper.skip(Institute::setId));
    }
}
