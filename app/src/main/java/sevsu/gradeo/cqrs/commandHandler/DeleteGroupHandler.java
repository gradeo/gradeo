package sevsu.gradeo.cqrs.commandHandler;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.cqrs.command.DeleteGroupCommand;
import sevsu.gradeo.exceptions.entities.NoSuchGroupException;
import sevsu.gradeo.repositories.GroupsRepository;

import java.time.LocalDateTime;

@Component
//Для удаления группы из БД
public class DeleteGroupHandler implements Command.Handler<DeleteGroupCommand, Voidy>{

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final GroupsRepository repository;

    public DeleteGroupHandler(GroupsRepository repository) {
        this.repository = repository;
    }

    @Override
    public Voidy handle(DeleteGroupCommand command){
        logger.debug(LogStrings.FIND_BY_ID, "Group", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchGroupException::new);
        logger.debug("Setting removedDate");
        entity.setRemovedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }
}
