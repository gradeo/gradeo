package sevsu.gradeo.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.dtos.tasks.CreateTaskDto;
import sevsu.gradeo.dtos.tasks.UpdateTaskDto;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchSubjectException;
import sevsu.gradeo.services.*;

import javax.validation.Valid;

@Controller
@RequestMapping("tasks")
public class TasksController {
    @Autowired
    SelectListService selectListService;
    @Autowired
    SubjectsService subjectsService;
    @Autowired
    TasksService tasksService;
    @Autowired
    AssessmentsService assessmentsService;
    @Autowired
    DepartmentsService departmentsService;


    Logger logger = LoggerFactory.getLogger(getClass());

    @GetMapping("new/subjectId/{id}")
    public String getCreatePageForSubject(
            @ModelAttribute("createTaskDto") CreateTaskDto createTaskDto,
            @PathVariable("id") Integer subjectId,
            Model model) {
        logger.debug(LogStrings.MODEL_ADD, "Task", createTaskDto);
        var subject = subjectsService.getSubjectById(subjectId);
        if (subject.isEmpty()) {
            logger.error(LogStrings.NOT_FOUND, "Subject", subjectId);
            return "redirect:/subjects";
        }
        model.addAttribute("subject", subject.get());
        return "Tasks/CreateForSubject";
    }

    @GetMapping("view/{id}")
    public String view(@PathVariable("id") Integer id, Model model) {
        var taskDto = tasksService.getTaskById(id);
        if (taskDto.isEmpty()) {
            logger.error(LogStrings.NOT_FOUND, "Task", id);
            return "redirect:/subjects";
        }
        model.addAttribute("task", taskDto.get());

        var assessmentsDtoList = assessmentsService.getAllAssessmentForTaskId(id);
        if (assessmentsDtoList.isEmpty()) {
            logger.error(LogStrings.NOT_FOUND, "assessments by taskId", id);
            model.addAttribute("assessments", assessmentsDtoList);
            return "Tasks/View";
        }
        model.addAttribute("assessments", assessmentsDtoList);
        return "Tasks/View";
    }

    @PostMapping("new/subjectId")
    public String createTaskForSubject(@ModelAttribute("createTaskDto") @Valid CreateTaskDto createTaskDto, BindingResult bindingResult, Model model) throws EntityNotFoundException {
        if (bindingResult.hasErrors()) {
            logger.debug(LogStrings.MODEL_ADD, "Task", createTaskDto);
            var subject = subjectsService.getSubjectById(createTaskDto.getSubjectId());
            if (subject.isEmpty()) {
                logger.error(LogStrings.NOT_FOUND, "Subject", createTaskDto.getSubjectId());
                return "redirect:/subjects";
            }
            model.addAttribute("subject", subject.get());
            logger.error(LogStrings.BINDING_HAS_ERRORS);
            return "Tasks/CreateForSubject";
        }
        try {
            logger.info(LogStrings.CREATE, "Task", createTaskDto.getName());
            tasksService.createTask(createTaskDto);
        } catch (NoSuchSubjectException e) {
            logger.debug(LogStrings.MODEL_ADD, "Task", createTaskDto);
            var subject = subjectsService.getSubjectById(createTaskDto.getSubjectId());
            if (subject.isEmpty()) {
                logger.error(LogStrings.NOT_FOUND, "Subject", createTaskDto.getSubjectId());
                return "redirect:/subjects";
            }
            model.addAttribute("subject", subject.get());
            logger.error(LogStrings.NO_SUCH, "Subject", createTaskDto.getSubjectId());
            bindingResult.rejectValue("subjectId", "error.createDto", "Невозможно использовать данный предмет");
            return "Tasks/CreateForSubject";
        }
        return "redirect:/subjects/view/" + createTaskDto.getSubjectId();
    }

    @GetMapping("edit/{id}")
    public String getUpdatePage(@PathVariable("id") Integer id, Model model) {
        logger.info(LogStrings.SEARCH, "Task", String.format("id = %d", id));
        var dto = tasksService.getTaskById(id);
        if (dto.isEmpty()) {
            logger.error(LogStrings.NOT_FOUND, "Task", id);
            return "redirect:/subjects";
        }
        logger.debug(LogStrings.MODEL_ADD, "Task", dto.get());
        model.addAttribute("task", dto.get());
        return "Tasks/Edit";
    }

    @PostMapping("edit")
    public String updateTask(@ModelAttribute("updateDto") @Valid UpdateTaskDto updateDto, BindingResult bindingResult) throws EntityNotFoundException {
        logger.debug(LogStrings.POST_REQUEST, "/edit");
        if (bindingResult.hasErrors()) {
            logger.warn(LogStrings.BINDING_HAS_ERRORS);
            return "Tasks/Edit";
        }
        try {
            logger.debug(LogStrings.UPDATE, "Task", updateDto.getId(), updateDto.getName());
            tasksService.updateTask(updateDto);
        } catch (NoSuchSubjectException e) {
            logger.error(LogStrings.NO_SUCH, "Department", updateDto.getSubjectId());
            bindingResult.rejectValue("subjectId", "error.createDto", "Невозможно использовать данный предмет");
            return "Tasks/Edit";
        }
        return "redirect:/subjects/view/" + updateDto.getSubjectId();
    }

    @PostMapping("delete/{id}")
    public String deleteTask(@PathVariable("id") Integer id) throws EntityNotFoundException {
        logger.info(LogStrings.DELETE, "Task", id);
        var taskDtoOptional = tasksService.getTaskById(id);
        if (taskDtoOptional.isEmpty()) {
            logger.error(LogStrings.NOT_FOUND, "Task", id);
            return "redirect:/subjects";
        }
        tasksService.deleteTaskById(id);
        return "redirect:/subjects/view/" + taskDtoOptional.get().getSubjectId();
    }

}
