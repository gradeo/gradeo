package sevsu.gradeo.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.dtos.students.CreateStudentDto;
import sevsu.gradeo.dtos.students.UpdateStudentDto;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchGroupException;
import sevsu.gradeo.services.SelectListService;
import sevsu.gradeo.services.StudentsService;

import javax.validation.Valid;

@Controller
@RequestMapping("students")
public class StudentsController {
    @Autowired
    StudentsService studentsService;
    @Autowired
    SelectListService selectListService;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("")
    public String index(Model model) {
        model.addAttribute("students", studentsService.getAllStudents());
        return "Students/Index";
    }

    @GetMapping("new")
    public String getCreatePage(@ModelAttribute("createDto") CreateStudentDto createStudentDto, Model model) {
        model.addAttribute("instituteOptions", selectListService.getInstitutesList());
        return "Students/Create";
    }

    @PostMapping("new")
    public String createStudent(@ModelAttribute("createDto") @Valid CreateStudentDto createDto, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            logger.warn(LogStrings.BINDING_HAS_ERRORS);
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(createDto.getInstituteId()));
            model.addAttribute("groupsInDepartment", selectListService.getGroupsListByDepartmentId(createDto.getDepartmentId()));
            return "Students/Create";
        }
        try {
            logger.info(LogStrings.CREATE, "Student", createDto.getName());
            studentsService.createStudent(createDto);
        } catch (NoSuchGroupException e) {
            logger.error(LogStrings.NO_SUCH, "Group", createDto.getGroupId());
            bindingResult.rejectValue("groupId", "error.createDto", "Невозможно использовать данную группу");
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            return "Students/Create";
        }
        return "redirect:/students";
    }

    @GetMapping("edit/{id}")
    public String getUpdatePage(@PathVariable("id") Integer id, Model model) {
        logger.debug(LogStrings.SEARCH, "Student", id);
        var dto = studentsService.getStudentById(id);
        if (dto.isEmpty()) {
            logger.error(LogStrings.NOT_FOUND, "Student", id);
            return "redirect:/students";
        }
        logger.debug(LogStrings.MODEL_ADD, "Student", dto.get().getName());
        model.addAttribute("updateDto", dto.get());
        model.addAttribute("instituteOptions", selectListService.getInstitutesList());
        model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(dto.get().getInstituteId()));
        model.addAttribute("groupsInDepartment", selectListService.getGroupsListByDepartmentId(dto.get().getDepartmentId()));
        return "Students/Edit";
    }

    @PostMapping("edit")
    public String updateStudent(@ModelAttribute("updateDto") @Valid UpdateStudentDto updateDto, BindingResult bindingResult, Model model) throws EntityNotFoundException {
        if (bindingResult.hasErrors()) {
            logger.warn(LogStrings.BINDING_HAS_ERRORS);
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(updateDto.getInstituteId()));
            model.addAttribute("groupsInDepartment", selectListService.getGroupsListByDepartmentId(updateDto.getDepartmentId()));
            return "Students/Edit";
        }
        try {
            logger.debug(LogStrings.UPDATE, "Student", updateDto.getId(), updateDto.getName());
            studentsService.updateStudent(updateDto);
        } catch (NoSuchGroupException e) {
            logger.error(LogStrings.NO_SUCH, "Group", updateDto.getGroupId());
            bindingResult.rejectValue("groupId", "error.updateDto", "Невозможно использовать данную группу");
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(updateDto.getInstituteId()));
            model.addAttribute("groupsInDepartment", selectListService.getGroupsListByDepartmentId(updateDto.getDepartmentId()));
            return "Students/Create";
        }

        return "redirect:/students";
    }

    @PostMapping("delete/{id}")
    public String deleteStudent(@PathVariable("id") Integer id) throws EntityNotFoundException {
        logger.info(LogStrings.DELETE, "Student", id);
        studentsService.deleteStudentById(id);
        return "redirect:/students";
    }

}
