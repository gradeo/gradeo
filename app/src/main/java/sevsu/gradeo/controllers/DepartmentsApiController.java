package sevsu.gradeo.controllers;

import an.awesome.pipelinr.Pipeline;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import sevsu.gradeo.cqrs.command.CreateDepartmentCommand;
import sevsu.gradeo.cqrs.command.DeleteDepartmentCommand;
import sevsu.gradeo.cqrs.command.UpdateDepartmentCommand;
import sevsu.gradeo.cqrs.query.GetDepartmentByIdQuery;
import sevsu.gradeo.cqrs.query.GetDepartmentsQuery;
import sevsu.gradeo.dtos.departments.DepartmentsViewDto;

import java.util.List;

@RestController
@RequestMapping("api/departments")
public class DepartmentsApiController {

    public final Pipeline pipeline;

    public DepartmentsApiController(Pipeline pipeline) {
        this.pipeline = pipeline;
    }

    @GetMapping
    @ResponseBody
    //возвращает список всех кафедр
    public List<DepartmentsViewDto> getAllDepartments() {
        return pipeline.send(new GetDepartmentsQuery());
    }

    @GetMapping("{id}")
    @ResponseBody
    //возвращает кафедру по id
    public DepartmentsViewDto getDepartmentById(@PathVariable int id) {
        return pipeline.send(new GetDepartmentByIdQuery(id));
    }

    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    //добавляет новую кафедру в базу
    public void createNewDepartment(@RequestBody CreateDepartmentCommand createDepartmentCommand) {
        pipeline.send(createDepartmentCommand);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    //редактирует кафедру в базе
    public void updateDepartment(@RequestBody UpdateDepartmentCommand updateDepartmentCommand) {
        pipeline.send(updateDepartmentCommand);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    //удаяет кафедру из базы по id
    public void deleteDepartmentById(@PathVariable int id){
        pipeline.send(new DeleteDepartmentCommand(id));
    }
}
