package sevsu.gradeo.controllers;

import an.awesome.pipelinr.Pipeline;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sevsu.gradeo.cqrs.command.CreateInstituteCommand;
import sevsu.gradeo.cqrs.command.DeleteInstituteCommand;
import sevsu.gradeo.cqrs.query.GetInstituteQuery;
import sevsu.gradeo.cqrs.query.GetInstitutesQuery;
import sevsu.gradeo.dtos.institutes.InstituteViewDto;

import java.util.List;

@RestController
@RequestMapping("api/institutes")
public class InstitutesApiController {

    private final Pipeline pipeline;

    public InstitutesApiController(Pipeline pipeline) {
        this.pipeline = pipeline;
    }

    @GetMapping
    @ResponseBody
    //список институтов
    public List<InstituteViewDto> getAllInstitutes() {
        return pipeline.send(new GetInstitutesQuery());
    }

    @GetMapping("{id}")
    @ResponseBody
    //институт по ид
    public InstituteViewDto getInstitute(@PathVariable Integer id) {
        return pipeline.send(new GetInstituteQuery(id));
    }

    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    //создание института
    public void createInstituteFromName(@RequestBody CreateInstituteCommand createInstituteCommand) {
        pipeline.send(createInstituteCommand);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    //удаление института по ид
    public void deleteInstituteById(@PathVariable Integer id) {
        pipeline.send(new DeleteInstituteCommand(id));
    }
}
