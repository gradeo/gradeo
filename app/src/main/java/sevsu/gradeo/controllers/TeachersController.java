package sevsu.gradeo.controllers;

import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.dtos.teachers.UpdateTeacherDto;
import sevsu.gradeo.entities.Teacher;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchDepartmentException;
import sevsu.gradeo.services.SelectListService;
import sevsu.gradeo.services.TeachersService;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("teachers")
public class TeachersController {
    @Autowired
    TeachersService teachersService;
    @Autowired
    SelectListService selectListService;

    Logger logger = LoggerFactory.getLogger(getClass());

    @GetMapping("")
    public String index(Model model) {
        model.addAttribute("teachers", teachersService.getAllTeachers());
        return "Teachers/Index";
    }

    @GetMapping("edit/{id}")
    public String getUpdatePage(@PathVariable("id") Integer id, Model model) {
        logger.info(LogStrings.SEARCH, "Teacher", id);
        Optional<UpdateTeacherDto> updateTeacherDto = teachersService.getTeacherById(id);
        UpdateTeacherDto teacher;
        if (updateTeacherDto.isPresent()) {
            teacher = updateTeacherDto.get();
        } else {
            logger.error(LogStrings.NOT_FOUND, "Department", id);
            return "redirect:/institutes";
        }
        logger.debug(LogStrings.MODEL_ADD, "Group", teacher.getName());
        model.addAttribute("teacher", teacher);
        model.addAttribute("instituteOptions", selectListService.getInstitutesList());
        model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(teacher.getInstituteId()));
        model.addAttribute("subjectsInDepartment", selectListService.getSubjectListByDepartmentId(teacher.getDepartmentId()));
        model.addAttribute("groupsInDepartment", selectListService.getGroupsListByDepartmentId(teacher.getDepartmentId()));
        return "Teachers/Edit";
    }

    @PostMapping("edit")
    public String updateTeacher(
            @ModelAttribute("teacher") @Valid UpdateTeacherDto updateDto,
            BindingResult bindingResult,
            Model model
    ) throws EntityNotFoundException {
        if (bindingResult.hasErrors()) {
            logger.warn(LogStrings.BINDING_HAS_ERRORS);
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(updateDto.getInstituteId()));
            model.addAttribute("subjectsInDepartment", selectListService.getSubjectListByDepartmentId(updateDto.getDepartmentId()));
            return "Teachers/Edit";
        }
        try {
            logger.info(LogStrings.UPDATE, "Teacher", updateDto.getId(), updateDto.getName());
            teachersService.updateTeacher(updateDto);
        } catch (NoSuchDepartmentException e) {
            logger.error(LogStrings.NO_SUCH, "Department", updateDto.getDepartmentId());
            bindingResult.rejectValue("departmentId", "error.teacher", "Невозможно использовать данную кафедру");
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(updateDto.getInstituteId()));
            model.addAttribute("subjectsInDepartment", selectListService.getSubjectListByDepartmentId(updateDto.getDepartmentId()));
        }
        return "redirect:/teachers";
    }
}
