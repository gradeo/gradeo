package sevsu.gradeo.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.dtos.institutes.CreateInstituteDto;
import sevsu.gradeo.dtos.institutes.UpdateInstituteDto;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.services.InstituteService;

import javax.validation.Valid;

@Controller
@RequestMapping("institutes")
public class InstitutesController {
    @Autowired
    InstituteService institutesService;

    Logger logger = LoggerFactory.getLogger(getClass());

    @GetMapping("")
    public String index(Model model,
                        Pageable pageable,
                        @RequestParam(defaultValue = "") String name,
                        @RequestParam(defaultValue = "off") Boolean isDeleted,
                        @RequestParam(value = "page", defaultValue = "0") Integer page,
                        @RequestParam(value = "sort", defaultValue = "id") String sort,
                        @RequestParam(value = "size", defaultValue = "20") Integer size) {
        var institutePageDto = institutesService.getAllInstitutes(
                name,
                isDeleted,
                pageable);
        model.addAttribute("model", institutePageDto);
        model.addAttribute("size", size);
        model.addAttribute("sort", sort);
        model.addAttribute("name", name);
        model.addAttribute("isDeleted", isDeleted);

        return "Institutes/Index";
    }

    @GetMapping("new")
    public String getCreatePage(@ModelAttribute("createDto") CreateInstituteDto createDto) {
        return "Institutes/Create";
    }

    @PostMapping("new")
    public String create(@ModelAttribute("createDto") @Valid CreateInstituteDto createDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            logger.error(LogStrings.BINDING_HAS_ERRORS);
            return "Institutes/Create";
        }
        logger.info(LogStrings.CREATE, "Institute", createDto.getName());
        institutesService.createInstitute(createDto);
        return "redirect:/institutes";
    }

    @GetMapping("edit/{id}")
    public String getUpdatePage(@PathVariable("id") Integer id, Model model) {
        logger.info(LogStrings.SEARCH, "Institute", id);
        var dto = institutesService.getInstituteById(id);
        if (dto.isEmpty()) {
            logger.warn(LogStrings.NOT_FOUND, "Institute", id);
            return "redirect:/institutes";
        }
        logger.debug(LogStrings.MODEL_ADD, "Institute", dto.get().getName());
        model.addAttribute("institute", dto.get());
        return "Institutes/Edit";
    }

    @PostMapping("edit")
    public String update(@ModelAttribute("institute") @Valid UpdateInstituteDto institute, BindingResult bindingResult) throws EntityNotFoundException {
        if (bindingResult.hasErrors()) {
            logger.warn(LogStrings.BINDING_HAS_ERRORS);
            return "Institutes/Edit";
        }
        logger.info(LogStrings.UPDATE, "Institute", institute.getId(), institute.getName());
        institutesService.updateInstitute(institute);
        return "redirect:/institutes";
    }

    @PostMapping("delete/{id}")
    public String delete(@PathVariable("id") Integer id) throws EntityNotFoundException {
        logger.info(LogStrings.DELETE, "Institute", id);
        institutesService.deleteInstituteById(id);
        return "redirect:/institutes";
    }
}

