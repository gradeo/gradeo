package sevsu.gradeo.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import sevsu.gradeo.dtos.lessons.CalendarLessonDto;
import sevsu.gradeo.entities.User;
import sevsu.gradeo.services.LessonsService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
@RequestMapping("calendar")
public class CalendarController {
    @Autowired
    LessonsService lessonsService;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("")
    public String index() {
        logger.debug("check user==teacher");
        User user = (User) SecurityContextHolder
                .getContext().getAuthentication().getPrincipal();
        if (user.getTeacher() == null) {
            return "Errors/403";
        }
        return "Calendar/Index";
    }

    @GetMapping("current_teacher")
    @ResponseBody
    public List<CalendarLessonDto> loadLessonsByTeacherId(
            @RequestParam("start") String strStart,
            @RequestParam("end") String strEnd
    ) {
        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
        LocalDateTime start = LocalDateTime.parse(strStart, formatter);
        LocalDateTime end = LocalDateTime.parse(strEnd, formatter);
        return lessonsService.getLessonsByCurrentTeacherIdFromCalendar(start, end);
    }
}
