package sevsu.gradeo.controllers;

import an.awesome.pipelinr.Pipeline;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import sevsu.gradeo.cqrs.command.DeleteGroupCommand;

@RestController
@RequestMapping("api/groups")
public class GroupsApiController {

    public final Pipeline pipeline;

    public GroupsApiController(Pipeline pipeline) {
        this.pipeline = pipeline;
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    //удаяет группу из базы по id
    public void deleteGroupById(@PathVariable int id){
        pipeline.send(new DeleteGroupCommand(id));
    }
}
