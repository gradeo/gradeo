package sevsu.gradeo.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.dtos.lessons.CreateLessonDto;
import sevsu.gradeo.dtos.lessons.UpdateLessonDto;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchGroupException;
import sevsu.gradeo.exceptions.entities.NoSuchSubjectException;
import sevsu.gradeo.exceptions.entities.NoSuchTeacherException;
import sevsu.gradeo.services.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Controller
@RequestMapping("lessons")
public class LessonsController {
    @Autowired
    LessonsService lessonsService;
    @Autowired
    SubjectsService subjectsService;
    @Autowired
    GroupsService groupsService;
    @Autowired
    AttendanceService attendanceService;
    @Autowired
    SelectListService selectListService;
    @Autowired
    AmqpTemplate template;
    Logger logger = LoggerFactory.getLogger(getClass());

    @GetMapping("")
    public String index(Model model) {
        model.addAttribute("lessons", lessonsService.getAllLessons());
        return "Lessons/Index";
    }

    @GetMapping("view/{id}")
    public String view(@PathVariable("id") Integer id, Model model) {
        var lessonDto = lessonsService.getLessonById(id);
        if (lessonDto.isEmpty()) {
            logger.error(LogStrings.NOT_FOUND, "Lesson", id);
            return "redirect:/lessons";
        }
        model.addAttribute("lesson", lessonDto.get());

        var attendanceDtoList = attendanceService.getAllAttendancesForLesson(id);
        if (attendanceDtoList.isEmpty()) {
            logger.error(LogStrings.NOT_FOUND, "Attendance", id);
            model.addAttribute("lesson", lessonDto.get());
            model.addAttribute("attendances", attendanceDtoList);
            return "Lessons/View";
        }
        model.addAttribute("attendances", attendanceDtoList);
        return "Lessons/View";
    }

    @GetMapping("new")
    public String getCreatePage(@ModelAttribute("createDto") CreateLessonDto createDto, Model model) {
        model.addAttribute("instituteOptions", selectListService.getInstitutesList());
        model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(createDto.getInstituteId()));
        model.addAttribute("groupsInDepartment", selectListService.getGroupsListByDepartmentId(createDto.getDepartmentId()));
        model.addAttribute("subjectsInDepartment", selectListService.getSubjectListByDepartmentId(createDto.getDepartmentId()));
        model.addAttribute("teachersInDepartment", selectListService.getTeachersListByDepartmentId(createDto.getDepartmentId()));
        return "Lessons/Create";
    }

    @PostMapping("new")
    public String create(@ModelAttribute("createDto") @Valid CreateLessonDto createDto, BindingResult bindingResult, Model model) throws NoSuchTeacherException {
        if (bindingResult.hasErrors()) {
            logger.warn(LogStrings.BINDING_HAS_ERRORS);
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(createDto.getInstituteId()));
            model.addAttribute("groupsInDepartment", selectListService.getGroupsListByDepartmentId(createDto.getDepartmentId()));
            model.addAttribute("subjectsInDepartment", selectListService.getSubjectListByDepartmentId(createDto.getDepartmentId()));
            model.addAttribute("teachersInDepartment", selectListService.getTeachersListByDepartmentId(createDto.getDepartmentId()));
            return "Lessons/Create";
        }
        try {
            logger.info(LogStrings.CREATE, "Lesson", "== subject name");
            lessonsService.createLesson(createDto);
        } catch (NoSuchSubjectException e) {
            logger.error(LogStrings.NO_SUCH, "Subject");
            bindingResult.rejectValue("subjectId", "error.createDto", "Невозможно использовать данный предмет");
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(createDto.getInstituteId()));
            model.addAttribute("groupsInDepartment", selectListService.getGroupsListByDepartmentId(createDto.getDepartmentId()));
            model.addAttribute("subjectsInDepartment", selectListService.getSubjectListByDepartmentId(createDto.getDepartmentId()));
            model.addAttribute("teachersInDepartment", selectListService.getTeachersListByDepartmentId(createDto.getDepartmentId()));
            return "Lessons/Create";
        } catch (NoSuchGroupException e) {
            logger.error(LogStrings.NO_SUCH, "Group");
            bindingResult.rejectValue("groupId", "error.createDto", "Невозможно использовать данную группу");
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(createDto.getInstituteId()));
            model.addAttribute("groupsInDepartment", selectListService.getGroupsListByDepartmentId(createDto.getDepartmentId()));
            model.addAttribute("subjectsInDepartment", selectListService.getSubjectListByDepartmentId(createDto.getDepartmentId()));
            model.addAttribute("teachersInDepartment", selectListService.getTeachersListByDepartmentId(createDto.getDepartmentId()));
            return "Lessons/Create";
        }

        return "redirect:/lessons";
    }

    @GetMapping("edit/{id}")
    public String getUpdatePage(@PathVariable("id") Integer id, Model model) {
        logger.info(LogStrings.SEARCH, "Lesson", id);
        var dto = lessonsService.getUpdateLessonById(id);
        if (dto.isEmpty()) {
            logger.error(LogStrings.NOT_FOUND, "Lesson", id);
            return "redirect:/lessons";
        }
        logger.debug(LogStrings.MODEL_ADD, "Lesson", dto.get().getSubjectName());
        model.addAttribute("updateDto", dto.get());
        return "Lessons/Edit";
    }

    @PostMapping("edit")
    public String update(@ModelAttribute("updateDto") @Valid UpdateLessonDto updateDto, BindingResult bindingResult, Model model) throws EntityNotFoundException {
        if (bindingResult.hasErrors()) {
            logger.warn(LogStrings.BINDING_HAS_ERRORS);
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(updateDto.getInstituteId()));
            model.addAttribute("groupsInDepartment", selectListService.getGroupsListByDepartmentId(updateDto.getDepartmentId()));
            model.addAttribute("subjectsInDepartment", selectListService.getSubjectListByDepartmentId(updateDto.getDepartmentId()));
            model.addAttribute("teachersInDepartment", selectListService.getTeachersListByDepartmentId(updateDto.getDepartmentId()));
            return "Lessons/Edit";
        }

        try {
            logger.info(LogStrings.UPDATE, "Lesson", updateDto.getId(), updateDto.getSubjectName());
            lessonsService.updateLesson(updateDto);
        } catch (NoSuchSubjectException e) {
            logger.error(LogStrings.NO_SUCH, "Subject", updateDto.getSubjectId());
            bindingResult.rejectValue("subjectId", "error.updateDto", "Невозможно использовать данный предмет");
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(updateDto.getInstituteId()));
            model.addAttribute("groupsInDepartment", selectListService.getGroupsListByDepartmentId(updateDto.getDepartmentId()));
            model.addAttribute("subjectsInDepartment", selectListService.getSubjectListByDepartmentId(updateDto.getDepartmentId()));
            model.addAttribute("teachersInDepartment", selectListService.getTeachersListByDepartmentId(updateDto.getDepartmentId()));
            return "Lessons/Create";
        } catch (NoSuchGroupException e) {
            logger.error(LogStrings.NO_SUCH, "Group", updateDto.getGroupId());
            bindingResult.rejectValue("groupId", "error.updateDto", "Невозможно использовать данную группу");
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(updateDto.getInstituteId()));
            model.addAttribute("groupsInDepartment", selectListService.getGroupsListByDepartmentId(updateDto.getDepartmentId()));
            model.addAttribute("subjectsInDepartment", selectListService.getSubjectListByDepartmentId(updateDto.getDepartmentId()));
            model.addAttribute("teachersInDepartment", selectListService.getTeachersListByDepartmentId(updateDto.getDepartmentId()));
            return "Lessons/Create";
        }
        return "redirect:/lessons";
    }

    @PostMapping("delete/{id}")
    public String delete(@PathVariable("id") Integer id) throws EntityNotFoundException {
        logger.info(LogStrings.DELETE, "Lesson", id);
        lessonsService.deleteLessonById(id);
        return "redirect:/lessons";
    }

    @GetMapping("report/attendance/{id}")
    public void index(@PathVariable("id") Integer id, HttpServletResponse response) {
        var lessonDto = lessonsService.getLessonByIdForReport(id);
        if (lessonDto.isEmpty()) {
            logger.error(LogStrings.NOT_FOUND, "Lesson", id);
            response.setStatus(500); // занятие не существует или удалено
        }

        DateTimeFormatter pattern = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        if (LocalDateTime
                .parse(lessonDto.get().getDate(), pattern)
                .isAfter(LocalDateTime.now())) {
            response.setStatus(600); // занятие еще не проведено

        } else {
            try {
                byte[] responseWorkbook = (byte[]) template.convertSendAndReceive("generateLessonAttendanceReport", lessonDto.get());

                if (responseWorkbook != null) {
                    String fileName = lessonDto.get().getDate()
                            + "-"
                            + lessonDto.get().getSubjectName()
                            + "-"
                            + lessonDto.get().getGroupName()
                            + "-посещение.xls";
                    String encodeFilename = URLEncoder.encode(fileName, StandardCharsets.UTF_8);
                    response.setHeader("Content-Disposition", encodeFilename);
                    response.setContentType("application/octet-stream");
                    response.getOutputStream().write(responseWorkbook);
                    response.getOutputStream().flush();
                    response.getOutputStream().close();
                } else {
                    response.setStatus(601); // ошибка генерации xls
                }

            } catch (IOException e) {
                response.setStatus(602); // IO исключение
                System.out.println(e.toString());
            }

        }
    }
}