package sevsu.gradeo.controllers;

import an.awesome.pipelinr.Pipeline;
import lombok.AllArgsConstructor;
import sevsu.gradeo.cqrs.query.GetSubjectQuery;
import org.springframework.web.bind.annotation.*;
import sevsu.gradeo.cqrs.command.UpdateSubjectCommand;
import sevsu.gradeo.cqrs.query.GetSubjectsQuery;
import sevsu.gradeo.dtos.subjects.SubjectViewDto;

import java.util.List;

@RestController
@RequestMapping("api/subjects")
@AllArgsConstructor
public class SubjectsApiController {

    private final Pipeline pipeline;

    @GetMapping
    public List<SubjectViewDto> getAllSubjects() {
        return pipeline.send(new GetSubjectsQuery());
    }

    @GetMapping("{id}")
    SubjectViewDto getSubjectById(@PathVariable Integer id) {
        return pipeline.send(new GetSubjectQuery(id));
    }

    @PostMapping
    public void updateSubject(@RequestBody UpdateSubjectCommand updateSubjectCommand) {
        pipeline.send(updateSubjectCommand);
    }

}
