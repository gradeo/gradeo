package sevsu.gradeo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import sevsu.gradeo.services.BakersService;

@Controller
@RequestMapping("about")
public class AboutController {
    @Autowired
    BakersService bakersService;

    @GetMapping("")
    public String about(Model model) {
        model.addAttribute("bakers", bakersService.getAllBakers());
        return "About";
    }
}
