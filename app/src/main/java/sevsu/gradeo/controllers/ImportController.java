package sevsu.gradeo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.SomeEntitiesInAnswersException;
import sevsu.gradeo.exceptions.WrongEducationTypeException;
import sevsu.gradeo.exceptions.entities.NoSuchInstituteException;
import sevsu.gradeo.services.ImportService;

import java.io.IOException;

@Controller
@RequestMapping("import")
public class ImportController {
    @Autowired
    ImportService importService;

    @GetMapping("")
    public String index() {
        return "Import/Index";
    }

    @PostMapping("groupsFromExcel")
    public String groupsImport (@RequestParam("file") MultipartFile file){
        try {
            importService.importGroupFromExcel(file);
        } catch (SomeEntitiesInAnswersException | IOException | WrongEducationTypeException | EntityNotFoundException e) {
            return "redirect:/Errors/500";
        }
        return "redirect:/import";
    }

    @PostMapping("lessonsFromExcel")
    public String lessonsImport (@RequestParam("file") MultipartFile file){
        try {
            importService.importLessonsFromExcel(file);
        } catch (SomeEntitiesInAnswersException | IOException | EntityNotFoundException e) {
            return "Errors/500";
        }
        return "redirect:/import";
    }

}
