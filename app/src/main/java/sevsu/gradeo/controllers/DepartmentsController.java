package sevsu.gradeo.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.dtos.departments.CreateDepartmentDto;
import sevsu.gradeo.dtos.departments.UpdateDepartmentDto;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchInstituteException;
import sevsu.gradeo.services.DepartmentsService;
import sevsu.gradeo.services.SelectListService;

import javax.validation.Valid;

@Controller
@RequestMapping("departments")
public class DepartmentsController {
    @Autowired
    DepartmentsService departmentsService;
    @Autowired
    SelectListService selectListService;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("")
    public String index(Model model,
                        Pageable pageable,
                        @RequestParam(defaultValue = "") String name,
                        @RequestParam(value = "inst_id", defaultValue = "0") int instituteId,
                        @RequestParam(defaultValue = "off") boolean isDeleted,
                        @RequestParam(value = "page", defaultValue = "0") Integer page,
                        @RequestParam(value = "sort", defaultValue = "id") String sort,
                        @RequestParam(value = "size", defaultValue = "20") Integer size) {
        var departmentPageDto = departmentsService.getAllDepartments(
                name,
                instituteId,
                isDeleted,
                pageable);
        model.addAttribute("model", departmentPageDto);
        model.addAttribute("size", size);
        model.addAttribute("sort", sort);
        model.addAttribute("name", name);
        model.addAttribute("isDeleted", isDeleted);
        model.addAttribute("inst_id", instituteId);
        model.addAttribute("instituteOptions", selectListService.getInstitutesList());
        return "Departments/Index";
    }

    @GetMapping("new")
    public String getCreatePage(@ModelAttribute("createDto") CreateDepartmentDto createDto, Model model) {
        model.addAttribute("instituteOptions", selectListService.getInstitutesList());
        return "Departments/Create";
    }

    @PostMapping("new")
    public String create(@ModelAttribute("createDto") @Valid CreateDepartmentDto createDto, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            logger.warn(LogStrings.BINDING_HAS_ERRORS);
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            return "Departments/Create";
        }

        try {
            logger.info(LogStrings.CREATE, "Department", createDto.getName());
            departmentsService.createDepartment(createDto);
        } catch (NoSuchInstituteException e) {
            logger.error(LogStrings.NO_SUCH, "Institute", createDto.getInstituteId());
            bindingResult.rejectValue("instituteId", "error.createDto", "Невозможно использовать данный институт");
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            return "Departments/Create";
        }

        return "redirect:/departments";
    }

    @GetMapping("edit/{id}")
    public String getUpdatePage(@PathVariable("id") Integer id, Model model) {
        logger.debug(LogStrings.SEARCH, "Department", id);
        var dto = departmentsService.getDepartmentById(id);
        if (dto.isEmpty()) {
            logger.error(LogStrings.NOT_FOUND, "Department", id);
            return "redirect:/departments";
        }
        logger.debug(LogStrings.MODEL_ADD, "Department", dto.get().getName());
        model.addAttribute("department", dto.get());
        model.addAttribute("instituteOptions", selectListService.getInstitutesList());
        return "Departments/Edit";
    }

    @PostMapping("edit")
    public String update(@ModelAttribute("department") @Valid UpdateDepartmentDto department, BindingResult bindingResult, Model model) throws EntityNotFoundException {
        if (bindingResult.hasErrors()) {
            logger.warn(LogStrings.BINDING_HAS_ERRORS);
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            return "Departments/Edit";
        }

        try {
            logger.debug(LogStrings.UPDATE, "Department", department.getId(), department.getName());
            departmentsService.updateDepartment(department);
        } catch (NoSuchInstituteException e) {
            logger.error(LogStrings.NO_SUCH, "Institute", department.getInstituteId());
            bindingResult.rejectValue("instituteId", "error.department", "Невозможно использовать данный институт");
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            return "Departments/Create";
        }

        return "redirect:/departments";
    }

    @PostMapping("delete/{id}")
    public String delete(@PathVariable("id") Integer id) throws EntityNotFoundException {
        logger.info(LogStrings.DELETE, "Department", id);
        departmentsService.deleteDepartmentById(id);
        return "redirect:/departments";
    }
}

