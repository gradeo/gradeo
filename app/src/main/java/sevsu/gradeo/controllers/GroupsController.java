package sevsu.gradeo.controllers;

import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.dtos.groups.CreateGroupDto;
import sevsu.gradeo.dtos.groups.UpdateGroupDto;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchDepartmentException;
import sevsu.gradeo.exceptions.entities.NoSuchTeacherException;
import sevsu.gradeo.services.*;

import javax.validation.Valid;

@Controller
@RequestMapping("groups")
public class GroupsController {
    @Autowired
    GroupsService groupsService;
    @Autowired
    StudentsService studentsService;
    @Autowired
    SubjectsService subjectsService;
    @Autowired
    SelectListService selectListService;
    @Autowired
    TeachersService teachersService;

    private Logger logger = LoggerFactory.getLogger(getClass());

    @GetMapping("")
    public String index(Model model) {
        model.addAttribute("groups", groupsService.getAllGroups());
        return "Groups/Index";
    }

    @GetMapping("view/{id}")
    public String view(@PathVariable("id") Integer id, Model model) {
        var group = groupsService.getGroupById(id);
        if (group.isEmpty()) {
            logger.error(LogStrings.NOT_FOUND, "Group", id);
            return "redirect:/groups";
        }
        var students = studentsService.getStudentsByGroupId(id);

        var subjects = subjectsService.getSubjectsByGroupId(id);
        model.addAttribute("group", group.get());
        model.addAttribute("students", students);
        model.addAttribute("subjects", subjects);

        return "Groups/View";
    }

    @GetMapping("new")
    public String getCreatePage(@ModelAttribute("createDto") CreateGroupDto createDto, Model model) {
        model.addAttribute("instituteOptions", selectListService.getInstitutesList());
        return "Groups/Create";
    }

    @PostMapping("new")
    public String createGroup(@ModelAttribute("createDto") @Valid CreateGroupDto createDto, BindingResult bindingResult, Model model) throws EntityNotFoundException {
        if (bindingResult.hasErrors()) {
            logger.warn(LogStrings.BINDING_HAS_ERRORS);
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(createDto.getInstituteId()));
            model.addAttribute("teachersInDepartment", selectListService.getFreeTeachers(createDto.getDepartmentId()));
            model.addAttribute("subjectsInDepartment", selectListService.getSubjectListByDepartmentId(createDto.getDepartmentId()));
            return "Groups/Create";
        }
        if (teachersService.isTeacherHaveGroup(createDto.getTeacherId())) {
            logger.warn(LogStrings.BINDING_HAS_ERRORS);
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(createDto.getInstituteId()));
            model.addAttribute("teachersInDepartment", selectListService.getFreeTeachers(createDto.getDepartmentId()));
            model.addAttribute("subjectsInDepartment", selectListService.getSubjectListByDepartmentId(createDto.getDepartmentId())); //TODO: мультиселект на предметы при создании группы
            bindingResult.rejectValue("teacherId", "error.createGroupDto", "Данный преподаватель курирует другую группу");
            return "Groups/Create";
        }
        try {
            logger.info(LogStrings.CREATE, "Group", createDto.getName());
            groupsService.createGroup(createDto);
        } catch (NoSuchDepartmentException e) {
            logger.error(LogStrings.NO_SUCH, "Department");
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(createDto.getInstituteId()));
            model.addAttribute("teachersInDepartment", selectListService.getFreeTeachers(createDto.getDepartmentId()));
            model.addAttribute("subjectsInDepartment", selectListService.getSubjectListByDepartmentId(createDto.getDepartmentId()));
            bindingResult.rejectValue("departmentId", "error.createGroupDto", "Невозможно использовать данную кафедру");
            return "Groups/Create";
        }
        return "redirect:/groups";
    }

    @GetMapping("edit/{id}")
    public String getUpdatePage(@PathVariable("id") Integer id, Model model) throws NoSuchTeacherException {
        logger.info(LogStrings.SEARCH, "Group", id);
        val groupOptional = groupsService.getGroupByIdForUpdate(id);
        if (groupOptional.isEmpty()) {
            logger.error(LogStrings.NOT_FOUND, "Department", id);
            return "redirect:/institutes";
        }
        val group = groupOptional.get();
        logger.debug(LogStrings.MODEL_ADD, "Group", group.getName());
        model.addAttribute("updateDto", group);
        model.addAttribute("instituteOptions", selectListService.getInstitutesList());
        model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(group.getInstituteId()));
        model.addAttribute("subjectsInDepartment", selectListService.getSubjectListByDepartmentId(group.getDepartmentId()));
        model.addAttribute("teachersInDepartment", selectListService.getFreeTeachers(group.getDepartmentId()));
        model.addAttribute("currentTeacher", teachersService.getTeacherById(group.getTeacherId()).orElseThrow(NoSuchTeacherException::new));
        return "Groups/Edit";
    }

    @PostMapping("edit")
    public String updateGroup(@ModelAttribute("updateDto") @Valid UpdateGroupDto updateDto, BindingResult bindingResult, Model model) throws EntityNotFoundException {
        if (bindingResult.hasErrors()) {
            logger.warn(LogStrings.BINDING_HAS_ERRORS);
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(updateDto.getInstituteId()));
            model.addAttribute("subjectsInDepartment", selectListService.getSubjectListByDepartmentId(updateDto.getDepartmentId()));
            model.addAttribute("teachersInDepartment", selectListService.getFreeTeachers(updateDto.getDepartmentId()));
            model.addAttribute("currentTeacher", teachersService.getTeacherById(updateDto.getTeacherId()).orElseThrow(NoSuchTeacherException::new));
            return "Groups/Edit";
        }
        try {
            logger.info(LogStrings.UPDATE, "Group", updateDto.getId(), updateDto.getName());
            groupsService.updateGroup(updateDto);
        } catch (NoSuchDepartmentException e) {
            logger.error(LogStrings.NO_SUCH, "Department", updateDto.getDepartmentId());
            bindingResult.rejectValue("departmentId", "error.group", "Невозможно использовать данную кафедру");
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(updateDto.getInstituteId()));
            model.addAttribute("subjectsInDepartment", selectListService.getSubjectListByDepartmentId(updateDto.getDepartmentId()));
            model.addAttribute("currentTeacher", teachersService.getTeacherById(updateDto.getTeacherId()).orElseThrow(NoSuchTeacherException::new));
        }

        return "redirect:/groups/view/" + updateDto.getId();
    }

    @PostMapping("delete/{id}")
    public String deleteGroup(@PathVariable("id") Integer id) throws EntityNotFoundException {
        logger.info(LogStrings.DELETE, "Group", id);
        groupsService.deleteGroupById(id);
        return "redirect:/groups";
    }
}
