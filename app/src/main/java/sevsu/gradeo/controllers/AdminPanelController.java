package sevsu.gradeo.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.dtos.users.UpdateUserDto;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.LastUserException;
import sevsu.gradeo.exceptions.entities.NoSuchUserException;
import sevsu.gradeo.services.UsersService;

import javax.validation.Valid;

@Controller
@RequestMapping("admin")
/*
@PreAuthorize("hasAuthority('ADMIN')")
Эта аннотация будет пускать только ADMIN. Но для начала нужно реализовать эту всю систему
т.к. по умолчанию все TEACHER.
*/

public class AdminPanelController {

    @Autowired
    private UsersService usersService;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("")
    public String index(Model model) {
        model.addAttribute("allUsers", usersService.getAllUsers());
        return "AdminPanel/Index";
    }


    @GetMapping("edit/{id}")
    public String getUpdatePage(@PathVariable("id") Integer id, Model model) throws EntityNotFoundException {
        logger.debug(LogStrings.SEARCH, "User", id);
        var dto = usersService.getUserById(id);
        if (dto.isEmpty()) {
            logger.error(LogStrings.NOT_FOUND, "User", id);
            return "redirect:/admin";
        }
        logger.debug(LogStrings.MODEL_ADD, "User", dto.get().getUsername());
        model.addAttribute("user", dto.get());
        model.addAttribute("teacher_id", dto.get().getTeacherId());
        return "AdminPanel/EditUser";
    }

    @PostMapping("edit")
    public String update(@ModelAttribute("user") @Valid UpdateUserDto user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            logger.warn(LogStrings.BINDING_HAS_ERRORS);
            return "AdminPanel/EditUser";
        }

        try {
            logger.debug(LogStrings.UPDATE, "User", user.getId(), user.getUsername());
            usersService.updateUser(user);
        } catch (NoSuchUserException e) {
            logger.error(LogStrings.NO_SUCH, "User", user.getId());
            bindingResult.rejectValue("username", "error.updateDto", "Невозможно использовать данный никнейм");
            return "AdminPanel/EditUser";
        }

        return "redirect:/admin";
    }

    @PostMapping("delete/{id}")
    public String deleteUser(@PathVariable("id") Integer id, Model model) throws EntityNotFoundException {
        try {
            logger.info(LogStrings.DELETE, "User", id);
            usersService.deleteUserById(id);
        } catch (LastUserException e) {
            logger.error("Couldn't delete last user in system!");
            model.addAttribute("lastUserException", "Вы не можете удалить последнего пользователя в системе!");
            return "AdminPanel/EditUser";
        }
        return "redirect:/admin";
    }
}

