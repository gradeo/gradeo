package sevsu.gradeo.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.dtos.assessments.SetAssessmentDtoByTask;
import sevsu.gradeo.dtos.assessments.SetAssessmentDtoByTaskAndStudent;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchStudentException;
import sevsu.gradeo.exceptions.entities.NoSuchTaskException;
import sevsu.gradeo.services.AssessmentsService;
import sevsu.gradeo.services.SelectListService;
import sevsu.gradeo.services.TasksService;

import javax.validation.Valid;

@Controller
@RequestMapping("assessments")
public class AssessmentController {
    @Autowired
    AssessmentsService assessmentsService;
    @Autowired
    SelectListService selectListService;
    @Autowired
    TasksService tasksService;

    Logger logger = LoggerFactory.getLogger(getClass());

    @GetMapping("set/taskId/{id}")
    public String setAssessmentByTaskPage(
            @ModelAttribute("dto") SetAssessmentDtoByTask dto,
            @PathVariable("id") Integer taskId,
            Model model) {
        logger.debug(LogStrings.MODEL_ADD, "Assessment");
        var task = tasksService.getTaskById(taskId);
        if (task.isEmpty()) {
            logger.error(LogStrings.NOT_FOUND, "Task", taskId);
            return "redirect:/subjects";
        }
        model.addAttribute("subjectId", task.get().getSubjectId());
        model.addAttribute("task", task.get());
        model.addAttribute("groupsOptions", selectListService.getGroupsListBySubjectId(task.get().getSubjectId()));
        return "Assessments/SetForTask";
    }

    @PostMapping("set/taskId")
    public String setAssessmentByTask(@ModelAttribute("dto") @Valid SetAssessmentDtoByTask dto, BindingResult bindingResult, Model model) throws EntityNotFoundException {
        if (bindingResult.hasErrors()) {
            logger.error(LogStrings.BINDING_HAS_ERRORS);
            logger.debug(LogStrings.MODEL_ADD, "Assessment");
            var task = tasksService.getTaskById(dto.getTaskId());
            if (task.isEmpty()) {
                logger.error(LogStrings.NOT_FOUND, "Task", dto.getTaskId());
                return "redirect:/subjects";
            }
            model.addAttribute("subjectId", task.get().getSubjectId());
            model.addAttribute("task", task.get());
            model.addAttribute("groupsOptions", selectListService.getGroupsListBySubjectId(task.get().getSubjectId()));
            return "Assessments/SetForTask";
        }
        try {
            logger.info(LogStrings.CREATE, "Assessment", "from task", dto.getTaskId(), "for student", dto.getStudentId());
            assessmentsService.setAssessment(dto);
        } catch (NoSuchTaskException e) {
            logger.error(LogStrings.NO_SUCH, "Task", dto.getTaskId());
            model.addAttribute("groupsOptions", selectListService.getInstitutesList());
            return "redirect:/subjects/view/" + dto.getSubjectId();
        } catch (NoSuchStudentException e) {
            logger.error(LogStrings.NO_SUCH, "Student", dto.getStudentId());
            logger.error(LogStrings.BINDING_HAS_ERRORS);
            var task = tasksService.getTaskById(dto.getTaskId());
            if (task.isEmpty()) {
                logger.error(LogStrings.NOT_FOUND, "Task", dto.getTaskId());
                return "redirect:/subjects";
            }
            logger.debug(LogStrings.MODEL_ADD, "Assessment", dto);
            model.addAttribute("subjectId", task.get().getSubjectId());
            model.addAttribute("task", task.get());
            model.addAttribute("groupsOptions", selectListService.getGroupsListBySubjectId(dto.getSubjectId()));
            return "Assessments/SetForTask";
        }
        return "redirect:/tasks/view/" + dto.getTaskId();
    }

    @GetMapping("set/taskId/{taskId}/studentId/{studentId}")
    public String setAssessmentByTaskAndStudentPage(
            @ModelAttribute("dto") SetAssessmentDtoByTaskAndStudent dto,
            @PathVariable("taskId") Integer taskId,
            @PathVariable("studentId") Integer studentId,
            Model model) {
        logger.debug(LogStrings.MODEL_ADD, "Assessment");
        var task = tasksService.getTaskById(taskId);
        if (task.isEmpty()) {
            logger.error(LogStrings.NOT_FOUND, "Task", taskId);
            return "redirect:/subjects";
        }
        model.addAttribute("studentId", studentId);
        model.addAttribute("subjectId", task.get().getSubjectId());
        model.addAttribute("task", task.get());
        model.addAttribute("groupsOptions", selectListService.getGroupsListBySubjectId(task.get().getSubjectId()));
        return "Assessments/SetForTaskAndStudent";
    }

    @PostMapping("set/taskId/studentId")
    public String setAssessmentByTaskAndStudent(
            @ModelAttribute("dto") @Valid SetAssessmentDtoByTaskAndStudent dto,
            BindingResult bindingResult,
            Model model) throws EntityNotFoundException {
        if (bindingResult.hasErrors()) {
            logger.error(LogStrings.BINDING_HAS_ERRORS);
            logger.debug(LogStrings.MODEL_ADD, "Assessment");
            var task = tasksService.getTaskById(dto.getTaskId());
            if (task.isEmpty()) {
                logger.error(LogStrings.NOT_FOUND, "Task", dto.getTaskId());
                return "redirect:/subjects";
            }
            model.addAttribute("studentId", dto.getStudentId());
            model.addAttribute("subjectId", task.get().getSubjectId());
            model.addAttribute("task", task.get());
            model.addAttribute("groupsOptions", selectListService.getGroupsListBySubjectId(task.get().getSubjectId()));
            return "Assessments/SetForTaskAndStudent";
        }
        try {
            logger.info(LogStrings.CREATE, "Assessment", "from task", dto.getTaskId(), "for student", dto.getStudentId());
            assessmentsService.setAssessment(dto);
        } catch (NoSuchTaskException e) {
            logger.error(LogStrings.NO_SUCH, "Task", dto.getTaskId());
            model.addAttribute("groupsOptions", selectListService.getInstitutesList());
            return "redirect:/subjects/view/" + dto.getSubjectId();
        } catch (NoSuchStudentException e) {
            logger.error(LogStrings.NO_SUCH, "Student", dto.getStudentId());
            logger.error(LogStrings.BINDING_HAS_ERRORS);
            var task = tasksService.getTaskById(dto.getTaskId());
            if (task.isEmpty()) {
                logger.error(LogStrings.NOT_FOUND, "Task", dto.getTaskId());
                return "redirect:/subjects";
            }
            logger.debug(LogStrings.MODEL_ADD, "Assessment", dto);
            model.addAttribute("studentId", dto.getStudentId());
            model.addAttribute("subjectId", task.get().getSubjectId());
            model.addAttribute("task", task.get());
            model.addAttribute("groupsOptions", selectListService.getGroupsListBySubjectId(dto.getSubjectId()));
            return "Assessments/SetForTaskAndStudent";
        }
        return "redirect:/tasks/view/" + dto.getTaskId();
    }
}