package sevsu.gradeo.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import sevsu.gradeo.dtos.attendance.CreateUpdateAttendanceDto;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchLessonException;
import sevsu.gradeo.exceptions.entities.NoSuchStudentException;
import sevsu.gradeo.services.AttendanceService;
import sevsu.gradeo.services.SelectListService;
import sevsu.gradeo.services.TeachersService;

@Controller
@RequestMapping("attendance")
public class AttendanceController {

    @Autowired
    SelectListService selectListService;
    @Autowired
    AttendanceService attendanceService;
    @Autowired
    TeachersService teachersService;
    Logger logger = LoggerFactory.getLogger(getClass());

    @GetMapping("")
    public String index(Model model) {
        try {
            var teacher = teachersService.getCurrentTeacher();
            model.addAttribute("teacher_subjects", selectListService
                    .getSubjectListByTeacherId(teacher.getId()));
        } catch (EntityNotFoundException e) {
            return "Errors/403";
        }
        return "Attendance/Index";
    }

    @PostMapping("")
    @ResponseBody
    public String setAttendance(
            @RequestBody CreateUpdateAttendanceDto dto) {
        try {
            attendanceService.handleAttendanceDto(dto);
            StringBuilder sb = new StringBuilder();
            sb.append("Отмечены присутствующими следующие студенты:");
            for (String studentName : dto.getStudentsName()) {
                sb.append("\n").append(studentName);
            }
            return sb.toString();
        } catch (NoSuchLessonException e) {
            return "Произошла ошибка при поиске занятия!";
        } catch (NoSuchStudentException e) {
            return "Произошла ошибка при поиске студента!";
        }

    }
}