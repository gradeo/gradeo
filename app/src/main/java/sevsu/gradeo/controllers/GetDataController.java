package sevsu.gradeo.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import sevsu.gradeo.dtos.util.SelectListItemDto;
import sevsu.gradeo.services.AttendanceService;
import sevsu.gradeo.services.SelectListService;

import java.util.List;

@Controller
@RequestMapping("data")
public class GetDataController {

    @Autowired
    SelectListService selectListService;
    @Autowired
    AttendanceService attendanceService;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/institutes/{instituteId}/departments")
    @ResponseBody
    public List<SelectListItemDto> loadDepartments(@PathVariable("instituteId") Integer instituteId) {
        return selectListService.getDepartmentListByInstituteId(instituteId);
    }

    //используется при селекторе групп по ИД кафедры
    @GetMapping("/institutes/{instituteId}/departments/{departmentId}/groups")
    @ResponseBody
    public List<SelectListItemDto> loadGroups(@PathVariable("departmentId") Integer departmentId, @PathVariable("instituteId") Integer instituteId) {
        logger.debug("GET '/institutes/{}/departments/{}/groups'", instituteId, departmentId);
        return selectListService.getGroupsListByDepartmentId(departmentId);
    }

    //используется при селекторе групп по ИД предмета
    @GetMapping("/institutes/{instituteId}/departments/{departmentId}/subjects/{subjectId}/groups")
    @ResponseBody
    public List<SelectListItemDto> loadGroupsBySubjectId(
            @PathVariable("subjectId") Integer subjectId,
            @PathVariable("instituteId") Integer instituteId,
            @PathVariable("departmentId") Integer departmentId) {
        logger.debug("GET '/institutes/{}/departments/{}/subjects/{}/groups'", instituteId, departmentId, subjectId);
        return selectListService.getGroupsListBySubjectId(subjectId);
    }

    @GetMapping("/institutes/{instituteId}/departments/{departmentId}/teachers")
    @ResponseBody
    public List<SelectListItemDto> loadTeachers(@PathVariable("departmentId") Integer departmentId, @PathVariable("instituteId") Integer instituteId) {
        return selectListService.getTeachersListByDepartmentId(departmentId);
    }

    @GetMapping("/institutes/{instituteId}/departments/{departmentId}/subjects/{subjectId}/teachers")
    @ResponseBody
    public List<SelectListItemDto> loadTeachersBySubjectId(@PathVariable("subjectId") Integer subjectId, @PathVariable("departmentId") Integer departmentId, @PathVariable("instituteId") Integer instituteId) {
        return selectListService.getTeachersListBySubjectId(subjectId);
    }

    @GetMapping("/institutes/{instituteId}/departments/{departmentId}/subjects")
    @ResponseBody
    public List<SelectListItemDto> loadSubjects(@PathVariable("departmentId") Integer departmentId, @PathVariable("instituteId") Integer instituteId) {
        logger.debug("GET '/institutes/{}/departments/{}/subjects'", instituteId, departmentId);
        return selectListService.getSubjectListByDepartmentId(departmentId);
    }

    @GetMapping("/institutes/{instituteId}/departments/{departmentId}/groups/{groupId}/subjects")
    @ResponseBody
    public List<SelectListItemDto> loadSubjectsByGroupId(@PathVariable("groupId") Integer groupId, @PathVariable("departmentId") Integer departmentId, @PathVariable("instituteId") Integer instituteId) {
        return selectListService.getSubjectListByGroupId(groupId);
    }

    @GetMapping("/institutes/{instituteId}/departments/{departmentId}/subjects/{subjectId}/groups/{groupId}/lessons")
    @ResponseBody
    public List<SelectListItemDto> loadLessons(
            @PathVariable("departmentId") Integer departmentId,
            @PathVariable("instituteId") Integer instituteId,
            @PathVariable("subjectId") Integer subjectId,
            @PathVariable("groupId") Integer groupId) {
        logger.debug("GET '/institutes/{}/departments/{}/subjects/{}/groups/{}/lessons'", instituteId, departmentId, subjectId, groupId);
        return selectListService.getLessonsListBySubjectIdAndByGroupId(subjectId, groupId);
    }

    @GetMapping("/subjects/{subjectId}/groups")
    @ResponseBody
    public List<SelectListItemDto> loadGroupsBySubjectIdForTasks(
            @PathVariable("subjectId") Integer subjectId) {
        logger.debug("GET '/subjects/{}/groups'", subjectId);
        return selectListService.getGroupsListBySubjectId(subjectId);
    }

    @GetMapping("/groups/{groupId}/students")
    @ResponseBody
    public List<SelectListItemDto> loadStudentsByGroupId(
            @PathVariable("groupId") Integer groupId) {
        logger.debug("GET '/groups/{}/students'", groupId);
        return selectListService.getStudentsListByGroupId(groupId);
    }

    @GetMapping("/institutes/{instituteId}/departments/{departmentId}/subjects/{subjectId}/groups/{groupId}/students")
    @ResponseBody
    public List<SelectListItemDto> loadStudents(
            @PathVariable("departmentId") Integer departmentId,
            @PathVariable("instituteId") Integer instituteId,
            @PathVariable("subjectId") Integer subjectId,
            @PathVariable("groupId") Integer groupId) {
        logger.debug("GET '/institutes/{}/departments/{}/subjects/{}/groups/{}/students'", instituteId, departmentId, subjectId, groupId);
        return selectListService.getStudentsListByGroupId(groupId);
    }

}
