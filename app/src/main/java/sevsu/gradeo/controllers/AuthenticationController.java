package sevsu.gradeo.controllers;


import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.dtos.teachers.CreateTeacherDto;
import sevsu.gradeo.dtos.users.CreateUserDto;
import sevsu.gradeo.dtos.util.ResetPasswordDto;
import sevsu.gradeo.dtos.util.ResetPasswordRequestDto;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.*;
import sevsu.gradeo.services.ResetPasswordService;
import sevsu.gradeo.services.SelectListService;
import sevsu.gradeo.services.TeachersService;
import sevsu.gradeo.services.UsersService;

import javax.validation.Valid;
import java.util.UUID;

@Controller
@RequestMapping("auth")
public class AuthenticationController {

    @Autowired
    private UsersService usersService;
    @Autowired
    private TeachersService teachersService;
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    SelectListService selectListService;
    @Autowired
    ResetPasswordService resetPasswordService;

    @GetMapping("sign-up")
    public String registrationPage(@ModelAttribute("createUserDto") CreateUserDto createUserDto) {
        return "Authenticating/Registration";
    }

    @PostMapping("sign-up")
    public String addUser(@ModelAttribute("createUserDto") @Valid CreateUserDto createUserDto,
                          BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            logger.warn(LogStrings.BINDING_HAS_ERRORS);
            return "Authenticating/Registration";
        }
        try {
            logger.info(LogStrings.CREATE, "User", createUserDto.getUsername());
            usersService.createUser(createUserDto);
        } catch (UserAlreadyExistsException e) {
            logger.error("User with nickname '{}' already exists!", createUserDto.getUsername());
            bindingResult.rejectValue("username", "error.createUserDto", "Данный никнейм уже занят!");
            return "Authenticating/Registration";
        } catch (EmailAlreadyExistsException e) {
            logger.error("User with email '{}' already exists!", createUserDto.getEmail());
            bindingResult.rejectValue("email", "error.createUserDto", "Данная почта уже используется!");
            return "Authenticating/Registration";
        } catch (PasswordsNotEqualException e) {
            logger.error("Password and confirmPassword are not equal!");
            bindingResult.rejectValue("confirmPassword", "error.createUserDto", "Пароли не совпадают!");
            return "Authenticating/Registration";
        }
        return "redirect:/admin";
    }


    @GetMapping("sign-up/teacher")
    public String registrationPageTeacher(@ModelAttribute("createDto") CreateTeacherDto createDto,
                                          Model model) {
        model.addAttribute("instituteOptions", selectListService.getInstitutesList());
        model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(createDto.getInstituteId()));
        return "AdminPanel/CreateTeacher";

    }

    @PostMapping("sign-up/teacher")
    public String addTeacher(@ModelAttribute("createDto") @Valid CreateTeacherDto createDto,
                             BindingResult bindingResult,
                             Model model) {
        if (bindingResult.hasErrors()) {
            logger.warn(LogStrings.BINDING_HAS_ERRORS);
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(createDto.getInstituteId()));
            return "AdminPanel/CreateTeacher";
        }
        try {
            logger.info(LogStrings.CREATE, "Teacher", createDto.getUsername());
            teachersService.createTeacher(createDto);
        } catch (UserAlreadyExistsException e) {
            logger.error("User with nickname '{}' already exists!", createDto.getUsername());
            bindingResult.rejectValue("username", "error.createUserDto", "Данный никнейм уже занят!");
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(createDto.getInstituteId()));
            return "AdminPanel/CreateTeacher";
        } catch (EmailAlreadyExistsException e) {
            logger.error("User with email '{}' already exists!", createDto.getEmail());
            bindingResult.rejectValue("email", "error.createDto", "Данная почта уже используется!");
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(createDto.getInstituteId()));
            return "AdminPanel/CreateTeacher";
        } catch (NoSuchDepartmentException e) {
            logger.error("No such department with id: {}", createDto.getDepartmentId());
            bindingResult.rejectValue("departmentId", "error.createDto", "Невозможно использовать данную кафедру");
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(createDto.getInstituteId()));
            return "AdminPanel/CreateTeacher";
        } catch (EntityNotFoundException e) {
            logger.error("Something went wrong while binding user_id to teachers entity! Check service.");
            bindingResult.rejectValue("username", "error.createDto", "Something went wrong while binding user_id to teachers entity! Check service.");
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(createDto.getInstituteId()));
            return "AdminPanel/CreateTeacher";
        }

        return "redirect:/admin";
    }


    // Обрабатыаем GET запрос на получение HTML страницы с восстановлением пароля
    @GetMapping("reset-request")
    public String passwordResetRequestPage(@ModelAttribute("resetPasswordRequestDto") ResetPasswordRequestDto resetPasswordRequestDto) {
        return "Authenticating/PasswordResetRequest";
    }

    // Обрабатываем POST запрос серверу на сброс пароля
    @PostMapping("reset-request")
    public String passwordRequestReset(@ModelAttribute("resetPasswordRequestDto") @Valid ResetPasswordRequestDto resetPasswordRequestDto,
                                       BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            logger.warn(LogStrings.BINDING_HAS_ERRORS);
            return "Authenticating/PasswordResetRequest";
        }

        UUID token = UUID.randomUUID();
        try {
            resetPasswordService.createToken(usersService.getUserByEmail(resetPasswordRequestDto.getEmail()), token);
        } catch (EntityNotFoundException e) {
            bindingResult.rejectValue("email", "error.resetPasswordRequestDto",
                    "Пользователь не найден");
            return "Authenticating/PasswordResetRequest";
        }
        resetPasswordService.sendMail(resetPasswordRequestDto.getEmail(), token);
        return "Authenticating/PasswordResetRequestSuccess";
    }

    @SneakyThrows
    @GetMapping("reset")
    public String passwordResetPage(@RequestParam(name = "code") String code,
                                    @ModelAttribute("resetPasswordDto") ResetPasswordDto resetPasswordDto,
                                    Model model) {

        // Проверка существования токена
        try {
            resetPasswordService.getToken(UUID.fromString(code));
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", "Запрос на восстановление не найден");
            return "Authenticating/TokenErrors"; }
        catch (TokenAlreadyUsedException e) {
            model.addAttribute("error", "Запрос на восстановление уже использован");
            return "Authenticating/TokenErrors"; }
        catch (TokenOverdueException e) {
            model.addAttribute("error", "Запрос на восстановление просрочен");
            return "Authenticating/TokenErrors";
        }
        return "Authenticating/PasswordReset";
    }

    @SneakyThrows
    @PostMapping("reset")
    public String passwordReset(@ModelAttribute("resetPasswordDto") @Valid ResetPasswordDto resetPasswordDto,
                                BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            logger.warn(LogStrings.BINDING_HAS_ERRORS);
            return "Authenticating/PasswordReset";
        }
        // Проверка совпадения паролей
        if (!resetPasswordDto.getPassword().equals(resetPasswordDto.getConfirmPassword())) {
            bindingResult.rejectValue("password", "error.resetPasswordDto",
                    "Пароли не совпадают");
            return "Authenticating/PasswordReset";
        }

        resetPasswordService.resetPassword(resetPasswordDto);

        return "redirect:/auth/sign-in";
    }
}
