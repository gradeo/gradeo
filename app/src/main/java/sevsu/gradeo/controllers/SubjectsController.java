package sevsu.gradeo.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sevsu.gradeo.config.LogStrings;
import sevsu.gradeo.dtos.subjects.CreateSubjectDto;
import sevsu.gradeo.dtos.subjects.UpdateSubjectDto;
import sevsu.gradeo.exceptions.EntityNotFoundException;
import sevsu.gradeo.exceptions.entities.NoSuchDepartmentException;
import sevsu.gradeo.services.SelectListService;
import sevsu.gradeo.services.SubjectsService;
import sevsu.gradeo.services.TasksService;

import javax.validation.Valid;

@Controller
@RequestMapping("subjects")
public class SubjectsController {
    @Autowired
    SubjectsService subjectsService;
    @Autowired
    SelectListService selectListService;
    @Autowired
    TasksService tasksService;

    Logger logger = LoggerFactory.getLogger(getClass());

    @GetMapping("")
    public String index(
            Model model,
            Pageable pageable,
            @RequestParam(defaultValue = "") String name,
            @RequestParam(defaultValue = "off") boolean isDeleted,
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "sort", defaultValue = "id") String sort,
            @RequestParam(value = "size", defaultValue = "20") Integer size) {
        var subjectPageDto = subjectsService.getAllSubjects(
                pageable,
                name,
                isDeleted);
        model.addAttribute("model", subjectPageDto);
        model.addAttribute("size", size);
        model.addAttribute("sort", sort);
        model.addAttribute("name", name);
        model.addAttribute("isDeleted", isDeleted);
        return "Subjects/Index";
    }

    @GetMapping("view/{id}")
    public String view(@PathVariable("id") Integer id, Model model) {
        var subjectDto = subjectsService.getSubjectById(id);
        if (subjectDto.isEmpty()) {
            logger.error(LogStrings.NOT_FOUND, "Subject", id);
            return "redirect:/subjects";
        }
        model.addAttribute("subject", subjectDto.get());

        var tasksDtoList = tasksService.getAllTasksForSubject(id);
        if (tasksDtoList.isEmpty()) {
            logger.error(LogStrings.NOT_FOUND, "task", id);
            model.addAttribute("tasks", tasksDtoList);
            return "Subjects/View";
        }
        model.addAttribute("tasks", tasksDtoList);
        return "Subjects/View";
    }

    @GetMapping("new")
    public String getCreatePage(@ModelAttribute("createDto") CreateSubjectDto createDto, Model model) {
        model.addAttribute("instituteOptions", selectListService.getInstitutesList());
        model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(createDto.getInstituteId()));
        model.addAttribute("teachersInDepartment", selectListService.getTeachersListByDepartmentId(createDto.getDepartmentId()));
        return "Subjects/Create";
    }

    @PostMapping("new")
    public String createSubject(@ModelAttribute("createDto") @Valid CreateSubjectDto createDto, BindingResult bindingResult, Model model) throws EntityNotFoundException {
        if (bindingResult.hasErrors()) {
            logger.error(LogStrings.BINDING_HAS_ERRORS);
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(createDto.getInstituteId()));
            model.addAttribute("teachersInDepartment", selectListService.getTeachersListByDepartmentId(createDto.getDepartmentId()));
            return "Subjects/Create";
        }
        try {
            logger.info(LogStrings.CREATE, "Subject", createDto.getName());
            subjectsService.createSubject(createDto);
        } catch (NoSuchDepartmentException e) {
            logger.error(LogStrings.NO_SUCH, "Department", createDto.getDepartmentId());
            bindingResult.rejectValue("departmentId", "error.createDto", "Невозможно использовать данную кафедру");
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(createDto.getInstituteId()));
            model.addAttribute("teachersInDepartment", selectListService.getTeachersListByDepartmentId(createDto.getDepartmentId()));
            return "Subjects/Create";
        }
        return "redirect:/subjects";
    }

    @GetMapping("edit/{id}")
    public String getUpdatePage(@PathVariable("id") Integer id, Model model) {
        logger.info(LogStrings.SEARCH, "Subject", String.format("id = %d", id));
        var dto = subjectsService.getSubjectById(id);
        if (dto.isEmpty()) {
            logger.error(LogStrings.NOT_FOUND, "Subject", id);
            return "redirect:/subjects";
        }
        logger.debug(LogStrings.MODEL_ADD, "Subject", dto.get());
        model.addAttribute("updateDto", dto.get());
        model.addAttribute("instituteOptions", selectListService.getInstitutesList());
        model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(dto.get().getInstituteId()));
        model.addAttribute("teachersInDepartment", selectListService.getTeachersListByDepartmentId(dto.get().getDepartmentId()));
        return "Subjects/Edit";
    }

    @PostMapping("edit")
    public String updateSubject(@ModelAttribute("updateDto") @Valid UpdateSubjectDto updateDto, BindingResult bindingResult, Model model) throws EntityNotFoundException {
        logger.debug(LogStrings.POST_REQUEST, "/edit");
        if (bindingResult.hasErrors()) {
            logger.warn(LogStrings.BINDING_HAS_ERRORS);
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(updateDto.getInstituteId()));
            model.addAttribute("teachersInDepartment", selectListService.getTeachersListByDepartmentId(updateDto.getDepartmentId()));
            return "Subjects/Edit";
        }
        try {
            logger.debug(LogStrings.UPDATE, "Student", updateDto.getId(), updateDto.getName());
            subjectsService.updateSubject(updateDto);
        } catch (NoSuchDepartmentException e) {
            logger.error(LogStrings.NO_SUCH, "Department", updateDto.getDepartmentId());
            bindingResult.rejectValue("groupId", "error.updateDto", "Невозможно использовать данную кафедру");
            model.addAttribute("instituteOptions", selectListService.getInstitutesList());
            model.addAttribute("departmentsInInstitute", selectListService.getDepartmentListByInstituteId(updateDto.getInstituteId()));
            model.addAttribute("teachersInDepartment", selectListService.getTeachersListByDepartmentId(updateDto.getDepartmentId()));
            return "Subjects/Edit";
        }
        return "redirect:/subjects";
    }

    @PostMapping("delete/{id}")
    public String deleteSubject(@PathVariable("id") Integer id) throws EntityNotFoundException {
        logger.info(LogStrings.DELETE, "Subject", id);
        subjectsService.deleteSubjectById(id);
        return "redirect:/subjects";
    }
}

