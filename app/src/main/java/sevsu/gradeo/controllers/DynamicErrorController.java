package sevsu.gradeo.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import sevsu.gradeo.config.LogStrings;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("error")
public class DynamicErrorController implements ErrorController {

    @Override
    public String getErrorPath() {
        return "Errors/Error";
    }

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("")
    public String handleError(HttpServletRequest request, Model model) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);


        if (status != null) {
            int statusCode = Integer.parseInt(status.toString());
            logger.error(LogStrings.HANDLING_ERROR, statusCode);

            if (statusCode == HttpStatus.BAD_REQUEST.value()) {
                return "Errors/400";
            } else if (statusCode == HttpStatus.UNAUTHORIZED.value()) {
                return "Errors/401";
            } else if (statusCode == HttpStatus.FORBIDDEN.value()) {
                return "Errors/403";
            } else if (statusCode == HttpStatus.NOT_FOUND.value()) {
                return "Errors/404";
            } else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                return "Errors/500";
            } else if (statusCode == HttpStatus.BAD_GATEWAY.value()) {
                return "Errors/502";
            } else if (statusCode == HttpStatus.SERVICE_UNAVAILABLE.value()) {
                return "Errors/503";
            } else if (statusCode == HttpStatus.GATEWAY_TIMEOUT.value()) {
                return "Errors/504";
            } else {
                model.addAttribute("error_status_code", statusCode);
            }
        }
        return "Errors/Error";
    }
}
