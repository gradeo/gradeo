package sevsu.gradeo.servletFilters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Component
@Order(2)
public class RequestResponseLoggingFilter implements Filter {

    private Logger logger = LoggerFactory.getLogger(getClass());

    //так как метод getParameterMap() объектов типа HttpServletResponse возвращает Map<String, String[]>
    //делаем внетренний класс со статическим методом который позволяет вывести содержимое Map необходимым образом
    private static class ParametersToStringConverter {
        static String convert (Map<String, String[]> parametersMap){
            StringBuilder out = new StringBuilder();
            out.append("parameters");
            for (Map.Entry<String, String[]> parameterEntry : parametersMap.entrySet()) {
                out.append(" [").append(parameterEntry.getKey()).append(":");
                for(String value : parameterEntry.getValue()){
                    out.append(" ").append(value);
                }
                out.append("]");
            }
            return out.toString();
        }
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        //перехваченный запрос
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        //перехваченный ответ
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        //Map параметров перехваченного ответа
        var parametersMap = httpServletRequest.getParameterMap();

        if (!httpServletRequest.getRequestURI().matches("/?(css|img|image).*")) {
            //логирование запроса
            if (parametersMap.isEmpty()) {
                //если параметры не возвращаются
                logger.debug(
                        "Logging Request :{} : {}",
                        httpServletRequest.getMethod(),
                        httpServletRequest.getRequestURI());
            } else {
                //если возвращаются
                logger.debug(
                        "Logging Request :{} : {} : {}",
                        httpServletRequest.getMethod(),
                        httpServletRequest.getRequestURI(),
                        ParametersToStringConverter.convert(parametersMap));
            }
        }
        //приказ сервлету обработать request и выдать результат в response
        chain.doFilter(request, response);

        if (!httpServletRequest.getRequestURI().matches("/?(css|img|image).*")) {
            //логирование ответа
            logger.debug(
                    "Logging Response :{}",
                    httpServletResponse.getContentType());
        }
    }
}