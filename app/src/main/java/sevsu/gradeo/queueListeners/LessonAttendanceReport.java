package sevsu.gradeo.queueListeners;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import sevsu.gradeo.dtos.lessons.LessonReportDto;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

@EnableRabbit
@Component
public class LessonAttendanceReport {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @RabbitListener(queues = "generateLessonAttendanceReport")
    public byte[] worker(LessonReportDto reportDto) throws IOException {
        logger.info("Worker accepted the task - generating xls report with data: " + reportDto.toString());
        return generateReport(reportDto);
    }


    private byte[] generateReport(LessonReportDto reportDto) throws IOException {
        logger.info("Start generating a xls report");

        try (HSSFWorkbook wb = new HSSFWorkbook()) {
            HSSFFont font = wb.createFont();
            font.setFontHeightInPoints((short) 10);
            font.setFontName("Arial");

            HSSFSheet sheet = wb.createSheet("Посещаемость");
            HSSFCellStyle headStyle = wb.createCellStyle();
            headStyle.setAlignment(HorizontalAlignment.CENTER);
            headStyle.setFont(font);

            HSSFRow row = sheet.createRow(0);

            HSSFCell teacherName = row.createCell(0);
            teacherName.setCellValue("Ф.И.О. преподавателя");
            teacherName.setCellStyle(headStyle);

            HSSFCell subjectName = row.createCell(1);
            subjectName.setCellValue("Предмет");
            subjectName.setCellStyle(headStyle);

            HSSFCell date = row.createCell(2);
            date.setCellValue("Дата проведения");
            date.setCellStyle(headStyle);

            HSSFCell presenceStudentsCol = row.createCell(3);
            presenceStudentsCol.setCellValue("Присутствующие студенты");
            presenceStudentsCol.setCellStyle(headStyle);

            HSSFCell absenceStudentsCol = row.createCell(4);
            absenceStudentsCol.setCellValue("Отсутствующие студенты");
            absenceStudentsCol.setCellStyle(headStyle);

            List<String> presenceStudents = Arrays.asList(reportDto.getPresenceStudents());
            Iterator<String> presenceIterator = presenceStudents.iterator();
            List<String> absenceStudents = Arrays.asList(reportDto.getAbsenceStudents());
            Iterator<String> absenceIterator = absenceStudents.iterator();

            row = sheet.createRow(1);

            teacherName = row.createCell(0);
            teacherName.setCellValue(reportDto.getTeacherName());
            teacherName.setCellStyle(headStyle);

            subjectName = row.createCell(1);
            subjectName.setCellValue(reportDto.getSubjectName());
            subjectName.setCellStyle(headStyle);

            date = row.createCell(2);
            date.setCellValue(reportDto.getDate());
            date.setCellStyle(headStyle);

            while (absenceIterator.hasNext() || presenceIterator.hasNext()) {

                if (presenceIterator.hasNext()) {
                    HSSFCell studentCell = row.createCell(3);
                    String studentName = presenceIterator.next();
                    studentCell.setCellValue(studentName);
                    studentCell.setCellStyle(headStyle);
                }
                if (absenceIterator.hasNext()) {
                    HSSFCell studentCell = row.createCell(4);
                    String studentName = absenceIterator.next();
                    studentCell.setCellValue(studentName);
                    studentCell.setCellStyle(headStyle);
                }

                row = sheet.createRow(wb
                        .getSheet("Посещаемость")
                        .getLastRowNum() + 1);
            }
            logger.info("autosize columns");
            sheet.autoSizeColumn(0);
            sheet.autoSizeColumn(1);
            sheet.autoSizeColumn(2);
            sheet.autoSizeColumn(3);
            sheet.autoSizeColumn(4);
            ByteArrayOutputStream workbook = new ByteArrayOutputStream();
            wb.write(workbook);
            workbook.close();
            return workbook.toByteArray();
        }
    }
}
