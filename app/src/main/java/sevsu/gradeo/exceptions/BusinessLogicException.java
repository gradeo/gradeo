package sevsu.gradeo.exceptions;

public class BusinessLogicException extends RuntimeException {

    public BusinessLogicException() {
        super();
    }

    public BusinessLogicException(String message) {
        super(message);
    }
}
