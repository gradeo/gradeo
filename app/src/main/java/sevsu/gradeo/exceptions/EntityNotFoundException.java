package sevsu.gradeo.exceptions;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Getter
@Setter
public class EntityNotFoundException extends BusinessLogicException {

    public EntityNotFoundException() {
        super();
    }

    public String getError() {
        return "Entity is not found";
    }

    public EntityNotFoundException(String message) {
        super(message);
    }
}
