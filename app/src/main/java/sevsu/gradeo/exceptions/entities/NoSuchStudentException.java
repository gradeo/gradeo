package sevsu.gradeo.exceptions.entities;

import sevsu.gradeo.exceptions.EntityNotFoundException;

public class NoSuchStudentException extends EntityNotFoundException {
    @Override
    public String getError() {
        return "Student is not found";
    }
}
