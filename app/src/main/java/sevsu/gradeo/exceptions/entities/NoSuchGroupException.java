package sevsu.gradeo.exceptions.entities;

import sevsu.gradeo.exceptions.EntityNotFoundException;

public class NoSuchGroupException extends EntityNotFoundException {
    @Override
    public String getError() {
        return "Group is not found";
    }
}
