package sevsu.gradeo.exceptions.entities;

import sevsu.gradeo.exceptions.EntityNotFoundException;

public class NoSuchSubjectException extends EntityNotFoundException {
    @Override
    public String getError() {
        return "Subject is not found";
    }
}
