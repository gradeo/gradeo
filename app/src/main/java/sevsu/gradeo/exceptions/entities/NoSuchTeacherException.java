package sevsu.gradeo.exceptions.entities;

import sevsu.gradeo.exceptions.EntityNotFoundException;

public class NoSuchTeacherException extends EntityNotFoundException {
    @Override
    public String getError() {
        return "Teacher is not found";
    }
}
