package sevsu.gradeo.exceptions.entities;

import sevsu.gradeo.exceptions.EntityNotFoundException;

public class NoSuchUserException extends EntityNotFoundException {
    @Override
    public String getError() {
        return "User is not found";
    }
}
