package sevsu.gradeo.exceptions.entities;

import sevsu.gradeo.exceptions.EntityNotFoundException;

public class NoSuchDepartmentException extends EntityNotFoundException {
    @Override
    public String getError() {
        return "Department is not found";
    }
}
