package sevsu.gradeo.exceptions.entities;

import sevsu.gradeo.exceptions.BusinessLogicException;

public class TokenAlreadyUsedException extends BusinessLogicException {
}
