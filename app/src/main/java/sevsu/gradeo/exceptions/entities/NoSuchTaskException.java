package sevsu.gradeo.exceptions.entities;

import sevsu.gradeo.exceptions.EntityNotFoundException;

public class NoSuchTaskException extends EntityNotFoundException {
    @Override
    public String getError() {
        return "Task is not found";
    }
}
