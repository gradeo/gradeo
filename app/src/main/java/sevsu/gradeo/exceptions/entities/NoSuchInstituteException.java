package sevsu.gradeo.exceptions.entities;

import sevsu.gradeo.exceptions.EntityNotFoundException;

public class NoSuchInstituteException extends EntityNotFoundException {
    @Override
    public String getError() {
        return "Institute is not found";
    }
}
