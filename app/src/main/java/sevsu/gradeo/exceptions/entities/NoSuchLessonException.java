package sevsu.gradeo.exceptions.entities;

import sevsu.gradeo.exceptions.EntityNotFoundException;

public class NoSuchLessonException extends EntityNotFoundException {
    @Override
    public String getError() {
        return "Lesson is not found";
    }
}
