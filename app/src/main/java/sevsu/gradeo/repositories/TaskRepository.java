package sevsu.gradeo.repositories;

import org.springframework.stereotype.Repository;
import sevsu.gradeo.entities.Task;

import java.util.List;

@Repository
public interface TaskRepository extends RepositoryBase<Task, Integer> {
    List<Task> findAllBySubjectIdAndRemovedDateIsNull(Integer id);
}
