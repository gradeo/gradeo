package sevsu.gradeo.repositories;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.stereotype.Repository;
import sevsu.gradeo.dtos.groups.UpdateGroupDto;
import sevsu.gradeo.entities.Group;

import java.util.List;
import java.util.Optional;

@Repository
public interface GroupsRepository extends RepositoryBase<Group, Integer> {
    @Override
    @EntityGraph(attributePaths = "department")
    Optional<Group> findById(Integer integer);

    Iterable<Group> findAllByDepartmentIdAndRemovedDateIsNull(Integer integer);

    List<Group> findAllBySubjectsIdAndRemovedDateIsNull(Integer id);

    Iterable<Group> findAllByNameAndRemovedDateIsNull(String name);

    Optional<Group> findByIdAndRemovedDateIsNull(int id);
}
