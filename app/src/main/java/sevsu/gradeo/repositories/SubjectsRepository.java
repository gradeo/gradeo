package sevsu.gradeo.repositories;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.stereotype.Repository;
import sevsu.gradeo.entities.Subject;

import java.util.List;
import java.util.Optional;

@Repository
public interface SubjectsRepository extends RepositoryBase<Subject, Integer> {
    @Override
    @EntityGraph(attributePaths = "department")
    Optional<Subject> findById(Integer integer);

    Optional<Subject> findByIdAndRemovedDateIsNull(Integer integer);

    Iterable<Subject> findAllByDepartmentIdAndRemovedDateIsNull(Integer integer);

    Iterable<Subject> findAllByTeachersIdAndRemovedDateIsNull(Integer integer);

    List<Subject> findAllByGroupsIdAndRemovedDateIsNull(Integer id);

    List<Subject> findAllByNameAndRemovedDateIsNull(String name);
}
