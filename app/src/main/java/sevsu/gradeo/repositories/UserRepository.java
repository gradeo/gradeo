package sevsu.gradeo.repositories;

import sevsu.gradeo.entities.User;

import java.util.Optional;


public interface UserRepository extends RepositoryBase<User, Integer> {
    User findByUsernameAndRemovedDateIsNull(String username);

    User findByEmailAndRemovedDateIsNull(String email);

    User findByUsername(String username);
    Optional<User> findById(Integer integer);
}
