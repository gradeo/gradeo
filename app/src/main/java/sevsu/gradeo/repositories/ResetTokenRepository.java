package sevsu.gradeo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sevsu.gradeo.entities.ResetToken;

import java.util.UUID;

@Repository
public interface ResetTokenRepository extends JpaRepository<ResetToken, Integer> {
    ResetToken findByToken(UUID token);
}
