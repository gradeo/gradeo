package sevsu.gradeo.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import sevsu.gradeo.entities.Attendance;

import java.util.List;
import java.util.Optional;

/**
 * Репозиторий посещаемости.
 */
@Repository
public interface AttendanceRepository
        extends RepositoryBase<Attendance, Integer>,
        JpaSpecificationExecutor<Attendance> {

    Optional<Attendance> findById(Integer integer);

    List<Attendance> findAllByLessonId(Integer integer);

    Optional<Attendance> findByStudentId(Integer integer);

}

