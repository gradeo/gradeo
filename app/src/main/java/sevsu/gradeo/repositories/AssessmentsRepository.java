package sevsu.gradeo.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import sevsu.gradeo.entities.Assessment;

import java.util.List;
import java.util.Optional;

public interface AssessmentsRepository
        extends RepositoryBase<Assessment, Integer>,
        JpaSpecificationExecutor<Assessment> {
    List<Assessment> findAllByTaskIdAndRemovedDateIsNull(Integer id);

    Optional<Assessment> findByTaskIdAndStudentIdAndRemovedDateIsNull(Integer task_id, Integer student_id);
}
