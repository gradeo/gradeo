package sevsu.gradeo.repositories;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.stereotype.Repository;
import sevsu.gradeo.entities.Lesson;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Репозиторий занятий.
 */
@Repository
public interface LessonsRepository extends RepositoryBase<Lesson, Integer> {
    @Override
    @EntityGraph(attributePaths = "group")
    Optional<Lesson> findById(Integer integer); //TODO: добавить removed date is null

    List<Lesson> findAllBySubjectIdAndGroupIdAndRemovedDateIsNullAndDateBetween(
            Integer subjectId,
            Integer groupId,
            LocalDateTime startDate,
            LocalDateTime endDate);

    List<Lesson> findAllByTeacherIdAndRemovedDateIsNullAndDateBetween(
            Integer teacherId,
            LocalDateTime startDate,
            LocalDateTime endDate);

    List<Lesson> findAllByGroup_IdAndDateAndRemovedDateIsNull(Integer group_id, LocalDateTime date);
}