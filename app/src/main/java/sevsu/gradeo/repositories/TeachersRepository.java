package sevsu.gradeo.repositories;

import org.springframework.data.jpa.repository.EntityGraph;
import sevsu.gradeo.entities.Group;
import sevsu.gradeo.entities.Teacher;

import java.util.Optional;

public interface TeachersRepository extends RepositoryBase<Teacher, Integer> {
    @Override
    @EntityGraph(attributePaths = "department")
    Optional<Teacher> findById(Integer integer);

    Optional<Teacher> findByIdAndRemovedDateIsNull(Integer integer);

    Iterable<Teacher> findAllByDepartmentIdAndRemovedDateIsNull(Integer id);

    Iterable<Teacher> findAllByNameAndRemovedDateIsNull(String name);

    Iterable<Teacher> findAllBySubjectsIdAndRemovedDateIsNull(Integer id);

    Iterable<Teacher> findAllByDepartmentIdAndGroupIdIsNullAndRemovedDateIsNull(Integer departmentId);

    Optional<Teacher> findByUserId(Integer id);
}
