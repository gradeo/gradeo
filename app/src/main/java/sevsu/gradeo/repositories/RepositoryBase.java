package sevsu.gradeo.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

@NoRepositoryBean
public interface RepositoryBase<TType, TKey extends Serializable> extends CrudRepository<TType, TKey>, JpaSpecificationExecutor<TType> {
    Iterable<TType> findAllByRemovedDateIsNullOrderByIdAsc();

    Page<TType> findAllByRemovedDateIsNull(Pageable pageable);

    Page<TType> findAllByRemovedDateIsNotNull(Pageable pageable);

    Page<TType> findAll(Specification<TType> specifications, Pageable pageable);

}
