package sevsu.gradeo.repositories;

import lombok.NonNull;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import sevsu.gradeo.entities.Department;

import java.util.Optional;

/**
 * Репозиторий кафедр.
 */
@Repository
public interface DepartmentsRepository
        extends RepositoryBase<Department, Integer>,
        JpaSpecificationExecutor<Department> {
    @Override
    @EntityGraph(attributePaths = "institute")
    Optional<Department> findById(Integer integer);

    Optional<Department> findByIdAndRemovedDateIsNull(int id);

    Iterable<Department> findAllByInstituteIdAndRemovedDateIsNull(Integer integer);

    Iterable<Department> findAllByNameAndRemovedDateIsNull(@NonNull String name);
}

