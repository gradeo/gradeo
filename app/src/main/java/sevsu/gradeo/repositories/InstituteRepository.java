package sevsu.gradeo.repositories;

import lombok.NonNull;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import sevsu.gradeo.entities.Institute;

import java.util.Optional;

/**
 * Репозиторий институтов.
 */
@Repository
public interface InstituteRepository extends RepositoryBase<Institute, Integer>, JpaSpecificationExecutor<Institute> {
    @Override
    @EntityGraph(attributePaths = "departments")
    Iterable<Institute> findAllByRemovedDateIsNullOrderByIdAsc();

    @EntityGraph(attributePaths = "departments")
    Iterable<Institute> findAllByNameAndRemovedDateIsNull(@NonNull String name);

    @EntityGraph(attributePaths = "departments")
    Iterable<Institute> findAllByRemovedDateIsNotNullOrderByIdAsc();

    Optional<Institute> findByIdAndRemovedDateIsNull(@NonNull Integer id);
}
