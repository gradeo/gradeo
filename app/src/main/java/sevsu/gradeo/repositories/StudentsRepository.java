package sevsu.gradeo.repositories;

import lombok.NonNull;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.stereotype.Repository;
import sevsu.gradeo.entities.Student;

import java.util.List;
import java.util.Optional;

/**
 * Репозиторий студентов.
 */
@Repository
public interface StudentsRepository extends RepositoryBase<Student, Integer> {
    @Override
    @EntityGraph(attributePaths = "group")
    Optional<Student> findById(Integer integer);

    List<Student> findAllByGroupIdAndRemovedDateIsNull(Integer id);

    List<Student> findAllByGroupIdAndRemovedDateIsNullOrderByNameAsc(Integer id);

    List<Student> findAllByNameAndGradeBookAndRemovedDateIsNull(@NonNull String name, Integer gradeBook);
}

