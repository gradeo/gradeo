function reportDownloader(id) {
    var xhr = $.ajax({
        type: 'GET',
        url: "/lessons/report/attendance/" + id,
        statusCode: {
            500: function () {
                alert("Ошибка генерации отчета - Занятие не существует или было удалено");
            },
            600: function () {
                alert("Ошибка генерации отчета - Занятие еще не проведено!");
            },
            601: function () {
                alert("Ошибка генерации отчета - Внутренняя ошибка!");
            },
            602: function () {
                alert("Ошибка генерации отчета - Ошибка ввода-вывода файла!");
            },

        },
        xhrFields: {
            responseType: 'blob'
        },

        success: function (data) {
            var a = document.createElement('a');
            var url = window.URL.createObjectURL(data);
            a.href = url;
            a.download = decodeURIComponent(xhr.getResponseHeader("Content-Disposition"));
            document.body.append(a);
            a.click();
            a.remove();
            window.URL.revokeObjectURL(url);
        }
    });
}