function departments() {
    var instituteId = isNaN(parseInt($(this).val())) ? -1 : (parseInt($(this).val()));
    var selector = $('#departmentId'), option = "";
    var textMessage = "Выберите кафедру";
    var url = "/data/institutes/" + instituteId + "/departments";

    if (instituteId === -1) {
        generateDefaultOption(selector, option, textMessage);
    } else {
        generateAjaxRequest(url, selector, textMessage);
    }
}

function teachers() {
    var instituteId = isNaN(parseInt($("#instituteId").val())) ? -1 : (parseInt($("#instituteId").val()));
    var departmentId = isNaN(parseInt($("#departmentId").val())) ? -1 : (parseInt($("#departmentId").val()));
    var selector = $('#teacherId'), option = "";
    var textMessage = "Выберите преподавателя";
    var url = "/data/institutes/" + instituteId + "/departments/" + departmentId + "/teachers";

    if (departmentId == -1 || instituteId == -1) {
        generateDefaultOption(selector, option, textMessage);
    } else {
        generateAjaxRequest(url, selector, textMessage);
    }
}

function teachersBySubjectId() {
    var instituteId = isNaN(parseInt($("#instituteId").val())) ? -1 : (parseInt($("#instituteId").val()));
    var departmentId = isNaN(parseInt($("#departmentId").val())) ? -1 : (parseInt($("#departmentId").val()));
    var subjectId = isNaN(parseInt($("#subjectId").val())) ? -1 : (parseInt($("#subjectId").val()));
    var selector = $('#teacherId'), option = "";
    var textMessage = "Выберите преподавателя";
    var url = "/data/institutes/" + instituteId + "/departments/" + departmentId + "/subjects/" + subjectId + "/teachers";

    if (departmentId == -1 || instituteId == -1 || subjectId == -1) {
        generateDefaultOption(selector, option, textMessage);
    } else {
        generateAjaxRequest(url, selector, textMessage);
    }
}

//используется при селекторе групп по ИД кафедры
function groups() {
    var instituteId = isNaN(parseInt($("#instituteId").val())) ? -1 : (parseInt($("#instituteId").val()));
    var departmentId = isNaN(parseInt($("#departmentId").val())) ? -1 : (parseInt($("#departmentId").val()));
    var selector = $('#groupId'), option = "";
    var textMessage = "Выберите группу";
    var url = "/data/institutes/" + instituteId + "/departments/" + departmentId + "/groups";

    if (departmentId == -1 || instituteId == -1) {
        generateDefaultOption(selector, option, textMessage);
    } else {
        generateAjaxRequest(url, selector, textMessage);
    }
}

//используется при селекторе групп по ИД предмета
function groupsBySubjectId() {
    var instituteId = isNaN(parseInt($("#instituteId").val())) ? -1 : (parseInt($("#instituteId").val()));
    var departmentId = isNaN(parseInt($("#departmentId").val())) ? -1 : (parseInt($("#departmentId").val()));
    var subjectId = isNaN(parseInt($("#subjectId").val())) ? -1 : (parseInt($("#subjectId").val()));
    var selector = $('#groupId'), option = "";
    var textMessage = "Выберите группу";
    var url = "/data/institutes/" + instituteId + "/departments/" + departmentId + "/subjects/" + subjectId + "/groups";

    if (subjectId == -1) {
        generateDefaultOption(selector, option, textMessage);
    } else {
        generateAjaxRequest(url, selector, textMessage);
    }
}

function subjects() {
    var instituteId = isNaN(parseInt($("#instituteId").val())) ? -1 : (parseInt($("#instituteId").val()));
    var departmentId = isNaN(parseInt($("#departmentId").val())) ? -1 : (parseInt($("#departmentId").val()));
    var selector = $('#subjectId'), option = "";
    var textMessage = "Выберите предмет";
    var url = "/data/institutes/" + instituteId + "/departments/" + departmentId + "/subjects";

    if (departmentId == -1 || instituteId == -1) {
        generateDefaultOption(selector, option, textMessage);
    } else {
        generateAjaxRequest(url, selector, textMessage);
    }
}

function subjectsByGroupId() {
    var instituteId = isNaN(parseInt($("#instituteId").val())) ? -1 : (parseInt($("#instituteId").val()));
    var departmentId = isNaN(parseInt($("#departmentId").val())) ? -1 : (parseInt($("#departmentId").val()));
    var groupId = isNaN(parseInt($("#groupId").val())) ? -1 : (parseInt($("#groupId").val()));
    var selector = $('#subjectId'), option = "";
    var textMessage = "Выберите предмет";
    var url = "/data/institutes/" + instituteId + "/departments/" + departmentId + "/groups/" + groupId + "/subjects";

    if (departmentId == -1 || instituteId == -1 || groupId == -1) {
        generateDefaultOption(selector, option, textMessage);
    } else {
        generateAjaxRequest(url, selector, textMessage);
    }
}


function lessons() {
    var instituteId = isNaN(parseInt($("#instituteId").val())) ? -1 : (parseInt($("#instituteId").val()));
    var departmentId = isNaN(parseInt($("#departmentId").val())) ? -1 : (parseInt($("#departmentId").val()));
    var subjectId = isNaN(parseInt($("#subjectId").val())) ? -1 : (parseInt($("#subjectId").val()));
    var groupId = isNaN(parseInt($("#groupId").val())) ? -1 : (parseInt($("#groupId").val()));
    var selector = $('#lessonId'), option = "";
    var textMessage = "Выберите занятие";
    var url = "/data/institutes/" + instituteId
        + "/departments/" + departmentId
        + "/subjects/" + subjectId
        + "/groups/" + groupId
        + "/lessons";
    if (subjectId == -1 || groupId == -1) {
        generateDefaultOption(selector, option, textMessage);
    } else {
        generateAjaxRequest(url, selector, textMessage);
    }
}

function students() {
    var instituteId = isNaN(parseInt($("#instituteId").val())) ? -1 : (parseInt($("#instituteId").val()));
    var departmentId = isNaN(parseInt($("#departmentId").val())) ? -1 : (parseInt($("#departmentId").val()));
    var subjectId = isNaN(parseInt($("#subjectId").val())) ? -1 : (parseInt($("#subjectId").val()));
    var groupId = isNaN(parseInt($("#groupId").val())) ? -1 : (parseInt($("#groupId").val()));
    var selector = $('#studentId'), option = "";
    var textMessage = "Список студентов";
    var url = "/data/institutes/" + instituteId
        + "/departments/" + departmentId
        + "/subjects/" + subjectId
        + "/groups/" + groupId
        + "/students";
    if (subjectId == -1 || groupId == -1) {
        generateDefaultOption(selector, option, textMessage);
    } else {
        generateAjaxRequest(url, selector, textMessage);
    }
}

function studentsByGroupId() {
    var groupId = isNaN(parseInt($("#groupId").val())) ? -1 : (parseInt($("#groupId").val()));
    var selector = $('#studentId'), option = "";
    var textMessage = "Список студентов";
    var url = "/data/groups/" + groupId
        + "/students";
    if (groupId == -1) {
        generateDefaultOption(selector, option, textMessage);
    } else {
        generateAjaxRequest(url, selector, textMessage);
    }
}

function groupsBySubjectIdForTask() {
    var subjectId = isNaN(parseInt($("#subjectId").val())) ? -1 : (parseInt($("#subjectId").val()));
    var selector = $('#groupId'), option = "";
    var textMessage = "Выберите группу";
    var url = "/data/subjects/" + subjectId + "/groups";

    generateAjaxRequest(url, selector, textMessage);

}

function generateOptions(data, selector, textMessage) {
    selector.empty();
    var option = "<option value=" + "" + ">" + textMessage + "</option>";
    for (var i = 0; i < data.length; i++) {
        option = option + "<option value='" + data[i].value + "'>" + data[i].text + "</option>";
    }
    selector.append(option);
}

function generateDefaultOption(selector, option, textMessage) {
    selector.empty();
    var option = "<option value=" + "" + ">" + textMessage + "</option>";
    selector.append(option);
}

function generateAjaxRequest(url, selector, textMessage) {
    $.ajax({
        type: 'GET',
        url: url,
        success: function (data) {
            generateOptions(data, selector, textMessage);
        },
        error: function () {
            alert("Что-то пошло не так при получении данных!");
        }
    });
}

//начало блока работы с таблицами
function studentsTable() {
    var instituteId = isNaN(parseInt($("#instituteId").val())) ? -1 : (parseInt($("#instituteId").val()));
    var departmentId = isNaN(parseInt($("#departmentId").val())) ? -1 : (parseInt($("#departmentId").val()));
    var subjectId = isNaN(parseInt($("#subjectId").val())) ? -1 : (parseInt($("#subjectId").val()));
    var groupId = isNaN(parseInt($("#groupId").val())) ? -1 : (parseInt($("#groupId").val()));
    var table = document.getElementById('studentTable');
    var textMessage = "Список студентов";
    var url = "/data/institutes/" + instituteId
        + "/departments/" + departmentId
        + "/subjects/" + subjectId
        + "/groups/" + groupId
        + "/students";
    if (subjectId == -1 || groupId == -1) {
        alert('Ошибка при загрузке данных студентов!')
    } else {
        generateAjaxRequestForTable(url, table);
    }
}

function generateAjaxRequestForTable(url, table) {
    $.ajax({
        type: 'GET',
        url: url,
        success: function (data) {
            generateRow(data, table);
        },
        error: function () {
            alert("Что-то пошло не так при получении данных!");
        }
    });
}

function generateRow(data, table) {
    var headOfTab = table.tHead.insertRow();
    headOfTab.insertCell().innerHTML = "<b>Ф.И.О.</b>";
    headOfTab.insertCell().innerHTML = "<b>Присутствовал</b>";
    for (var i = 0; i < data.length; i++) {
        var oRow = table.tBodies[0].insertRow();
        oRow.insertCell().innerText = data[i].text;
        oRow.insertCell().innerHTML = '<input type="checkbox">';
        var idHandler = oRow.insertCell();
        idHandler.hidden = true;
        idHandler.innerText = data[i].value;
    }
}

function findCheckedRows() {
    var table = document.getElementById('studentTable');
    var checkedRows = table.tBodies[0].getElementsByTagName('input');
    var i = 0;
    var idArr = [];
    var namesArr = [];
    var url = "/attendance";
    while (checkedRows.length > i) {
        if (checkedRows.item(i).checked) {
            idArr.push(checkedRows.item(i).parentElement.parentElement.children[2].innerHTML);
            namesArr.push(checkedRows.item(i).parentElement.parentElement.children[0].innerHTML);
        }
        i++;
    }
    submitAttendance(url, idArr, namesArr);
}

function submitAttendance(url, studentsId, studentsName) {
    var subjectId = isNaN(parseInt($("#subjectId").val())) ? -1 : (parseInt($("#subjectId").val()));
    var groupId = isNaN(parseInt($("#groupId").val())) ? -1 : (parseInt($("#groupId").val()));
    var lessonId = isNaN(parseInt($("#lessonId").val())) ? -1 : (parseInt($("#lessonId").val()));

    $.ajax({
        type: 'POST',
        url: url,
        data: JSON.stringify({
            subjectId: subjectId,
            lessonId: lessonId,
            groupId: groupId,
            studentsId: studentsId,
            studentsName: studentsName
        }),
        contentType: 'application/json',
        success: function (data) {
            alert(data);
        },
        error: function () {
            alert("Что-то пошло не так при отправке данных!");
        }
    });
}

function cleanTable() {
    var table = document.getElementById('studentTable');
    var oRows = table.tBodies[0].getElementsByTagName('tr');
    var i = 0;
    /* начинаем перебор элементов в цикле */
    while (i < oRows.length) {
        table.deleteRow(i);
    }
}

function buttonFunk() {
    let validationCheck = true;
    validationCheck = validationSelectorById("subjectId", validationCheck);
    validationCheck = validationSelectorById("groupId", validationCheck);
    validationCheck = validationSelectorById("lessonId", validationCheck);
    if (validationCheck) {
        findCheckedRows();
    }
}

function validationSelectorById(idName, validationCheck) {
    const input = document.getElementById(idName);
    const feedbackElement = document.getElementById(idName + `Error`);
    input.classList.remove('is-invalid');
    input.classList.remove('is-valid');
    feedbackElement.hidden = true;
    const value = input.value;
    if (value == null || value == "") {
        input.classList.add('is-invalid');
        feedbackElement.hidden = false;
        validationCheck = false;
    }
    return validationCheck;
}